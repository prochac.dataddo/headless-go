ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
UID:=$(shell id -u)
GID:=$(shell id -g)
MODULE_NAME:=$(shell cd "${ROOT_DIR}";go list -m)
GIT_HOST:=$(shell echo ${MODULE_NAME} | cut -d'/' -f1)
GIT_USER_ID:=$(shell echo ${MODULE_NAME} | cut -d'/' -f2)
GIT_REPO_ID:=$(shell echo ${MODULE_NAME} | cut -d'/' -f3)

# tests do
GEN_GARBAGE=.openapi-generator/ api/ docs/ test/ .gitignore .openapi-generator-ignore .travis.yml git_push.sh go.mod go.sum README.md

openapi_generator_version=v6.4.0
remote_openapi_file=https://headless.dataddo.com/docs/swagger.yaml

all: clean customer_api build-test

customer_api:
	docker run --rm \
		--user ${UID}:${GID} \
		-v "${ROOT_DIR}:/local" \
		openapitools/openapi-generator-cli:${openapi_generator_version} generate \
        	-i ${remote_openapi_file} \
        	-g go \
        	--git-host "${GIT_HOST}" --git-user-id "${GIT_USER_ID}" --git-repo-id "${GIT_REPO_ID}" \
        	-p packageName=customer_api \
        	-p isGoSubmodule=true \
        	-o /local/customer_api
    # add missing oneOf/allOf code
	cp ${ROOT_DIR}/patch/*.go ${ROOT_DIR}/customer_api
	# remove unnecessary code
	cd ${ROOT_DIR}/customer_api && rm -r ${GEN_GARBAGE}
	# fix imports
	cd ${ROOT_DIR}/customer_api && goimports -w . && go mod tidy
    # patch oneOf code of Flow object
	patch ${ROOT_DIR}/customer_api/model_flow.go ${ROOT_DIR}/patch/model_flow.go.patch
	# sync demo project with generated SDK
	cd ${ROOT_DIR} && go mod tidy

build-test:
	go build ./...

commit:
	git add ${ROOT_DIR}/customer_api
	git commit -am "update generated code"

clean:
	-rm -rf ${ROOT_DIR}/customer_api
