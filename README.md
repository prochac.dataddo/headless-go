# Dataddo API SDK for Go

More info here:  
[https://headless.dataddo.com/](https://headless.dataddo.com/)

## regenerate OpenAPI client

Use the included Makefile.

Simple `make` will create new client based on the
latest [online OpenAPI spec](https://headless.dataddo.com/docs/swagger.yaml).

## Use of the client

Dataddo Headless API uses HTTP Cookies for Auth.
That means, the first API call should be login.
Feel free to use provided helper that initializes Cookie Jar for the client.

```go
package main

import (
	"gitlab.com/prochac.dataddo/headless-go/auth"
	capi "gitlab.com/prochac.dataddo/headless-go/customer_api"
)

func main() {
	cfg := capi.NewConfiguration()
	client := capi.NewAPIClient(cfg)

	if err := auth.Login(ctx, client, email, password); err != nil {
		// handle login error
	}
}
```

The token inside cookie requires refresh, from time to time.
For long-running process don't forget refresh mechanism.

```go
package main

import (
	"context"
	"time"

	"gitlab.com/prochac.dataddo/headless-go/auth"
	capi "gitlab.com/prochac.dataddo/headless-go/customer_api"
)

func runTokenRefresher(ctx context.Context, client *capi.APIClient) {
	for {
		select {
		case <-ctx.Done():
		// correctly, it should be based on expiration
		case <-time.After(10 * time.Minute):
			if err := auth.Refresh(ctx, client); err != nil {
				// handle error
			}
		}
	}
}

```
