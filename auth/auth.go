package auth

import (
	"context"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"

	"github.com/pkg/errors"

	capi "gitlab.com/prochac.dataddo/headless-go/customer_api"
)

func Login(ctx context.Context, client *capi.APIClient, email, password string) error {
	authResp, _, err := client.AuthApi.
		AuthControllerAuth(ctx).
		AuthRequest(capi.AuthRequest{
			Email:    capi.PtrString(email),
			Password: capi.PtrString(password),
		}).
		Execute()
	if err != nil {
		return err
	}
	_ = authResp.ExpiresIn

	u := serverURL(ctx, client)
	jar, _ := cookiejar.New(nil)
	setCookies(jar, u, authResp)
	client.GetConfig().HTTPClient.Jar = jar
	return nil
}

func Refresh(ctx context.Context, client *capi.APIClient) error {
	refreshResp, _, err := client.AuthApi.AuthControllerRefresh(ctx).Execute()
	if err != nil {
		return errors.WithStack(err)
	}
	u := serverURL(ctx, client)
	setCookies(client.GetConfig().HTTPClient.Jar, u, refreshResp)
	return nil
}

func serverURL(ctx context.Context, client *capi.APIClient) *url.URL {
	serverIdx, _ := ctx.Value(capi.ContextServerIndex).(int)
	if serverIdx >= len(client.GetConfig().Servers) {
		// if no single server config, the panic is ok-ish
		serverIdx = 0
	}
	serverURL := client.GetConfig().Servers[serverIdx].URL
	// If url is invalid, everything will break.
	u, _ := url.Parse(serverURL)
	return u
}

func setCookies(jar http.CookieJar, u *url.URL, authResp *capi.SecurityDto) {
	jar.SetCookies(u, []*http.Cookie{
		{Name: "dataddo-access-token", Value: authResp.GetAccessToken()},
		{Name: "dataddo-provider", Value: authResp.GetProvider().(string)},
		{Name: "dataddo-refresh-token", Value: authResp.GetRefreshToken()},
		{Name: "dataddo-realm", Value: strconv.Itoa(int(authResp.GetRealmId()))},
	})
}
