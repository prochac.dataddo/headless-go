/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
)

// ConnectorsGoogleAdsApiService ConnectorsGoogleAdsApi service
type ConnectorsGoogleAdsApiService service

type ApiGoogleAdsControllerActionAttributeRequest struct {
	ctx                                    context.Context
	ApiService                             *ConnectorsGoogleAdsApiService
	googleAdsControllerActionMetricRequest *GoogleAdsControllerActionMetricRequest
}

func (r ApiGoogleAdsControllerActionAttributeRequest) GoogleAdsControllerActionMetricRequest(googleAdsControllerActionMetricRequest GoogleAdsControllerActionMetricRequest) ApiGoogleAdsControllerActionAttributeRequest {
	r.googleAdsControllerActionMetricRequest = &googleAdsControllerActionMetricRequest
	return r
}

func (r ApiGoogleAdsControllerActionAttributeRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionAttributeExecute(r)
}

/*
GoogleAdsControllerActionAttribute List of all attributes

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionAttributeRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionAttribute(ctx context.Context) ApiGoogleAdsControllerActionAttributeRequest {
	return ApiGoogleAdsControllerActionAttributeRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionAttributeExecute(r ApiGoogleAdsControllerActionAttributeRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionAttribute")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/actions/attribute"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.googleAdsControllerActionMetricRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGoogleAdsControllerActionAuthorizationRequest struct {
	ctx        context.Context
	ApiService *ConnectorsGoogleAdsApiService
}

func (r ApiGoogleAdsControllerActionAuthorizationRequest) Execute() ([]ActionAuthorizationResponseInner, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionAuthorizationExecute(r)
}

/*
GoogleAdsControllerActionAuthorization List of authorization objects

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionAuthorizationRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionAuthorization(ctx context.Context) ApiGoogleAdsControllerActionAuthorizationRequest {
	return ApiGoogleAdsControllerActionAuthorizationRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []ActionAuthorizationResponseInner
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionAuthorizationExecute(r ApiGoogleAdsControllerActionAuthorizationRequest) ([]ActionAuthorizationResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []ActionAuthorizationResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionAuthorization")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/actions/authorization"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGoogleAdsControllerActionCustomerRequest struct {
	ctx                                  context.Context
	ApiService                           *ConnectorsGoogleAdsApiService
	hubspotControllerActionObjectRequest *HubspotControllerActionObjectRequest
}

func (r ApiGoogleAdsControllerActionCustomerRequest) HubspotControllerActionObjectRequest(hubspotControllerActionObjectRequest HubspotControllerActionObjectRequest) ApiGoogleAdsControllerActionCustomerRequest {
	r.hubspotControllerActionObjectRequest = &hubspotControllerActionObjectRequest
	return r
}

func (r ApiGoogleAdsControllerActionCustomerRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionCustomerExecute(r)
}

/*
GoogleAdsControllerActionCustomer List of customers

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionCustomerRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionCustomer(ctx context.Context) ApiGoogleAdsControllerActionCustomerRequest {
	return ApiGoogleAdsControllerActionCustomerRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionCustomerExecute(r ApiGoogleAdsControllerActionCustomerRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionCustomer")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/actions/customer"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.hubspotControllerActionObjectRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGoogleAdsControllerActionCustomerClientRequest struct {
	ctx                                            context.Context
	ApiService                                     *ConnectorsGoogleAdsApiService
	googleAdsControllerActionCustomerClientRequest *GoogleAdsControllerActionCustomerClientRequest
}

func (r ApiGoogleAdsControllerActionCustomerClientRequest) GoogleAdsControllerActionCustomerClientRequest(googleAdsControllerActionCustomerClientRequest GoogleAdsControllerActionCustomerClientRequest) ApiGoogleAdsControllerActionCustomerClientRequest {
	r.googleAdsControllerActionCustomerClientRequest = &googleAdsControllerActionCustomerClientRequest
	return r
}

func (r ApiGoogleAdsControllerActionCustomerClientRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionCustomerClientExecute(r)
}

/*
GoogleAdsControllerActionCustomerClient List of clients

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionCustomerClientRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionCustomerClient(ctx context.Context) ApiGoogleAdsControllerActionCustomerClientRequest {
	return ApiGoogleAdsControllerActionCustomerClientRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionCustomerClientExecute(r ApiGoogleAdsControllerActionCustomerClientRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionCustomerClient")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/actions/customerClient"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.googleAdsControllerActionCustomerClientRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGoogleAdsControllerActionLoadRequest struct {
	ctx                    context.Context
	ApiService             *ConnectorsGoogleAdsApiService
	googleAdsConnectorForm *GoogleAdsConnectorForm
}

func (r ApiGoogleAdsControllerActionLoadRequest) GoogleAdsConnectorForm(googleAdsConnectorForm GoogleAdsConnectorForm) ApiGoogleAdsControllerActionLoadRequest {
	r.googleAdsConnectorForm = &googleAdsConnectorForm
	return r
}

func (r ApiGoogleAdsControllerActionLoadRequest) Execute() (*Source, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionLoadExecute(r)
}

/*
GoogleAdsControllerActionLoad Create GoogleAds source

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionLoadRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionLoad(ctx context.Context) ApiGoogleAdsControllerActionLoadRequest {
	return ApiGoogleAdsControllerActionLoadRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return Source
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionLoadExecute(r ApiGoogleAdsControllerActionLoadRequest) (*Source, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *Source
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionLoad")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/create-source"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.googleAdsConnectorForm
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGoogleAdsControllerActionMetricRequest struct {
	ctx                                    context.Context
	ApiService                             *ConnectorsGoogleAdsApiService
	googleAdsControllerActionMetricRequest *GoogleAdsControllerActionMetricRequest
}

func (r ApiGoogleAdsControllerActionMetricRequest) GoogleAdsControllerActionMetricRequest(googleAdsControllerActionMetricRequest GoogleAdsControllerActionMetricRequest) ApiGoogleAdsControllerActionMetricRequest {
	r.googleAdsControllerActionMetricRequest = &googleAdsControllerActionMetricRequest
	return r
}

func (r ApiGoogleAdsControllerActionMetricRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionMetricExecute(r)
}

/*
GoogleAdsControllerActionMetric List of metric objects

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionMetricRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionMetric(ctx context.Context) ApiGoogleAdsControllerActionMetricRequest {
	return ApiGoogleAdsControllerActionMetricRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionMetricExecute(r ApiGoogleAdsControllerActionMetricRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionMetric")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/actions/metric"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.googleAdsControllerActionMetricRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGoogleAdsControllerActionPreviewRequest struct {
	ctx                    context.Context
	ApiService             *ConnectorsGoogleAdsApiService
	googleAdsConnectorForm *GoogleAdsConnectorForm
}

func (r ApiGoogleAdsControllerActionPreviewRequest) GoogleAdsConnectorForm(googleAdsConnectorForm GoogleAdsConnectorForm) ApiGoogleAdsControllerActionPreviewRequest {
	r.googleAdsConnectorForm = &googleAdsConnectorForm
	return r
}

func (r ApiGoogleAdsControllerActionPreviewRequest) Execute() (*SuccessResponse, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionPreviewExecute(r)
}

/*
GoogleAdsControllerActionPreview Preview the data

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionPreviewRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionPreview(ctx context.Context) ApiGoogleAdsControllerActionPreviewRequest {
	return ApiGoogleAdsControllerActionPreviewRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return SuccessResponse
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionPreviewExecute(r ApiGoogleAdsControllerActionPreviewRequest) (*SuccessResponse, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *SuccessResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionPreview")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/preview"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.googleAdsConnectorForm
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGoogleAdsControllerActionReportTypeRequest struct {
	ctx        context.Context
	ApiService *ConnectorsGoogleAdsApiService
}

func (r ApiGoogleAdsControllerActionReportTypeRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionReportTypeExecute(r)
}

/*
GoogleAdsControllerActionReportType List of all report types

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionReportTypeRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionReportType(ctx context.Context) ApiGoogleAdsControllerActionReportTypeRequest {
	return ApiGoogleAdsControllerActionReportTypeRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionReportTypeExecute(r ApiGoogleAdsControllerActionReportTypeRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionReportType")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/actions/reportType"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGoogleAdsControllerActionSegmentRequest struct {
	ctx                                    context.Context
	ApiService                             *ConnectorsGoogleAdsApiService
	googleAdsControllerActionMetricRequest *GoogleAdsControllerActionMetricRequest
}

func (r ApiGoogleAdsControllerActionSegmentRequest) GoogleAdsControllerActionMetricRequest(googleAdsControllerActionMetricRequest GoogleAdsControllerActionMetricRequest) ApiGoogleAdsControllerActionSegmentRequest {
	r.googleAdsControllerActionMetricRequest = &googleAdsControllerActionMetricRequest
	return r
}

func (r ApiGoogleAdsControllerActionSegmentRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.GoogleAdsControllerActionSegmentExecute(r)
}

/*
GoogleAdsControllerActionSegment List of all segments

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGoogleAdsControllerActionSegmentRequest
*/
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionSegment(ctx context.Context) ApiGoogleAdsControllerActionSegmentRequest {
	return ApiGoogleAdsControllerActionSegmentRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *ConnectorsGoogleAdsApiService) GoogleAdsControllerActionSegmentExecute(r ApiGoogleAdsControllerActionSegmentRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsGoogleAdsApiService.GoogleAdsControllerActionSegment")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/google_ads/actions/segment"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.googleAdsControllerActionMetricRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
