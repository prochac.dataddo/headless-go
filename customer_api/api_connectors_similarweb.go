/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
	"strings"
)

// ConnectorsSimilarwebApiService ConnectorsSimilarwebApi service
type ConnectorsSimilarwebApiService service

type ApiSimilarwebControllerActionAuthorizationRequest struct {
	ctx        context.Context
	ApiService *ConnectorsSimilarwebApiService
}

func (r ApiSimilarwebControllerActionAuthorizationRequest) Execute() ([]ActionAuthorizationResponseInner, *http.Response, error) {
	return r.ApiService.SimilarwebControllerActionAuthorizationExecute(r)
}

/*
SimilarwebControllerActionAuthorization List of authorization objects

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiSimilarwebControllerActionAuthorizationRequest
*/
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionAuthorization(ctx context.Context) ApiSimilarwebControllerActionAuthorizationRequest {
	return ApiSimilarwebControllerActionAuthorizationRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []ActionAuthorizationResponseInner
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionAuthorizationExecute(r ApiSimilarwebControllerActionAuthorizationRequest) ([]ActionAuthorizationResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []ActionAuthorizationResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSimilarwebApiService.SimilarwebControllerActionAuthorization")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/similarweb/actions/authorization"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiSimilarwebControllerActionDateRangeRequest struct {
	ctx        context.Context
	ApiService *ConnectorsSimilarwebApiService
}

func (r ApiSimilarwebControllerActionDateRangeRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.SimilarwebControllerActionDateRangeExecute(r)
}

/*
SimilarwebControllerActionDateRange Method for listing date range

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiSimilarwebControllerActionDateRangeRequest
*/
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionDateRange(ctx context.Context) ApiSimilarwebControllerActionDateRangeRequest {
	return ApiSimilarwebControllerActionDateRangeRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionDateRangeExecute(r ApiSimilarwebControllerActionDateRangeRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSimilarwebApiService.SimilarwebControllerActionDateRange")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/similarweb/actions/dateRange"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiSimilarwebControllerActionLoadRequest struct {
	ctx                     context.Context
	ApiService              *ConnectorsSimilarwebApiService
	similarwebConnectorForm *SimilarwebConnectorForm
}

func (r ApiSimilarwebControllerActionLoadRequest) SimilarwebConnectorForm(similarwebConnectorForm SimilarwebConnectorForm) ApiSimilarwebControllerActionLoadRequest {
	r.similarwebConnectorForm = &similarwebConnectorForm
	return r
}

func (r ApiSimilarwebControllerActionLoadRequest) Execute() (*Source, *http.Response, error) {
	return r.ApiService.SimilarwebControllerActionLoadExecute(r)
}

/*
SimilarwebControllerActionLoad Create Similarweb source

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiSimilarwebControllerActionLoadRequest
*/
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionLoad(ctx context.Context) ApiSimilarwebControllerActionLoadRequest {
	return ApiSimilarwebControllerActionLoadRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return Source
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionLoadExecute(r ApiSimilarwebControllerActionLoadRequest) (*Source, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *Source
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSimilarwebApiService.SimilarwebControllerActionLoad")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/similarweb/create-source"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.similarwebConnectorForm
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiSimilarwebControllerActionPreviewRequest struct {
	ctx                     context.Context
	ApiService              *ConnectorsSimilarwebApiService
	similarwebConnectorForm *SimilarwebConnectorForm
}

func (r ApiSimilarwebControllerActionPreviewRequest) SimilarwebConnectorForm(similarwebConnectorForm SimilarwebConnectorForm) ApiSimilarwebControllerActionPreviewRequest {
	r.similarwebConnectorForm = &similarwebConnectorForm
	return r
}

func (r ApiSimilarwebControllerActionPreviewRequest) Execute() (*SuccessResponse, *http.Response, error) {
	return r.ApiService.SimilarwebControllerActionPreviewExecute(r)
}

/*
SimilarwebControllerActionPreview Preview the data

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiSimilarwebControllerActionPreviewRequest
*/
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionPreview(ctx context.Context) ApiSimilarwebControllerActionPreviewRequest {
	return ApiSimilarwebControllerActionPreviewRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return SuccessResponse
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionPreviewExecute(r ApiSimilarwebControllerActionPreviewRequest) (*SuccessResponse, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *SuccessResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSimilarwebApiService.SimilarwebControllerActionPreview")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/similarweb/preview"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.similarwebConnectorForm
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiSimilarwebControllerActionTemplateRequest struct {
	ctx        context.Context
	ApiService *ConnectorsSimilarwebApiService
	id         string
}

func (r ApiSimilarwebControllerActionTemplateRequest) Execute() (map[string]interface{}, *http.Response, error) {
	return r.ApiService.SimilarwebControllerActionTemplateExecute(r)
}

/*
SimilarwebControllerActionTemplate List details of dataset template

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param id The id of the template to retrieve
 @return ApiSimilarwebControllerActionTemplateRequest
*/
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionTemplate(ctx context.Context, id string) ApiSimilarwebControllerActionTemplateRequest {
	return ApiSimilarwebControllerActionTemplateRequest{
		ApiService: a,
		ctx:        ctx,
		id:         id,
	}
}

// Execute executes the request
//  @return map[string]interface{}
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionTemplateExecute(r ApiSimilarwebControllerActionTemplateRequest) (map[string]interface{}, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue map[string]interface{}
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSimilarwebApiService.SimilarwebControllerActionTemplate")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/similarweb/templates/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", url.PathEscape(parameterValueToString(r.id, "id")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiSimilarwebControllerActionTemplatesRequest struct {
	ctx        context.Context
	ApiService *ConnectorsSimilarwebApiService
}

func (r ApiSimilarwebControllerActionTemplatesRequest) Execute() (*ConnectorTemplateResponse, *http.Response, error) {
	return r.ApiService.SimilarwebControllerActionTemplatesExecute(r)
}

/*
SimilarwebControllerActionTemplates List all Similarweb dataset templates

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiSimilarwebControllerActionTemplatesRequest
*/
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionTemplates(ctx context.Context) ApiSimilarwebControllerActionTemplatesRequest {
	return ApiSimilarwebControllerActionTemplatesRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return ConnectorTemplateResponse
func (a *ConnectorsSimilarwebApiService) SimilarwebControllerActionTemplatesExecute(r ApiSimilarwebControllerActionTemplatesRequest) (*ConnectorTemplateResponse, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *ConnectorTemplateResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSimilarwebApiService.SimilarwebControllerActionTemplates")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/similarweb/templates"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
