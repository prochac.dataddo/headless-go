/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
)

// ConnectorsSnowflakeApiService ConnectorsSnowflakeApi service
type ConnectorsSnowflakeApiService service

type ApiAppConnectorSnowflakeSnowflakeConnectorActionAuthorizationRequest struct {
	ctx        context.Context
	ApiService *ConnectorsSnowflakeApiService
}

func (r ApiAppConnectorSnowflakeSnowflakeConnectorActionAuthorizationRequest) Execute() ([]ActionAuthorizationResponseInner, *http.Response, error) {
	return r.ApiService.AppConnectorSnowflakeSnowflakeConnectorActionAuthorizationExecute(r)
}

/*
AppConnectorSnowflakeSnowflakeConnectorActionAuthorization List of authorization objects

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppConnectorSnowflakeSnowflakeConnectorActionAuthorizationRequest
*/
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorActionAuthorization(ctx context.Context) ApiAppConnectorSnowflakeSnowflakeConnectorActionAuthorizationRequest {
	return ApiAppConnectorSnowflakeSnowflakeConnectorActionAuthorizationRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []ActionAuthorizationResponseInner
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorActionAuthorizationExecute(r ApiAppConnectorSnowflakeSnowflakeConnectorActionAuthorizationRequest) ([]ActionAuthorizationResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []ActionAuthorizationResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSnowflakeApiService.AppConnectorSnowflakeSnowflakeConnectorActionAuthorization")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/snowflake/actions/authorization"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppConnectorSnowflakeSnowflakeConnectorActionListColumnsRequest struct {
	ctx        context.Context
	ApiService *ConnectorsSnowflakeApiService
}

func (r ApiAppConnectorSnowflakeSnowflakeConnectorActionListColumnsRequest) Execute() ([]AppConnectorRedshiftRedshiftConnectorActionListTables200ResponseInner, *http.Response, error) {
	return r.ApiService.AppConnectorSnowflakeSnowflakeConnectorActionListColumnsExecute(r)
}

/*
AppConnectorSnowflakeSnowflakeConnectorActionListColumns List of table columns

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppConnectorSnowflakeSnowflakeConnectorActionListColumnsRequest
*/
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorActionListColumns(ctx context.Context) ApiAppConnectorSnowflakeSnowflakeConnectorActionListColumnsRequest {
	return ApiAppConnectorSnowflakeSnowflakeConnectorActionListColumnsRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []AppConnectorRedshiftRedshiftConnectorActionListTables200ResponseInner
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorActionListColumnsExecute(r ApiAppConnectorSnowflakeSnowflakeConnectorActionListColumnsRequest) ([]AppConnectorRedshiftRedshiftConnectorActionListTables200ResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []AppConnectorRedshiftRedshiftConnectorActionListTables200ResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSnowflakeApiService.AppConnectorSnowflakeSnowflakeConnectorActionListColumns")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/snowflake/actions/list-columns"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppConnectorSnowflakeSnowflakeConnectorActionListTablesRequest struct {
	ctx        context.Context
	ApiService *ConnectorsSnowflakeApiService
}

func (r ApiAppConnectorSnowflakeSnowflakeConnectorActionListTablesRequest) Execute() ([]AppConnectorRedshiftRedshiftConnectorActionListTables200ResponseInner, *http.Response, error) {
	return r.ApiService.AppConnectorSnowflakeSnowflakeConnectorActionListTablesExecute(r)
}

/*
AppConnectorSnowflakeSnowflakeConnectorActionListTables List of database tables

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppConnectorSnowflakeSnowflakeConnectorActionListTablesRequest
*/
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorActionListTables(ctx context.Context) ApiAppConnectorSnowflakeSnowflakeConnectorActionListTablesRequest {
	return ApiAppConnectorSnowflakeSnowflakeConnectorActionListTablesRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []AppConnectorRedshiftRedshiftConnectorActionListTables200ResponseInner
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorActionListTablesExecute(r ApiAppConnectorSnowflakeSnowflakeConnectorActionListTablesRequest) ([]AppConnectorRedshiftRedshiftConnectorActionListTables200ResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []AppConnectorRedshiftRedshiftConnectorActionListTables200ResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSnowflakeApiService.AppConnectorSnowflakeSnowflakeConnectorActionListTables")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/snowflake/actions/list-tables"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppConnectorSnowflakeSnowflakeConnectorActionSQLRequest struct {
	ctx        context.Context
	ApiService *ConnectorsSnowflakeApiService
}

func (r ApiAppConnectorSnowflakeSnowflakeConnectorActionSQLRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.AppConnectorSnowflakeSnowflakeConnectorActionSQLExecute(r)
}

/*
AppConnectorSnowflakeSnowflakeConnectorActionSQL Get generated SQL

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppConnectorSnowflakeSnowflakeConnectorActionSQLRequest
*/
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorActionSQL(ctx context.Context) ApiAppConnectorSnowflakeSnowflakeConnectorActionSQLRequest {
	return ApiAppConnectorSnowflakeSnowflakeConnectorActionSQLRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorActionSQLExecute(r ApiAppConnectorSnowflakeSnowflakeConnectorActionSQLRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSnowflakeApiService.AppConnectorSnowflakeSnowflakeConnectorActionSQL")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/snowflake/actions/sql"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppConnectorSnowflakeSnowflakeConnectorCreateSourceRequest struct {
	ctx          context.Context
	ApiService   *ConnectorsSnowflakeApiService
	snowflakeDto *SnowflakeDto
}

func (r ApiAppConnectorSnowflakeSnowflakeConnectorCreateSourceRequest) SnowflakeDto(snowflakeDto SnowflakeDto) ApiAppConnectorSnowflakeSnowflakeConnectorCreateSourceRequest {
	r.snowflakeDto = &snowflakeDto
	return r
}

func (r ApiAppConnectorSnowflakeSnowflakeConnectorCreateSourceRequest) Execute() (*Source, *http.Response, error) {
	return r.ApiService.AppConnectorSnowflakeSnowflakeConnectorCreateSourceExecute(r)
}

/*
AppConnectorSnowflakeSnowflakeConnectorCreateSource Create Snowflake source

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppConnectorSnowflakeSnowflakeConnectorCreateSourceRequest
*/
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorCreateSource(ctx context.Context) ApiAppConnectorSnowflakeSnowflakeConnectorCreateSourceRequest {
	return ApiAppConnectorSnowflakeSnowflakeConnectorCreateSourceRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return Source
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorCreateSourceExecute(r ApiAppConnectorSnowflakeSnowflakeConnectorCreateSourceRequest) (*Source, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *Source
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSnowflakeApiService.AppConnectorSnowflakeSnowflakeConnectorCreateSource")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/snowflake/create-source"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.snowflakeDto
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppConnectorSnowflakeSnowflakeConnectorPreviewRequest struct {
	ctx          context.Context
	ApiService   *ConnectorsSnowflakeApiService
	snowflakeDto *SnowflakeDto
}

func (r ApiAppConnectorSnowflakeSnowflakeConnectorPreviewRequest) SnowflakeDto(snowflakeDto SnowflakeDto) ApiAppConnectorSnowflakeSnowflakeConnectorPreviewRequest {
	r.snowflakeDto = &snowflakeDto
	return r
}

func (r ApiAppConnectorSnowflakeSnowflakeConnectorPreviewRequest) Execute() (*SuccessResponse, *http.Response, error) {
	return r.ApiService.AppConnectorSnowflakeSnowflakeConnectorPreviewExecute(r)
}

/*
AppConnectorSnowflakeSnowflakeConnectorPreview Data preview

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppConnectorSnowflakeSnowflakeConnectorPreviewRequest
*/
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorPreview(ctx context.Context) ApiAppConnectorSnowflakeSnowflakeConnectorPreviewRequest {
	return ApiAppConnectorSnowflakeSnowflakeConnectorPreviewRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return SuccessResponse
func (a *ConnectorsSnowflakeApiService) AppConnectorSnowflakeSnowflakeConnectorPreviewExecute(r ApiAppConnectorSnowflakeSnowflakeConnectorPreviewRequest) (*SuccessResponse, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *SuccessResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ConnectorsSnowflakeApiService.AppConnectorSnowflakeSnowflakeConnectorPreview")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/connectors/snowflake/preview"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.snowflakeDto
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
