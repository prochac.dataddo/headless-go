/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
)

// DestinationsGoogleCloudStorageApiService DestinationsGoogleCloudStorageApi service
type DestinationsGoogleCloudStorageApiService service

type ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorizationRequest struct {
	ctx        context.Context
	ApiService *DestinationsGoogleCloudStorageApiService
}

func (r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorizationRequest) Execute() ([]ActionAuthorizationResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorizationExecute(r)
}

/*
AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorization List of authorization objects

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorizationRequest
*/
func (a *DestinationsGoogleCloudStorageApiService) AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorization(ctx context.Context) ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorizationRequest {
	return ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorizationRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []ActionAuthorizationResponseInner
func (a *DestinationsGoogleCloudStorageApiService) AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorizationExecute(r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorizationRequest) ([]ActionAuthorizationResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []ActionAuthorizationResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsGoogleCloudStorageApiService.AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionAuthorization")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/google_cloud_storage/actions/authorization"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatternsRequest struct {
	ctx        context.Context
	ApiService *DestinationsGoogleCloudStorageApiService
}

func (r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatternsRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatternsExecute(r)
}

/*
AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatterns List of filename patterns

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatternsRequest
*/
func (a *DestinationsGoogleCloudStorageApiService) AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatterns(ctx context.Context) ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatternsRequest {
	return ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatternsRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *DestinationsGoogleCloudStorageApiService) AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatternsExecute(r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatternsRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsGoogleCloudStorageApiService.AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionFilenamePatterns")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/google_cloud_storage/actions/filename_formats"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesRequest struct {
	ctx                                                              context.Context
	ApiService                                                       *DestinationsGoogleCloudStorageApiService
	appDestinationOneDriveOneDriveDestinationActionWriteModesRequest *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest
}

func (r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesRequest) AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest(appDestinationOneDriveOneDriveDestinationActionWriteModesRequest AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesRequest {
	r.appDestinationOneDriveOneDriveDestinationActionWriteModesRequest = &appDestinationOneDriveOneDriveDestinationActionWriteModesRequest
	return r
}

func (r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesExecute(r)
}

/*
AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModes List of write modes

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesRequest
*/
func (a *DestinationsGoogleCloudStorageApiService) AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModes(ctx context.Context) ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesRequest {
	return ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *DestinationsGoogleCloudStorageApiService) AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesExecute(r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModesRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsGoogleCloudStorageApiService.AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationActionWriteModes")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/google_cloud_storage/actions/writeModes"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appDestinationOneDriveOneDriveDestinationActionWriteModesRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateRequest struct {
	ctx                       context.Context
	ApiService                *DestinationsGoogleCloudStorageApiService
	googleCloudStorageRequest *GoogleCloudStorageRequest
}

//
func (r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateRequest) GoogleCloudStorageRequest(googleCloudStorageRequest GoogleCloudStorageRequest) ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateRequest {
	r.googleCloudStorageRequest = &googleCloudStorageRequest
	return r
}

func (r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateRequest) Execute() (*GoogleCloudStorageDestination, *http.Response, error) {
	return r.ApiService.AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateExecute(r)
}

/*
AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreate Create destination

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateRequest
*/
func (a *DestinationsGoogleCloudStorageApiService) AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreate(ctx context.Context) ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateRequest {
	return ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return GoogleCloudStorageDestination
func (a *DestinationsGoogleCloudStorageApiService) AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateExecute(r ApiAppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreateRequest) (*GoogleCloudStorageDestination, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *GoogleCloudStorageDestination
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsGoogleCloudStorageApiService.AppDestinationGoogleCloudStorageGoogleCloudStorageDestinationCreate")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/google_cloud_storage"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.googleCloudStorageRequest == nil {
		return localVarReturnValue, nil, reportError("googleCloudStorageRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.googleCloudStorageRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
