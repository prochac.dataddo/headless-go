/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
)

// DestinationsGoogleSheetsApiService DestinationsGoogleSheetsApi service
type DestinationsGoogleSheetsApiService service

type ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorizationRequest struct {
	ctx        context.Context
	ApiService *DestinationsGoogleSheetsApiService
}

func (r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorizationRequest) Execute() ([]ActionAuthorizationResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorizationExecute(r)
}

/*
AppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorization List of authorization objects

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorizationRequest
*/
func (a *DestinationsGoogleSheetsApiService) AppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorization(ctx context.Context) ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorizationRequest {
	return ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorizationRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []ActionAuthorizationResponseInner
func (a *DestinationsGoogleSheetsApiService) AppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorizationExecute(r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorizationRequest) ([]ActionAuthorizationResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []ActionAuthorizationResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsGoogleSheetsApiService.AppDestinationGoogleSheetsGoogleSheetsDestinationActionAuthorization")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/google_sheets/actions/authorization"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest struct {
	ctx                                                                                 context.Context
	ApiService                                                                          *DestinationsGoogleSheetsApiService
	appDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest *AppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest
}

func (r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest) AppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest(appDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest AppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest) ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest {
	r.appDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest = &appDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest
	return r
}

func (r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsExecute(r)
}

/*
AppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheets List of spreadsheets

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest
*/
func (a *DestinationsGoogleSheetsApiService) AppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheets(ctx context.Context) ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest {
	return ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *DestinationsGoogleSheetsApiService) AppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsExecute(r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsGoogleSheetsApiService.AppDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheets")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/google_sheets/actions/availableSpreadsheets"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appDestinationGoogleSheetsGoogleSheetsDestinationActionAvailableSpreadsheetsRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesRequest struct {
	ctx                                                              context.Context
	ApiService                                                       *DestinationsGoogleSheetsApiService
	appDestinationOneDriveOneDriveDestinationActionWriteModesRequest *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest
}

func (r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesRequest) AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest(appDestinationOneDriveOneDriveDestinationActionWriteModesRequest AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesRequest {
	r.appDestinationOneDriveOneDriveDestinationActionWriteModesRequest = &appDestinationOneDriveOneDriveDestinationActionWriteModesRequest
	return r
}

func (r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesExecute(r)
}

/*
AppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModes List of write modes

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesRequest
*/
func (a *DestinationsGoogleSheetsApiService) AppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModes(ctx context.Context) ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesRequest {
	return ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *DestinationsGoogleSheetsApiService) AppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesExecute(r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModesRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsGoogleSheetsApiService.AppDestinationGoogleSheetsGoogleSheetsDestinationActionWriteModes")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/google_sheets/actions/writeModes"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appDestinationOneDriveOneDriveDestinationActionWriteModesRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationGoogleSheetsGoogleSheetsDestinationCreateRequest struct {
	ctx                        context.Context
	ApiService                 *DestinationsGoogleSheetsApiService
	googleSheetsStorageRequest *GoogleSheetsStorageRequest
}

//
func (r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationCreateRequest) GoogleSheetsStorageRequest(googleSheetsStorageRequest GoogleSheetsStorageRequest) ApiAppDestinationGoogleSheetsGoogleSheetsDestinationCreateRequest {
	r.googleSheetsStorageRequest = &googleSheetsStorageRequest
	return r
}

func (r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationCreateRequest) Execute() (*GoogleSheetsDestination, *http.Response, error) {
	return r.ApiService.AppDestinationGoogleSheetsGoogleSheetsDestinationCreateExecute(r)
}

/*
AppDestinationGoogleSheetsGoogleSheetsDestinationCreate Create destination

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationGoogleSheetsGoogleSheetsDestinationCreateRequest
*/
func (a *DestinationsGoogleSheetsApiService) AppDestinationGoogleSheetsGoogleSheetsDestinationCreate(ctx context.Context) ApiAppDestinationGoogleSheetsGoogleSheetsDestinationCreateRequest {
	return ApiAppDestinationGoogleSheetsGoogleSheetsDestinationCreateRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return GoogleSheetsDestination
func (a *DestinationsGoogleSheetsApiService) AppDestinationGoogleSheetsGoogleSheetsDestinationCreateExecute(r ApiAppDestinationGoogleSheetsGoogleSheetsDestinationCreateRequest) (*GoogleSheetsDestination, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *GoogleSheetsDestination
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsGoogleSheetsApiService.AppDestinationGoogleSheetsGoogleSheetsDestinationCreate")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/google_sheets"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.googleSheetsStorageRequest == nil {
		return localVarReturnValue, nil, reportError("googleSheetsStorageRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.googleSheetsStorageRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
