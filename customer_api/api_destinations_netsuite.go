/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
)

// DestinationsNetsuiteApiService DestinationsNetsuiteApi service
type DestinationsNetsuiteApiService service

type ApiAppDestinationNetsuiteNetsuiteDestinationActionAuthorizationRequest struct {
	ctx        context.Context
	ApiService *DestinationsNetsuiteApiService
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationActionAuthorizationRequest) Execute() ([]ActionAuthorizationResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationNetsuiteNetsuiteDestinationActionAuthorizationExecute(r)
}

/*
AppDestinationNetsuiteNetsuiteDestinationActionAuthorization List of authorization objects

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationNetsuiteNetsuiteDestinationActionAuthorizationRequest
*/
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionAuthorization(ctx context.Context) ApiAppDestinationNetsuiteNetsuiteDestinationActionAuthorizationRequest {
	return ApiAppDestinationNetsuiteNetsuiteDestinationActionAuthorizationRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []ActionAuthorizationResponseInner
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionAuthorizationExecute(r ApiAppDestinationNetsuiteNetsuiteDestinationActionAuthorizationRequest) ([]ActionAuthorizationResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []ActionAuthorizationResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsNetsuiteApiService.AppDestinationNetsuiteNetsuiteDestinationActionAuthorization")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/netsuite/actions/authorization"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsRequest struct {
	ctx                                                                   context.Context
	ApiService                                                            *DestinationsNetsuiteApiService
	appDestinationHubspotHubspotDestinationActionDestinationFieldsRequest *AppDestinationHubspotHubspotDestinationActionDestinationFieldsRequest
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsRequest) AppDestinationHubspotHubspotDestinationActionDestinationFieldsRequest(appDestinationHubspotHubspotDestinationActionDestinationFieldsRequest AppDestinationHubspotHubspotDestinationActionDestinationFieldsRequest) ApiAppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsRequest {
	r.appDestinationHubspotHubspotDestinationActionDestinationFieldsRequest = &appDestinationHubspotHubspotDestinationActionDestinationFieldsRequest
	return r
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsRequest) Execute() ([]DestinationFieldResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsExecute(r)
}

/*
AppDestinationNetsuiteNetsuiteDestinationActionDestinationFields List of destination fields

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsRequest
*/
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionDestinationFields(ctx context.Context) ApiAppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsRequest {
	return ApiAppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []DestinationFieldResponseInner
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsExecute(r ApiAppDestinationNetsuiteNetsuiteDestinationActionDestinationFieldsRequest) ([]DestinationFieldResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []DestinationFieldResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsNetsuiteApiService.AppDestinationNetsuiteNetsuiteDestinationActionDestinationFields")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/netsuite/actions/destinationFields"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.appDestinationHubspotHubspotDestinationActionDestinationFieldsRequest == nil {
		return localVarReturnValue, nil, reportError("appDestinationHubspotHubspotDestinationActionDestinationFieldsRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appDestinationHubspotHubspotDestinationActionDestinationFieldsRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationNetsuiteNetsuiteDestinationActionEndpointRequest struct {
	ctx        context.Context
	ApiService *DestinationsNetsuiteApiService
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationActionEndpointRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationNetsuiteNetsuiteDestinationActionEndpointExecute(r)
}

/*
AppDestinationNetsuiteNetsuiteDestinationActionEndpoint List of endpoints

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationNetsuiteNetsuiteDestinationActionEndpointRequest
*/
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionEndpoint(ctx context.Context) ApiAppDestinationNetsuiteNetsuiteDestinationActionEndpointRequest {
	return ApiAppDestinationNetsuiteNetsuiteDestinationActionEndpointRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionEndpointExecute(r ApiAppDestinationNetsuiteNetsuiteDestinationActionEndpointRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodGet
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsNetsuiteApiService.AppDestinationNetsuiteNetsuiteDestinationActionEndpoint")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/netsuite/actions/endpoint"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsRequest struct {
	ctx                context.Context
	ApiService         *DestinationsNetsuiteApiService
	flowPreviewRequest *FlowPreviewRequest
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsRequest) FlowPreviewRequest(flowPreviewRequest FlowPreviewRequest) ApiAppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsRequest {
	r.flowPreviewRequest = &flowPreviewRequest
	return r
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsRequest) Execute() ([]SourceFieldResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsExecute(r)
}

/*
AppDestinationNetsuiteNetsuiteDestinationActionSourceFields List of source fields

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsRequest
*/
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionSourceFields(ctx context.Context) ApiAppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsRequest {
	return ApiAppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []SourceFieldResponseInner
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsExecute(r ApiAppDestinationNetsuiteNetsuiteDestinationActionSourceFieldsRequest) ([]SourceFieldResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []SourceFieldResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsNetsuiteApiService.AppDestinationNetsuiteNetsuiteDestinationActionSourceFields")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/netsuite/actions/sourceFields"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.flowPreviewRequest == nil {
		return localVarReturnValue, nil, reportError("flowPreviewRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.flowPreviewRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationNetsuiteNetsuiteDestinationActionWriteModesRequest struct {
	ctx                                                              context.Context
	ApiService                                                       *DestinationsNetsuiteApiService
	appDestinationOneDriveOneDriveDestinationActionWriteModesRequest *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationActionWriteModesRequest) AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest(appDestinationOneDriveOneDriveDestinationActionWriteModesRequest AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) ApiAppDestinationNetsuiteNetsuiteDestinationActionWriteModesRequest {
	r.appDestinationOneDriveOneDriveDestinationActionWriteModesRequest = &appDestinationOneDriveOneDriveDestinationActionWriteModesRequest
	return r
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationActionWriteModesRequest) Execute() ([]LabelValueResponseInner, *http.Response, error) {
	return r.ApiService.AppDestinationNetsuiteNetsuiteDestinationActionWriteModesExecute(r)
}

/*
AppDestinationNetsuiteNetsuiteDestinationActionWriteModes List of write modes

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationNetsuiteNetsuiteDestinationActionWriteModesRequest
*/
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionWriteModes(ctx context.Context) ApiAppDestinationNetsuiteNetsuiteDestinationActionWriteModesRequest {
	return ApiAppDestinationNetsuiteNetsuiteDestinationActionWriteModesRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return []LabelValueResponseInner
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationActionWriteModesExecute(r ApiAppDestinationNetsuiteNetsuiteDestinationActionWriteModesRequest) ([]LabelValueResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue []LabelValueResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsNetsuiteApiService.AppDestinationNetsuiteNetsuiteDestinationActionWriteModes")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/netsuite/actions/writeModes"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appDestinationOneDriveOneDriveDestinationActionWriteModesRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppDestinationNetsuiteNetsuiteDestinationCreateRequest struct {
	ctx                    context.Context
	ApiService             *DestinationsNetsuiteApiService
	netsuiteStorageRequest *NetsuiteStorageRequest
}

//
func (r ApiAppDestinationNetsuiteNetsuiteDestinationCreateRequest) NetsuiteStorageRequest(netsuiteStorageRequest NetsuiteStorageRequest) ApiAppDestinationNetsuiteNetsuiteDestinationCreateRequest {
	r.netsuiteStorageRequest = &netsuiteStorageRequest
	return r
}

func (r ApiAppDestinationNetsuiteNetsuiteDestinationCreateRequest) Execute() (*NetsuiteDestination, *http.Response, error) {
	return r.ApiService.AppDestinationNetsuiteNetsuiteDestinationCreateExecute(r)
}

/*
AppDestinationNetsuiteNetsuiteDestinationCreate Create destination

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppDestinationNetsuiteNetsuiteDestinationCreateRequest
*/
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationCreate(ctx context.Context) ApiAppDestinationNetsuiteNetsuiteDestinationCreateRequest {
	return ApiAppDestinationNetsuiteNetsuiteDestinationCreateRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return NetsuiteDestination
func (a *DestinationsNetsuiteApiService) AppDestinationNetsuiteNetsuiteDestinationCreateExecute(r ApiAppDestinationNetsuiteNetsuiteDestinationCreateRequest) (*NetsuiteDestination, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *NetsuiteDestination
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DestinationsNetsuiteApiService.AppDestinationNetsuiteNetsuiteDestinationCreate")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/destinations/netsuite"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.netsuiteStorageRequest == nil {
		return localVarReturnValue, nil, reportError("netsuiteStorageRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.netsuiteStorageRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
