/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
)

// ServicesGoogleBigQueryCustomApiService ServicesGoogleBigQueryCustomApi service
type ServicesGoogleBigQueryCustomApiService service

type ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest struct {
	ctx                                                                                    context.Context
	ApiService                                                                             *ServicesGoogleBigQueryCustomApiService
	appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest *AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest
}

//
func (r ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest) AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest(appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest) ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest {
	r.appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest = &appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest
	return r
}

func (r ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest) Execute() (*GoogleBigQueryCustomUserAuth, *http.Response, error) {
	return r.ApiService.AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackExecute(r)
}

/*
AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallback Processes callback URL

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest
*/
func (a *ServicesGoogleBigQueryCustomApiService) AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallback(ctx context.Context) ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest {
	return ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return GoogleBigQueryCustomUserAuth
func (a *ServicesGoogleBigQueryCustomApiService) AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackExecute(r ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest) (*GoogleBigQueryCustomUserAuth, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *GoogleBigQueryCustomUserAuth
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ServicesGoogleBigQueryCustomApiService.AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallback")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/services/google_big_query_custom/oauth-process-callback"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest == nil {
		return localVarReturnValue, nil, reportError("appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthCallbackRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest struct {
	ctx                                                                               context.Context
	ApiService                                                                        *ServicesGoogleBigQueryCustomApiService
	appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest *AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest
}

//
func (r ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest) AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest(appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest) ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest {
	r.appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest = &appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest
	return r
}

func (r ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest) Execute() (*GoogleBigQueryCustomUserAuth, *http.Response, error) {
	return r.ApiService.AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlExecute(r)
}

/*
AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrl Builds redirect URL

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest
*/
func (a *ServicesGoogleBigQueryCustomApiService) AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrl(ctx context.Context) ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest {
	return ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return GoogleBigQueryCustomUserAuth
func (a *ServicesGoogleBigQueryCustomApiService) AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlExecute(r ApiAppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest) (*GoogleBigQueryCustomUserAuth, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *GoogleBigQueryCustomUserAuth
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ServicesGoogleBigQueryCustomApiService.AppAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrl")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/services/google_big_query_custom/oauth-request-url"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest == nil {
		return localVarReturnValue, nil, reportError("appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appAuthorizerGoogleBigQueryCustomGoogleBigQueryCustomAuthorizerNewOAuthUrlRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
