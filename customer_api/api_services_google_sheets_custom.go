/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
)

// ServicesGoogleSheetsCustomApiService ServicesGoogleSheetsCustomApi service
type ServicesGoogleSheetsCustomApiService service

type ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest struct {
	ctx                                                                                context.Context
	ApiService                                                                         *ServicesGoogleSheetsCustomApiService
	appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest
}

//
func (r ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest) AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest(appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest) ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest {
	r.appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest = &appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest
	return r
}

func (r ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest) Execute() (*GoogleSheetsCustomUserAuth, *http.Response, error) {
	return r.ApiService.AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackExecute(r)
}

/*
AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallback Processes callback URL

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest
*/
func (a *ServicesGoogleSheetsCustomApiService) AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallback(ctx context.Context) ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest {
	return ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return GoogleSheetsCustomUserAuth
func (a *ServicesGoogleSheetsCustomApiService) AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackExecute(r ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest) (*GoogleSheetsCustomUserAuth, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *GoogleSheetsCustomUserAuth
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ServicesGoogleSheetsCustomApiService.AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallback")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/services/google_sheets_custom/oauth-process-callback"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest == nil {
		return localVarReturnValue, nil, reportError("appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthCallbackRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest struct {
	ctx                                                                           context.Context
	ApiService                                                                    *ServicesGoogleSheetsCustomApiService
	appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest
}

//
func (r ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest(appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest {
	r.appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest = &appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest
	return r
}

func (r ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) Execute() (*GoogleSheetsCustomUserAuth, *http.Response, error) {
	return r.ApiService.AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlExecute(r)
}

/*
AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrl Builds redirect URL

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest
*/
func (a *ServicesGoogleSheetsCustomApiService) AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrl(ctx context.Context) ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest {
	return ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return GoogleSheetsCustomUserAuth
func (a *ServicesGoogleSheetsCustomApiService) AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlExecute(r ApiAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) (*GoogleSheetsCustomUserAuth, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue *GoogleSheetsCustomUserAuth
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ServicesGoogleSheetsCustomApiService.AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrl")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/services/google_sheets_custom/oauth-request-url"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest == nil {
		return localVarReturnValue, nil, reportError("appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
