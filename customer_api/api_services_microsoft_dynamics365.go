/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
)

// ServicesMicrosoftDynamics365ApiService ServicesMicrosoftDynamics365Api service
type ServicesMicrosoftDynamics365ApiService service

type ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest struct {
	ctx                                                                                        context.Context
	ApiService                                                                                 *ServicesMicrosoftDynamics365ApiService
	appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest *AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest
}

//
func (r ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest) AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest(appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest) ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest {
	r.appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest = &appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest
	return r
}

func (r ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest) Execute() (map[string]interface{}, *http.Response, error) {
	return r.ApiService.AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackExecute(r)
}

/*
AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallback Create service

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest
*/
func (a *ServicesMicrosoftDynamics365ApiService) AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallback(ctx context.Context) ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest {
	return ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return map[string]interface{}
func (a *ServicesMicrosoftDynamics365ApiService) AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackExecute(r ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest) (map[string]interface{}, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue map[string]interface{}
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ServicesMicrosoftDynamics365ApiService.AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallback")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/services/microsoft_dynamics365/oauth-process-callback"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest == nil {
		return localVarReturnValue, nil, reportError("appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthProcessCallbackRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest struct {
	ctx                                                                                   context.Context
	ApiService                                                                            *ServicesMicrosoftDynamics365ApiService
	appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest *AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest
}

//
func (r ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest) AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest(appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest) ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest {
	r.appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest = &appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest
	return r
}

func (r ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest) Execute() (map[string]interface{}, *http.Response, error) {
	return r.ApiService.AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLExecute(r)
}

/*
AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURL Get url for service authentication

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest
*/
func (a *ServicesMicrosoftDynamics365ApiService) AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURL(ctx context.Context) ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest {
	return ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// Execute executes the request
//  @return map[string]interface{}
func (a *ServicesMicrosoftDynamics365ApiService) AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLExecute(r ApiAppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest) (map[string]interface{}, *http.Response, error) {
	var (
		localVarHTTPMethod  = http.MethodPost
		localVarPostBody    interface{}
		formFiles           []formFile
		localVarReturnValue map[string]interface{}
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ServicesMicrosoftDynamics365ApiService.AppAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURL")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/services/microsoft_dynamics365/oauth-request-url"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest == nil {
		return localVarReturnValue, nil, reportError("appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.appAuthorizerMicrosoftDynamics365MicrosoftDynamics365AuthorizerOAuthRequestURLRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
