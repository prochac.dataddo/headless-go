/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the AmazonAdsDataExtractorConnectorForm type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AmazonAdsDataExtractorConnectorForm{}

// AmazonAdsDataExtractorConnectorForm struct for AmazonAdsDataExtractorConnectorForm
type AmazonAdsDataExtractorConnectorForm struct {
	ConnectorId string `json:"connectorId"`
	TemplateId  string `json:"templateId"`
	Strategy    string `json:"strategy"`
	// Enforcing data types
	EnsureDataTypes map[string]interface{} `json:"ensureDataTypes,omitempty"`
	// Label of the source
	Label string `json:"label"`
	// Dataddo Flow endpoint
	Flow string `json:"flow"`
	// Dataddo token
	Token string `json:"token"`
}

// NewAmazonAdsDataExtractorConnectorForm instantiates a new AmazonAdsDataExtractorConnectorForm object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAmazonAdsDataExtractorConnectorForm(connectorId string, templateId string, strategy string, label string, flow string, token string) *AmazonAdsDataExtractorConnectorForm {
	this := AmazonAdsDataExtractorConnectorForm{}
	this.ConnectorId = connectorId
	this.TemplateId = templateId
	this.Strategy = strategy
	this.Label = label
	this.Flow = flow
	this.Token = token
	return &this
}

// NewAmazonAdsDataExtractorConnectorFormWithDefaults instantiates a new AmazonAdsDataExtractorConnectorForm object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAmazonAdsDataExtractorConnectorFormWithDefaults() *AmazonAdsDataExtractorConnectorForm {
	this := AmazonAdsDataExtractorConnectorForm{}
	return &this
}

// GetConnectorId returns the ConnectorId field value
func (o *AmazonAdsDataExtractorConnectorForm) GetConnectorId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ConnectorId
}

// GetConnectorIdOk returns a tuple with the ConnectorId field value
// and a boolean to check if the value has been set.
func (o *AmazonAdsDataExtractorConnectorForm) GetConnectorIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ConnectorId, true
}

// SetConnectorId sets field value
func (o *AmazonAdsDataExtractorConnectorForm) SetConnectorId(v string) {
	o.ConnectorId = v
}

// GetTemplateId returns the TemplateId field value
func (o *AmazonAdsDataExtractorConnectorForm) GetTemplateId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.TemplateId
}

// GetTemplateIdOk returns a tuple with the TemplateId field value
// and a boolean to check if the value has been set.
func (o *AmazonAdsDataExtractorConnectorForm) GetTemplateIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.TemplateId, true
}

// SetTemplateId sets field value
func (o *AmazonAdsDataExtractorConnectorForm) SetTemplateId(v string) {
	o.TemplateId = v
}

// GetStrategy returns the Strategy field value
func (o *AmazonAdsDataExtractorConnectorForm) GetStrategy() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Strategy
}

// GetStrategyOk returns a tuple with the Strategy field value
// and a boolean to check if the value has been set.
func (o *AmazonAdsDataExtractorConnectorForm) GetStrategyOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Strategy, true
}

// SetStrategy sets field value
func (o *AmazonAdsDataExtractorConnectorForm) SetStrategy(v string) {
	o.Strategy = v
}

// GetEnsureDataTypes returns the EnsureDataTypes field value if set, zero value otherwise.
func (o *AmazonAdsDataExtractorConnectorForm) GetEnsureDataTypes() map[string]interface{} {
	if o == nil || IsNil(o.EnsureDataTypes) {
		var ret map[string]interface{}
		return ret
	}
	return o.EnsureDataTypes
}

// GetEnsureDataTypesOk returns a tuple with the EnsureDataTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AmazonAdsDataExtractorConnectorForm) GetEnsureDataTypesOk() (map[string]interface{}, bool) {
	if o == nil || IsNil(o.EnsureDataTypes) {
		return map[string]interface{}{}, false
	}
	return o.EnsureDataTypes, true
}

// HasEnsureDataTypes returns a boolean if a field has been set.
func (o *AmazonAdsDataExtractorConnectorForm) HasEnsureDataTypes() bool {
	if o != nil && !IsNil(o.EnsureDataTypes) {
		return true
	}

	return false
}

// SetEnsureDataTypes gets a reference to the given map[string]interface{} and assigns it to the EnsureDataTypes field.
func (o *AmazonAdsDataExtractorConnectorForm) SetEnsureDataTypes(v map[string]interface{}) {
	o.EnsureDataTypes = v
}

// GetLabel returns the Label field value
func (o *AmazonAdsDataExtractorConnectorForm) GetLabel() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Label
}

// GetLabelOk returns a tuple with the Label field value
// and a boolean to check if the value has been set.
func (o *AmazonAdsDataExtractorConnectorForm) GetLabelOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Label, true
}

// SetLabel sets field value
func (o *AmazonAdsDataExtractorConnectorForm) SetLabel(v string) {
	o.Label = v
}

// GetFlow returns the Flow field value
func (o *AmazonAdsDataExtractorConnectorForm) GetFlow() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Flow
}

// GetFlowOk returns a tuple with the Flow field value
// and a boolean to check if the value has been set.
func (o *AmazonAdsDataExtractorConnectorForm) GetFlowOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Flow, true
}

// SetFlow sets field value
func (o *AmazonAdsDataExtractorConnectorForm) SetFlow(v string) {
	o.Flow = v
}

// GetToken returns the Token field value
func (o *AmazonAdsDataExtractorConnectorForm) GetToken() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Token
}

// GetTokenOk returns a tuple with the Token field value
// and a boolean to check if the value has been set.
func (o *AmazonAdsDataExtractorConnectorForm) GetTokenOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Token, true
}

// SetToken sets field value
func (o *AmazonAdsDataExtractorConnectorForm) SetToken(v string) {
	o.Token = v
}

func (o AmazonAdsDataExtractorConnectorForm) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AmazonAdsDataExtractorConnectorForm) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["connectorId"] = o.ConnectorId
	toSerialize["templateId"] = o.TemplateId
	toSerialize["strategy"] = o.Strategy
	if !IsNil(o.EnsureDataTypes) {
		toSerialize["ensureDataTypes"] = o.EnsureDataTypes
	}
	toSerialize["label"] = o.Label
	toSerialize["flow"] = o.Flow
	toSerialize["token"] = o.Token
	return toSerialize, nil
}

type NullableAmazonAdsDataExtractorConnectorForm struct {
	value *AmazonAdsDataExtractorConnectorForm
	isSet bool
}

func (v NullableAmazonAdsDataExtractorConnectorForm) Get() *AmazonAdsDataExtractorConnectorForm {
	return v.value
}

func (v *NullableAmazonAdsDataExtractorConnectorForm) Set(val *AmazonAdsDataExtractorConnectorForm) {
	v.value = val
	v.isSet = true
}

func (v NullableAmazonAdsDataExtractorConnectorForm) IsSet() bool {
	return v.isSet
}

func (v *NullableAmazonAdsDataExtractorConnectorForm) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAmazonAdsDataExtractorConnectorForm(val *AmazonAdsDataExtractorConnectorForm) *NullableAmazonAdsDataExtractorConnectorForm {
	return &NullableAmazonAdsDataExtractorConnectorForm{value: val, isSet: true}
}

func (v NullableAmazonAdsDataExtractorConnectorForm) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAmazonAdsDataExtractorConnectorForm) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
