/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest{}

// AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest struct for AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest
type AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest struct {
	Config *GoogleAnalyticsCustomDtoAuthorizer `json:"config,omitempty"`
}

// NewAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest instantiates a new AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest() *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest {
	this := AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest{}
	return &this
}

// NewAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequestWithDefaults instantiates a new AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequestWithDefaults() *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest {
	this := AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest{}
	return &this
}

// GetConfig returns the Config field value if set, zero value otherwise.
func (o *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) GetConfig() GoogleAnalyticsCustomDtoAuthorizer {
	if o == nil || IsNil(o.Config) {
		var ret GoogleAnalyticsCustomDtoAuthorizer
		return ret
	}
	return *o.Config
}

// GetConfigOk returns a tuple with the Config field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) GetConfigOk() (*GoogleAnalyticsCustomDtoAuthorizer, bool) {
	if o == nil || IsNil(o.Config) {
		return nil, false
	}
	return o.Config, true
}

// HasConfig returns a boolean if a field has been set.
func (o *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) HasConfig() bool {
	if o != nil && !IsNil(o.Config) {
		return true
	}

	return false
}

// SetConfig gets a reference to the given GoogleAnalyticsCustomDtoAuthorizer and assigns it to the Config field.
func (o *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) SetConfig(v GoogleAnalyticsCustomDtoAuthorizer) {
	o.Config = &v
}

func (o AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Config) {
		toSerialize["config"] = o.Config
	}
	return toSerialize, nil
}

type NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest struct {
	value *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest
	isSet bool
}

func (v NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) Get() *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest {
	return v.value
}

func (v *NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) Set(val *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest(val *AppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) *NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest {
	return &NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest{value: val, isSet: true}
}

func (v NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAppAuthorizerGoogleAnalyticsCustomGoogleAnalyticsCustomAuthorizerNewOAuthUrlRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
