/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest{}

// AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest struct for AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest
type AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest struct {
	Config *GoogleSheetsCustomDtoAuthorizer `json:"config,omitempty"`
}

// NewAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest instantiates a new AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest() *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest {
	this := AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest{}
	return &this
}

// NewAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequestWithDefaults instantiates a new AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequestWithDefaults() *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest {
	this := AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest{}
	return &this
}

// GetConfig returns the Config field value if set, zero value otherwise.
func (o *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) GetConfig() GoogleSheetsCustomDtoAuthorizer {
	if o == nil || IsNil(o.Config) {
		var ret GoogleSheetsCustomDtoAuthorizer
		return ret
	}
	return *o.Config
}

// GetConfigOk returns a tuple with the Config field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) GetConfigOk() (*GoogleSheetsCustomDtoAuthorizer, bool) {
	if o == nil || IsNil(o.Config) {
		return nil, false
	}
	return o.Config, true
}

// HasConfig returns a boolean if a field has been set.
func (o *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) HasConfig() bool {
	if o != nil && !IsNil(o.Config) {
		return true
	}

	return false
}

// SetConfig gets a reference to the given GoogleSheetsCustomDtoAuthorizer and assigns it to the Config field.
func (o *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) SetConfig(v GoogleSheetsCustomDtoAuthorizer) {
	o.Config = &v
}

func (o AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Config) {
		toSerialize["config"] = o.Config
	}
	return toSerialize, nil
}

type NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest struct {
	value *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest
	isSet bool
}

func (v NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) Get() *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest {
	return v.value
}

func (v *NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) Set(val *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest(val *AppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) *NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest {
	return &NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest{value: val, isSet: true}
}

func (v NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAppAuthorizerGoogleSheetsCustomGoogleSheetsCustomAuthorizerNewOAuthUrlRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
