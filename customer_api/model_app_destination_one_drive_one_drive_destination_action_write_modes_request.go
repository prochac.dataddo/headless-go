/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest{}

// AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest struct for AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest
type AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest struct {
	WriteMode *string `json:"writeMode,omitempty"`
}

// NewAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest instantiates a new AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest() *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest {
	this := AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest{}
	return &this
}

// NewAppDestinationOneDriveOneDriveDestinationActionWriteModesRequestWithDefaults instantiates a new AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAppDestinationOneDriveOneDriveDestinationActionWriteModesRequestWithDefaults() *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest {
	this := AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest{}
	return &this
}

// GetWriteMode returns the WriteMode field value if set, zero value otherwise.
func (o *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) GetWriteMode() string {
	if o == nil || IsNil(o.WriteMode) {
		var ret string
		return ret
	}
	return *o.WriteMode
}

// GetWriteModeOk returns a tuple with the WriteMode field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) GetWriteModeOk() (*string, bool) {
	if o == nil || IsNil(o.WriteMode) {
		return nil, false
	}
	return o.WriteMode, true
}

// HasWriteMode returns a boolean if a field has been set.
func (o *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) HasWriteMode() bool {
	if o != nil && !IsNil(o.WriteMode) {
		return true
	}

	return false
}

// SetWriteMode gets a reference to the given string and assigns it to the WriteMode field.
func (o *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) SetWriteMode(v string) {
	o.WriteMode = &v
}

func (o AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.WriteMode) {
		toSerialize["writeMode"] = o.WriteMode
	}
	return toSerialize, nil
}

type NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest struct {
	value *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest
	isSet bool
}

func (v NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) Get() *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest {
	return v.value
}

func (v *NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) Set(val *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest(val *AppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) *NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest {
	return &NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest{value: val, isSet: true}
}

func (v NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAppDestinationOneDriveOneDriveDestinationActionWriteModesRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
