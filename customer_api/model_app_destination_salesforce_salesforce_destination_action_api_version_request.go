/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest{}

// AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest struct for AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest
type AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest struct {
	OAuthId  *string `json:"oAuthId,omitempty"`
	Instance *string `json:"instance,omitempty"`
}

// NewAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest instantiates a new AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest() *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest {
	this := AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest{}
	return &this
}

// NewAppDestinationSalesforceSalesforceDestinationActionApiVersionRequestWithDefaults instantiates a new AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAppDestinationSalesforceSalesforceDestinationActionApiVersionRequestWithDefaults() *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest {
	this := AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest{}
	return &this
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise.
func (o *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) GetOAuthId() string {
	if o == nil || IsNil(o.OAuthId) {
		var ret string
		return ret
	}
	return *o.OAuthId
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) GetOAuthIdOk() (*string, bool) {
	if o == nil || IsNil(o.OAuthId) {
		return nil, false
	}
	return o.OAuthId, true
}

// HasOAuthId returns a boolean if a field has been set.
func (o *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) HasOAuthId() bool {
	if o != nil && !IsNil(o.OAuthId) {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given string and assigns it to the OAuthId field.
func (o *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) SetOAuthId(v string) {
	o.OAuthId = &v
}

// GetInstance returns the Instance field value if set, zero value otherwise.
func (o *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) GetInstance() string {
	if o == nil || IsNil(o.Instance) {
		var ret string
		return ret
	}
	return *o.Instance
}

// GetInstanceOk returns a tuple with the Instance field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) GetInstanceOk() (*string, bool) {
	if o == nil || IsNil(o.Instance) {
		return nil, false
	}
	return o.Instance, true
}

// HasInstance returns a boolean if a field has been set.
func (o *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) HasInstance() bool {
	if o != nil && !IsNil(o.Instance) {
		return true
	}

	return false
}

// SetInstance gets a reference to the given string and assigns it to the Instance field.
func (o *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) SetInstance(v string) {
	o.Instance = &v
}

func (o AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.OAuthId) {
		toSerialize["oAuthId"] = o.OAuthId
	}
	if !IsNil(o.Instance) {
		toSerialize["instance"] = o.Instance
	}
	return toSerialize, nil
}

type NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest struct {
	value *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest
	isSet bool
}

func (v NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) Get() *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest {
	return v.value
}

func (v *NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) Set(val *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest(val *AppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) *NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest {
	return &NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest{value: val, isSet: true}
}

func (v NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAppDestinationSalesforceSalesforceDestinationActionApiVersionRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
