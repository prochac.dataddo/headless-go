/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the ConnectorRequestDto type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ConnectorRequestDto{}

// ConnectorRequestDto struct for ConnectorRequestDto
type ConnectorRequestDto struct {
	Method *string `json:"method,omitempty"`
	Url    *string `json:"url,omitempty"`
	//
	Headers        []string `json:"headers,omitempty"`
	Transformation *string  `json:"transformation,omitempty"`
	Body           *string  `json:"body,omitempty"`
	//
	EnsureDataTypes      []string       `json:"ensureDataTypes,omitempty"`
	Emitter              NullableString `json:"emitter,omitempty"`
	EmitOnly             *bool          `json:"emitOnly,omitempty"`
	Format               *string        `json:"format,omitempty"`
	Timeout              *int32         `json:"timeout,omitempty"`
	SleepTimeMillisecond *int32         `json:"sleepTimeMillisecond,omitempty"`
	OAuthId              NullableInt32  `json:"oAuthId,omitempty"`
}

// NewConnectorRequestDto instantiates a new ConnectorRequestDto object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewConnectorRequestDto() *ConnectorRequestDto {
	this := ConnectorRequestDto{}
	return &this
}

// NewConnectorRequestDtoWithDefaults instantiates a new ConnectorRequestDto object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewConnectorRequestDtoWithDefaults() *ConnectorRequestDto {
	this := ConnectorRequestDto{}
	return &this
}

// GetMethod returns the Method field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetMethod() string {
	if o == nil || IsNil(o.Method) {
		var ret string
		return ret
	}
	return *o.Method
}

// GetMethodOk returns a tuple with the Method field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetMethodOk() (*string, bool) {
	if o == nil || IsNil(o.Method) {
		return nil, false
	}
	return o.Method, true
}

// HasMethod returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasMethod() bool {
	if o != nil && !IsNil(o.Method) {
		return true
	}

	return false
}

// SetMethod gets a reference to the given string and assigns it to the Method field.
func (o *ConnectorRequestDto) SetMethod(v string) {
	o.Method = &v
}

// GetUrl returns the Url field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetUrl() string {
	if o == nil || IsNil(o.Url) {
		var ret string
		return ret
	}
	return *o.Url
}

// GetUrlOk returns a tuple with the Url field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetUrlOk() (*string, bool) {
	if o == nil || IsNil(o.Url) {
		return nil, false
	}
	return o.Url, true
}

// HasUrl returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasUrl() bool {
	if o != nil && !IsNil(o.Url) {
		return true
	}

	return false
}

// SetUrl gets a reference to the given string and assigns it to the Url field.
func (o *ConnectorRequestDto) SetUrl(v string) {
	o.Url = &v
}

// GetHeaders returns the Headers field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetHeaders() []string {
	if o == nil || IsNil(o.Headers) {
		var ret []string
		return ret
	}
	return o.Headers
}

// GetHeadersOk returns a tuple with the Headers field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetHeadersOk() ([]string, bool) {
	if o == nil || IsNil(o.Headers) {
		return nil, false
	}
	return o.Headers, true
}

// HasHeaders returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasHeaders() bool {
	if o != nil && !IsNil(o.Headers) {
		return true
	}

	return false
}

// SetHeaders gets a reference to the given []string and assigns it to the Headers field.
func (o *ConnectorRequestDto) SetHeaders(v []string) {
	o.Headers = v
}

// GetTransformation returns the Transformation field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetTransformation() string {
	if o == nil || IsNil(o.Transformation) {
		var ret string
		return ret
	}
	return *o.Transformation
}

// GetTransformationOk returns a tuple with the Transformation field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetTransformationOk() (*string, bool) {
	if o == nil || IsNil(o.Transformation) {
		return nil, false
	}
	return o.Transformation, true
}

// HasTransformation returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasTransformation() bool {
	if o != nil && !IsNil(o.Transformation) {
		return true
	}

	return false
}

// SetTransformation gets a reference to the given string and assigns it to the Transformation field.
func (o *ConnectorRequestDto) SetTransformation(v string) {
	o.Transformation = &v
}

// GetBody returns the Body field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetBody() string {
	if o == nil || IsNil(o.Body) {
		var ret string
		return ret
	}
	return *o.Body
}

// GetBodyOk returns a tuple with the Body field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetBodyOk() (*string, bool) {
	if o == nil || IsNil(o.Body) {
		return nil, false
	}
	return o.Body, true
}

// HasBody returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasBody() bool {
	if o != nil && !IsNil(o.Body) {
		return true
	}

	return false
}

// SetBody gets a reference to the given string and assigns it to the Body field.
func (o *ConnectorRequestDto) SetBody(v string) {
	o.Body = &v
}

// GetEnsureDataTypes returns the EnsureDataTypes field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetEnsureDataTypes() []string {
	if o == nil || IsNil(o.EnsureDataTypes) {
		var ret []string
		return ret
	}
	return o.EnsureDataTypes
}

// GetEnsureDataTypesOk returns a tuple with the EnsureDataTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetEnsureDataTypesOk() ([]string, bool) {
	if o == nil || IsNil(o.EnsureDataTypes) {
		return nil, false
	}
	return o.EnsureDataTypes, true
}

// HasEnsureDataTypes returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasEnsureDataTypes() bool {
	if o != nil && !IsNil(o.EnsureDataTypes) {
		return true
	}

	return false
}

// SetEnsureDataTypes gets a reference to the given []string and assigns it to the EnsureDataTypes field.
func (o *ConnectorRequestDto) SetEnsureDataTypes(v []string) {
	o.EnsureDataTypes = v
}

// GetEmitter returns the Emitter field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *ConnectorRequestDto) GetEmitter() string {
	if o == nil || IsNil(o.Emitter.Get()) {
		var ret string
		return ret
	}
	return *o.Emitter.Get()
}

// GetEmitterOk returns a tuple with the Emitter field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *ConnectorRequestDto) GetEmitterOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Emitter.Get(), o.Emitter.IsSet()
}

// HasEmitter returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasEmitter() bool {
	if o != nil && o.Emitter.IsSet() {
		return true
	}

	return false
}

// SetEmitter gets a reference to the given NullableString and assigns it to the Emitter field.
func (o *ConnectorRequestDto) SetEmitter(v string) {
	o.Emitter.Set(&v)
}

// SetEmitterNil sets the value for Emitter to be an explicit nil
func (o *ConnectorRequestDto) SetEmitterNil() {
	o.Emitter.Set(nil)
}

// UnsetEmitter ensures that no value is present for Emitter, not even an explicit nil
func (o *ConnectorRequestDto) UnsetEmitter() {
	o.Emitter.Unset()
}

// GetEmitOnly returns the EmitOnly field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetEmitOnly() bool {
	if o == nil || IsNil(o.EmitOnly) {
		var ret bool
		return ret
	}
	return *o.EmitOnly
}

// GetEmitOnlyOk returns a tuple with the EmitOnly field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetEmitOnlyOk() (*bool, bool) {
	if o == nil || IsNil(o.EmitOnly) {
		return nil, false
	}
	return o.EmitOnly, true
}

// HasEmitOnly returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasEmitOnly() bool {
	if o != nil && !IsNil(o.EmitOnly) {
		return true
	}

	return false
}

// SetEmitOnly gets a reference to the given bool and assigns it to the EmitOnly field.
func (o *ConnectorRequestDto) SetEmitOnly(v bool) {
	o.EmitOnly = &v
}

// GetFormat returns the Format field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetFormat() string {
	if o == nil || IsNil(o.Format) {
		var ret string
		return ret
	}
	return *o.Format
}

// GetFormatOk returns a tuple with the Format field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetFormatOk() (*string, bool) {
	if o == nil || IsNil(o.Format) {
		return nil, false
	}
	return o.Format, true
}

// HasFormat returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasFormat() bool {
	if o != nil && !IsNil(o.Format) {
		return true
	}

	return false
}

// SetFormat gets a reference to the given string and assigns it to the Format field.
func (o *ConnectorRequestDto) SetFormat(v string) {
	o.Format = &v
}

// GetTimeout returns the Timeout field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetTimeout() int32 {
	if o == nil || IsNil(o.Timeout) {
		var ret int32
		return ret
	}
	return *o.Timeout
}

// GetTimeoutOk returns a tuple with the Timeout field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetTimeoutOk() (*int32, bool) {
	if o == nil || IsNil(o.Timeout) {
		return nil, false
	}
	return o.Timeout, true
}

// HasTimeout returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasTimeout() bool {
	if o != nil && !IsNil(o.Timeout) {
		return true
	}

	return false
}

// SetTimeout gets a reference to the given int32 and assigns it to the Timeout field.
func (o *ConnectorRequestDto) SetTimeout(v int32) {
	o.Timeout = &v
}

// GetSleepTimeMillisecond returns the SleepTimeMillisecond field value if set, zero value otherwise.
func (o *ConnectorRequestDto) GetSleepTimeMillisecond() int32 {
	if o == nil || IsNil(o.SleepTimeMillisecond) {
		var ret int32
		return ret
	}
	return *o.SleepTimeMillisecond
}

// GetSleepTimeMillisecondOk returns a tuple with the SleepTimeMillisecond field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorRequestDto) GetSleepTimeMillisecondOk() (*int32, bool) {
	if o == nil || IsNil(o.SleepTimeMillisecond) {
		return nil, false
	}
	return o.SleepTimeMillisecond, true
}

// HasSleepTimeMillisecond returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasSleepTimeMillisecond() bool {
	if o != nil && !IsNil(o.SleepTimeMillisecond) {
		return true
	}

	return false
}

// SetSleepTimeMillisecond gets a reference to the given int32 and assigns it to the SleepTimeMillisecond field.
func (o *ConnectorRequestDto) SetSleepTimeMillisecond(v int32) {
	o.SleepTimeMillisecond = &v
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *ConnectorRequestDto) GetOAuthId() int32 {
	if o == nil || IsNil(o.OAuthId.Get()) {
		var ret int32
		return ret
	}
	return *o.OAuthId.Get()
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *ConnectorRequestDto) GetOAuthIdOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.OAuthId.Get(), o.OAuthId.IsSet()
}

// HasOAuthId returns a boolean if a field has been set.
func (o *ConnectorRequestDto) HasOAuthId() bool {
	if o != nil && o.OAuthId.IsSet() {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given NullableInt32 and assigns it to the OAuthId field.
func (o *ConnectorRequestDto) SetOAuthId(v int32) {
	o.OAuthId.Set(&v)
}

// SetOAuthIdNil sets the value for OAuthId to be an explicit nil
func (o *ConnectorRequestDto) SetOAuthIdNil() {
	o.OAuthId.Set(nil)
}

// UnsetOAuthId ensures that no value is present for OAuthId, not even an explicit nil
func (o *ConnectorRequestDto) UnsetOAuthId() {
	o.OAuthId.Unset()
}

func (o ConnectorRequestDto) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ConnectorRequestDto) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Method) {
		toSerialize["method"] = o.Method
	}
	if !IsNil(o.Url) {
		toSerialize["url"] = o.Url
	}
	if !IsNil(o.Headers) {
		toSerialize["headers"] = o.Headers
	}
	if !IsNil(o.Transformation) {
		toSerialize["transformation"] = o.Transformation
	}
	if !IsNil(o.Body) {
		toSerialize["body"] = o.Body
	}
	if !IsNil(o.EnsureDataTypes) {
		toSerialize["ensureDataTypes"] = o.EnsureDataTypes
	}
	if o.Emitter.IsSet() {
		toSerialize["emitter"] = o.Emitter.Get()
	}
	if !IsNil(o.EmitOnly) {
		toSerialize["emitOnly"] = o.EmitOnly
	}
	if !IsNil(o.Format) {
		toSerialize["format"] = o.Format
	}
	if !IsNil(o.Timeout) {
		toSerialize["timeout"] = o.Timeout
	}
	if !IsNil(o.SleepTimeMillisecond) {
		toSerialize["sleepTimeMillisecond"] = o.SleepTimeMillisecond
	}
	if o.OAuthId.IsSet() {
		toSerialize["oAuthId"] = o.OAuthId.Get()
	}
	return toSerialize, nil
}

type NullableConnectorRequestDto struct {
	value *ConnectorRequestDto
	isSet bool
}

func (v NullableConnectorRequestDto) Get() *ConnectorRequestDto {
	return v.value
}

func (v *NullableConnectorRequestDto) Set(val *ConnectorRequestDto) {
	v.value = val
	v.isSet = true
}

func (v NullableConnectorRequestDto) IsSet() bool {
	return v.isSet
}

func (v *NullableConnectorRequestDto) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableConnectorRequestDto(val *ConnectorRequestDto) *NullableConnectorRequestDto {
	return &NullableConnectorRequestDto{value: val, isSet: true}
}

func (v NullableConnectorRequestDto) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableConnectorRequestDto) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
