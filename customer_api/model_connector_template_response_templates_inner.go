/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the ConnectorTemplateResponseTemplatesInner type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ConnectorTemplateResponseTemplatesInner{}

// ConnectorTemplateResponseTemplatesInner struct for ConnectorTemplateResponseTemplatesInner
type ConnectorTemplateResponseTemplatesInner struct {
	Name        *string       `json:"name,omitempty"`
	Description *string       `json:"description,omitempty"`
	Type        *string       `json:"type,omitempty"`
	TemplateId  *string       `json:"templateId,omitempty"`
	CategoryId  []interface{} `json:"categoryId,omitempty"`
	Attributes  []interface{} `json:"attributes,omitempty"`
}

// NewConnectorTemplateResponseTemplatesInner instantiates a new ConnectorTemplateResponseTemplatesInner object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewConnectorTemplateResponseTemplatesInner() *ConnectorTemplateResponseTemplatesInner {
	this := ConnectorTemplateResponseTemplatesInner{}
	return &this
}

// NewConnectorTemplateResponseTemplatesInnerWithDefaults instantiates a new ConnectorTemplateResponseTemplatesInner object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewConnectorTemplateResponseTemplatesInnerWithDefaults() *ConnectorTemplateResponseTemplatesInner {
	this := ConnectorTemplateResponseTemplatesInner{}
	return &this
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *ConnectorTemplateResponseTemplatesInner) GetName() string {
	if o == nil || IsNil(o.Name) {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorTemplateResponseTemplatesInner) GetNameOk() (*string, bool) {
	if o == nil || IsNil(o.Name) {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *ConnectorTemplateResponseTemplatesInner) HasName() bool {
	if o != nil && !IsNil(o.Name) {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *ConnectorTemplateResponseTemplatesInner) SetName(v string) {
	o.Name = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *ConnectorTemplateResponseTemplatesInner) GetDescription() string {
	if o == nil || IsNil(o.Description) {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorTemplateResponseTemplatesInner) GetDescriptionOk() (*string, bool) {
	if o == nil || IsNil(o.Description) {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *ConnectorTemplateResponseTemplatesInner) HasDescription() bool {
	if o != nil && !IsNil(o.Description) {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *ConnectorTemplateResponseTemplatesInner) SetDescription(v string) {
	o.Description = &v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *ConnectorTemplateResponseTemplatesInner) GetType() string {
	if o == nil || IsNil(o.Type) {
		var ret string
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorTemplateResponseTemplatesInner) GetTypeOk() (*string, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *ConnectorTemplateResponseTemplatesInner) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given string and assigns it to the Type field.
func (o *ConnectorTemplateResponseTemplatesInner) SetType(v string) {
	o.Type = &v
}

// GetTemplateId returns the TemplateId field value if set, zero value otherwise.
func (o *ConnectorTemplateResponseTemplatesInner) GetTemplateId() string {
	if o == nil || IsNil(o.TemplateId) {
		var ret string
		return ret
	}
	return *o.TemplateId
}

// GetTemplateIdOk returns a tuple with the TemplateId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorTemplateResponseTemplatesInner) GetTemplateIdOk() (*string, bool) {
	if o == nil || IsNil(o.TemplateId) {
		return nil, false
	}
	return o.TemplateId, true
}

// HasTemplateId returns a boolean if a field has been set.
func (o *ConnectorTemplateResponseTemplatesInner) HasTemplateId() bool {
	if o != nil && !IsNil(o.TemplateId) {
		return true
	}

	return false
}

// SetTemplateId gets a reference to the given string and assigns it to the TemplateId field.
func (o *ConnectorTemplateResponseTemplatesInner) SetTemplateId(v string) {
	o.TemplateId = &v
}

// GetCategoryId returns the CategoryId field value if set, zero value otherwise.
func (o *ConnectorTemplateResponseTemplatesInner) GetCategoryId() []interface{} {
	if o == nil || IsNil(o.CategoryId) {
		var ret []interface{}
		return ret
	}
	return o.CategoryId
}

// GetCategoryIdOk returns a tuple with the CategoryId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConnectorTemplateResponseTemplatesInner) GetCategoryIdOk() ([]interface{}, bool) {
	if o == nil || IsNil(o.CategoryId) {
		return nil, false
	}
	return o.CategoryId, true
}

// HasCategoryId returns a boolean if a field has been set.
func (o *ConnectorTemplateResponseTemplatesInner) HasCategoryId() bool {
	if o != nil && !IsNil(o.CategoryId) {
		return true
	}

	return false
}

// SetCategoryId gets a reference to the given []interface{} and assigns it to the CategoryId field.
func (o *ConnectorTemplateResponseTemplatesInner) SetCategoryId(v []interface{}) {
	o.CategoryId = v
}

// GetAttributes returns the Attributes field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *ConnectorTemplateResponseTemplatesInner) GetAttributes() []interface{} {
	if o == nil {
		var ret []interface{}
		return ret
	}
	return o.Attributes
}

// GetAttributesOk returns a tuple with the Attributes field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *ConnectorTemplateResponseTemplatesInner) GetAttributesOk() ([]interface{}, bool) {
	if o == nil || IsNil(o.Attributes) {
		return nil, false
	}
	return o.Attributes, true
}

// HasAttributes returns a boolean if a field has been set.
func (o *ConnectorTemplateResponseTemplatesInner) HasAttributes() bool {
	if o != nil && IsNil(o.Attributes) {
		return true
	}

	return false
}

// SetAttributes gets a reference to the given []interface{} and assigns it to the Attributes field.
func (o *ConnectorTemplateResponseTemplatesInner) SetAttributes(v []interface{}) {
	o.Attributes = v
}

func (o ConnectorTemplateResponseTemplatesInner) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ConnectorTemplateResponseTemplatesInner) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Name) {
		toSerialize["name"] = o.Name
	}
	if !IsNil(o.Description) {
		toSerialize["description"] = o.Description
	}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	if !IsNil(o.TemplateId) {
		toSerialize["templateId"] = o.TemplateId
	}
	if !IsNil(o.CategoryId) {
		toSerialize["categoryId"] = o.CategoryId
	}
	if o.Attributes != nil {
		toSerialize["attributes"] = o.Attributes
	}
	return toSerialize, nil
}

type NullableConnectorTemplateResponseTemplatesInner struct {
	value *ConnectorTemplateResponseTemplatesInner
	isSet bool
}

func (v NullableConnectorTemplateResponseTemplatesInner) Get() *ConnectorTemplateResponseTemplatesInner {
	return v.value
}

func (v *NullableConnectorTemplateResponseTemplatesInner) Set(val *ConnectorTemplateResponseTemplatesInner) {
	v.value = val
	v.isSet = true
}

func (v NullableConnectorTemplateResponseTemplatesInner) IsSet() bool {
	return v.isSet
}

func (v *NullableConnectorTemplateResponseTemplatesInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableConnectorTemplateResponseTemplatesInner(val *ConnectorTemplateResponseTemplatesInner) *NullableConnectorTemplateResponseTemplatesInner {
	return &NullableConnectorTemplateResponseTemplatesInner{value: val, isSet: true}
}

func (v NullableConnectorTemplateResponseTemplatesInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableConnectorTemplateResponseTemplatesInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
