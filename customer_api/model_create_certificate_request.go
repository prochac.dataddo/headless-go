/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the CreateCertificateRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CreateCertificateRequest{}

// CreateCertificateRequest struct for CreateCertificateRequest
type CreateCertificateRequest struct {
	Label       *string `json:"label,omitempty"`
	Type        *string `json:"type,omitempty"`
	FileContent *string `json:"fileContent,omitempty"`
	FileName    *string `json:"fileName,omitempty"`
}

// NewCreateCertificateRequest instantiates a new CreateCertificateRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreateCertificateRequest() *CreateCertificateRequest {
	this := CreateCertificateRequest{}
	return &this
}

// NewCreateCertificateRequestWithDefaults instantiates a new CreateCertificateRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreateCertificateRequestWithDefaults() *CreateCertificateRequest {
	this := CreateCertificateRequest{}
	return &this
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *CreateCertificateRequest) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCertificateRequest) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *CreateCertificateRequest) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *CreateCertificateRequest) SetLabel(v string) {
	o.Label = &v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *CreateCertificateRequest) GetType() string {
	if o == nil || IsNil(o.Type) {
		var ret string
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCertificateRequest) GetTypeOk() (*string, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *CreateCertificateRequest) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given string and assigns it to the Type field.
func (o *CreateCertificateRequest) SetType(v string) {
	o.Type = &v
}

// GetFileContent returns the FileContent field value if set, zero value otherwise.
func (o *CreateCertificateRequest) GetFileContent() string {
	if o == nil || IsNil(o.FileContent) {
		var ret string
		return ret
	}
	return *o.FileContent
}

// GetFileContentOk returns a tuple with the FileContent field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCertificateRequest) GetFileContentOk() (*string, bool) {
	if o == nil || IsNil(o.FileContent) {
		return nil, false
	}
	return o.FileContent, true
}

// HasFileContent returns a boolean if a field has been set.
func (o *CreateCertificateRequest) HasFileContent() bool {
	if o != nil && !IsNil(o.FileContent) {
		return true
	}

	return false
}

// SetFileContent gets a reference to the given string and assigns it to the FileContent field.
func (o *CreateCertificateRequest) SetFileContent(v string) {
	o.FileContent = &v
}

// GetFileName returns the FileName field value if set, zero value otherwise.
func (o *CreateCertificateRequest) GetFileName() string {
	if o == nil || IsNil(o.FileName) {
		var ret string
		return ret
	}
	return *o.FileName
}

// GetFileNameOk returns a tuple with the FileName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateCertificateRequest) GetFileNameOk() (*string, bool) {
	if o == nil || IsNil(o.FileName) {
		return nil, false
	}
	return o.FileName, true
}

// HasFileName returns a boolean if a field has been set.
func (o *CreateCertificateRequest) HasFileName() bool {
	if o != nil && !IsNil(o.FileName) {
		return true
	}

	return false
}

// SetFileName gets a reference to the given string and assigns it to the FileName field.
func (o *CreateCertificateRequest) SetFileName(v string) {
	o.FileName = &v
}

func (o CreateCertificateRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CreateCertificateRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	if !IsNil(o.FileContent) {
		toSerialize["fileContent"] = o.FileContent
	}
	if !IsNil(o.FileName) {
		toSerialize["fileName"] = o.FileName
	}
	return toSerialize, nil
}

type NullableCreateCertificateRequest struct {
	value *CreateCertificateRequest
	isSet bool
}

func (v NullableCreateCertificateRequest) Get() *CreateCertificateRequest {
	return v.value
}

func (v *NullableCreateCertificateRequest) Set(val *CreateCertificateRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableCreateCertificateRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableCreateCertificateRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreateCertificateRequest(val *CreateCertificateRequest) *NullableCreateCertificateRequest {
	return &NullableCreateCertificateRequest{value: val, isSet: true}
}

func (v NullableCreateCertificateRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreateCertificateRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
