/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the DatabricksDto type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &DatabricksDto{}

// DatabricksDto struct for DatabricksDto
type DatabricksDto struct {
	ConnectorId *string `json:"connectorId,omitempty"`
	TemplateId  *string `json:"templateId,omitempty"`
	Label       *string `json:"label,omitempty"`
	Strategy    *string `json:"strategy,omitempty"`
	OAuthId     *int32  `json:"o_auth_id,omitempty"`
	Statement   *string `json:"statement,omitempty"`
}

// NewDatabricksDto instantiates a new DatabricksDto object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewDatabricksDto() *DatabricksDto {
	this := DatabricksDto{}
	return &this
}

// NewDatabricksDtoWithDefaults instantiates a new DatabricksDto object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewDatabricksDtoWithDefaults() *DatabricksDto {
	this := DatabricksDto{}
	return &this
}

// GetConnectorId returns the ConnectorId field value if set, zero value otherwise.
func (o *DatabricksDto) GetConnectorId() string {
	if o == nil || IsNil(o.ConnectorId) {
		var ret string
		return ret
	}
	return *o.ConnectorId
}

// GetConnectorIdOk returns a tuple with the ConnectorId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DatabricksDto) GetConnectorIdOk() (*string, bool) {
	if o == nil || IsNil(o.ConnectorId) {
		return nil, false
	}
	return o.ConnectorId, true
}

// HasConnectorId returns a boolean if a field has been set.
func (o *DatabricksDto) HasConnectorId() bool {
	if o != nil && !IsNil(o.ConnectorId) {
		return true
	}

	return false
}

// SetConnectorId gets a reference to the given string and assigns it to the ConnectorId field.
func (o *DatabricksDto) SetConnectorId(v string) {
	o.ConnectorId = &v
}

// GetTemplateId returns the TemplateId field value if set, zero value otherwise.
func (o *DatabricksDto) GetTemplateId() string {
	if o == nil || IsNil(o.TemplateId) {
		var ret string
		return ret
	}
	return *o.TemplateId
}

// GetTemplateIdOk returns a tuple with the TemplateId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DatabricksDto) GetTemplateIdOk() (*string, bool) {
	if o == nil || IsNil(o.TemplateId) {
		return nil, false
	}
	return o.TemplateId, true
}

// HasTemplateId returns a boolean if a field has been set.
func (o *DatabricksDto) HasTemplateId() bool {
	if o != nil && !IsNil(o.TemplateId) {
		return true
	}

	return false
}

// SetTemplateId gets a reference to the given string and assigns it to the TemplateId field.
func (o *DatabricksDto) SetTemplateId(v string) {
	o.TemplateId = &v
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *DatabricksDto) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DatabricksDto) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *DatabricksDto) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *DatabricksDto) SetLabel(v string) {
	o.Label = &v
}

// GetStrategy returns the Strategy field value if set, zero value otherwise.
func (o *DatabricksDto) GetStrategy() string {
	if o == nil || IsNil(o.Strategy) {
		var ret string
		return ret
	}
	return *o.Strategy
}

// GetStrategyOk returns a tuple with the Strategy field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DatabricksDto) GetStrategyOk() (*string, bool) {
	if o == nil || IsNil(o.Strategy) {
		return nil, false
	}
	return o.Strategy, true
}

// HasStrategy returns a boolean if a field has been set.
func (o *DatabricksDto) HasStrategy() bool {
	if o != nil && !IsNil(o.Strategy) {
		return true
	}

	return false
}

// SetStrategy gets a reference to the given string and assigns it to the Strategy field.
func (o *DatabricksDto) SetStrategy(v string) {
	o.Strategy = &v
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise.
func (o *DatabricksDto) GetOAuthId() int32 {
	if o == nil || IsNil(o.OAuthId) {
		var ret int32
		return ret
	}
	return *o.OAuthId
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DatabricksDto) GetOAuthIdOk() (*int32, bool) {
	if o == nil || IsNil(o.OAuthId) {
		return nil, false
	}
	return o.OAuthId, true
}

// HasOAuthId returns a boolean if a field has been set.
func (o *DatabricksDto) HasOAuthId() bool {
	if o != nil && !IsNil(o.OAuthId) {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given int32 and assigns it to the OAuthId field.
func (o *DatabricksDto) SetOAuthId(v int32) {
	o.OAuthId = &v
}

// GetStatement returns the Statement field value if set, zero value otherwise.
func (o *DatabricksDto) GetStatement() string {
	if o == nil || IsNil(o.Statement) {
		var ret string
		return ret
	}
	return *o.Statement
}

// GetStatementOk returns a tuple with the Statement field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DatabricksDto) GetStatementOk() (*string, bool) {
	if o == nil || IsNil(o.Statement) {
		return nil, false
	}
	return o.Statement, true
}

// HasStatement returns a boolean if a field has been set.
func (o *DatabricksDto) HasStatement() bool {
	if o != nil && !IsNil(o.Statement) {
		return true
	}

	return false
}

// SetStatement gets a reference to the given string and assigns it to the Statement field.
func (o *DatabricksDto) SetStatement(v string) {
	o.Statement = &v
}

func (o DatabricksDto) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o DatabricksDto) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.ConnectorId) {
		toSerialize["connectorId"] = o.ConnectorId
	}
	if !IsNil(o.TemplateId) {
		toSerialize["templateId"] = o.TemplateId
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.Strategy) {
		toSerialize["strategy"] = o.Strategy
	}
	if !IsNil(o.OAuthId) {
		toSerialize["o_auth_id"] = o.OAuthId
	}
	if !IsNil(o.Statement) {
		toSerialize["statement"] = o.Statement
	}
	return toSerialize, nil
}

type NullableDatabricksDto struct {
	value *DatabricksDto
	isSet bool
}

func (v NullableDatabricksDto) Get() *DatabricksDto {
	return v.value
}

func (v *NullableDatabricksDto) Set(val *DatabricksDto) {
	v.value = val
	v.isSet = true
}

func (v NullableDatabricksDto) IsSet() bool {
	return v.isSet
}

func (v *NullableDatabricksDto) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableDatabricksDto(val *DatabricksDto) *NullableDatabricksDto {
	return &NullableDatabricksDto{value: val, isSet: true}
}

func (v NullableDatabricksDto) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableDatabricksDto) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
