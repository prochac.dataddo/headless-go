/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the DocumentStaticKeyTrait type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &DocumentStaticKeyTrait{}

// DocumentStaticKeyTrait struct for DocumentStaticKeyTrait
type DocumentStaticKeyTrait struct {
	StaticKey *string `json:"staticKey,omitempty"`
}

// NewDocumentStaticKeyTrait instantiates a new DocumentStaticKeyTrait object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewDocumentStaticKeyTrait() *DocumentStaticKeyTrait {
	this := DocumentStaticKeyTrait{}
	return &this
}

// NewDocumentStaticKeyTraitWithDefaults instantiates a new DocumentStaticKeyTrait object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewDocumentStaticKeyTraitWithDefaults() *DocumentStaticKeyTrait {
	this := DocumentStaticKeyTrait{}
	return &this
}

// GetStaticKey returns the StaticKey field value if set, zero value otherwise.
func (o *DocumentStaticKeyTrait) GetStaticKey() string {
	if o == nil || IsNil(o.StaticKey) {
		var ret string
		return ret
	}
	return *o.StaticKey
}

// GetStaticKeyOk returns a tuple with the StaticKey field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DocumentStaticKeyTrait) GetStaticKeyOk() (*string, bool) {
	if o == nil || IsNil(o.StaticKey) {
		return nil, false
	}
	return o.StaticKey, true
}

// HasStaticKey returns a boolean if a field has been set.
func (o *DocumentStaticKeyTrait) HasStaticKey() bool {
	if o != nil && !IsNil(o.StaticKey) {
		return true
	}

	return false
}

// SetStaticKey gets a reference to the given string and assigns it to the StaticKey field.
func (o *DocumentStaticKeyTrait) SetStaticKey(v string) {
	o.StaticKey = &v
}

func (o DocumentStaticKeyTrait) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o DocumentStaticKeyTrait) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.StaticKey) {
		toSerialize["staticKey"] = o.StaticKey
	}
	return toSerialize, nil
}

type NullableDocumentStaticKeyTrait struct {
	value *DocumentStaticKeyTrait
	isSet bool
}

func (v NullableDocumentStaticKeyTrait) Get() *DocumentStaticKeyTrait {
	return v.value
}

func (v *NullableDocumentStaticKeyTrait) Set(val *DocumentStaticKeyTrait) {
	v.value = val
	v.isSet = true
}

func (v NullableDocumentStaticKeyTrait) IsSet() bool {
	return v.isSet
}

func (v *NullableDocumentStaticKeyTrait) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableDocumentStaticKeyTrait(val *DocumentStaticKeyTrait) *NullableDocumentStaticKeyTrait {
	return &NullableDocumentStaticKeyTrait{value: val, isSet: true}
}

func (v NullableDocumentStaticKeyTrait) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableDocumentStaticKeyTrait) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
