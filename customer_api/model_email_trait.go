/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the EmailTrait type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &EmailTrait{}

// EmailTrait struct for EmailTrait
type EmailTrait struct {
	Email *string `json:"email,omitempty"`
}

// NewEmailTrait instantiates a new EmailTrait object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewEmailTrait() *EmailTrait {
	this := EmailTrait{}
	return &this
}

// NewEmailTraitWithDefaults instantiates a new EmailTrait object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewEmailTraitWithDefaults() *EmailTrait {
	this := EmailTrait{}
	return &this
}

// GetEmail returns the Email field value if set, zero value otherwise.
func (o *EmailTrait) GetEmail() string {
	if o == nil || IsNil(o.Email) {
		var ret string
		return ret
	}
	return *o.Email
}

// GetEmailOk returns a tuple with the Email field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *EmailTrait) GetEmailOk() (*string, bool) {
	if o == nil || IsNil(o.Email) {
		return nil, false
	}
	return o.Email, true
}

// HasEmail returns a boolean if a field has been set.
func (o *EmailTrait) HasEmail() bool {
	if o != nil && !IsNil(o.Email) {
		return true
	}

	return false
}

// SetEmail gets a reference to the given string and assigns it to the Email field.
func (o *EmailTrait) SetEmail(v string) {
	o.Email = &v
}

func (o EmailTrait) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o EmailTrait) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Email) {
		toSerialize["email"] = o.Email
	}
	return toSerialize, nil
}

type NullableEmailTrait struct {
	value *EmailTrait
	isSet bool
}

func (v NullableEmailTrait) Get() *EmailTrait {
	return v.value
}

func (v *NullableEmailTrait) Set(val *EmailTrait) {
	v.value = val
	v.isSet = true
}

func (v NullableEmailTrait) IsSet() bool {
	return v.isSet
}

func (v *NullableEmailTrait) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableEmailTrait(val *EmailTrait) *NullableEmailTrait {
	return &NullableEmailTrait{value: val, isSet: true}
}

func (v NullableEmailTrait) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableEmailTrait) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
