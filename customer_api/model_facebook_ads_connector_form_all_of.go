/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the FacebookAdsConnectorFormAllOf type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &FacebookAdsConnectorFormAllOf{}

// FacebookAdsConnectorFormAllOf struct for FacebookAdsConnectorFormAllOf
type FacebookAdsConnectorFormAllOf struct {
	// Authorizer ID
	OAuthId *string `json:"oAuthId,omitempty"`
	// Label of the source
	Label *string `json:"label,omitempty"`
	// Facebook Ads Account ID.
	AccountId *string `json:"accountId,omitempty"`
	// Facebook campaign ID
	CampaignId *string `json:"campaignId,omitempty"`
	// Facebook Ad Set Id
	AdSetId *string `json:"adSetId,omitempty"`
	// Data range expression
	DateRange *string `json:"dateRange,omitempty"`
	// Time break-up
	TimeBreakup *string `json:"timeBreakup,omitempty"`
	// Reporting level
	Level *string `json:"level,omitempty"`
	// Metrics
	Metric []string `json:"metric,omitempty"`
	// Breakdown
	Breakdown []string `json:"breakdown,omitempty"`
	// Data labels
	DataLabel []string `json:"dataLabel,omitempty"`
}

// NewFacebookAdsConnectorFormAllOf instantiates a new FacebookAdsConnectorFormAllOf object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFacebookAdsConnectorFormAllOf() *FacebookAdsConnectorFormAllOf {
	this := FacebookAdsConnectorFormAllOf{}
	var timeBreakup string = "all_days"
	this.TimeBreakup = &timeBreakup
	return &this
}

// NewFacebookAdsConnectorFormAllOfWithDefaults instantiates a new FacebookAdsConnectorFormAllOf object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFacebookAdsConnectorFormAllOfWithDefaults() *FacebookAdsConnectorFormAllOf {
	this := FacebookAdsConnectorFormAllOf{}
	var timeBreakup string = "all_days"
	this.TimeBreakup = &timeBreakup
	return &this
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetOAuthId() string {
	if o == nil || IsNil(o.OAuthId) {
		var ret string
		return ret
	}
	return *o.OAuthId
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetOAuthIdOk() (*string, bool) {
	if o == nil || IsNil(o.OAuthId) {
		return nil, false
	}
	return o.OAuthId, true
}

// HasOAuthId returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasOAuthId() bool {
	if o != nil && !IsNil(o.OAuthId) {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given string and assigns it to the OAuthId field.
func (o *FacebookAdsConnectorFormAllOf) SetOAuthId(v string) {
	o.OAuthId = &v
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *FacebookAdsConnectorFormAllOf) SetLabel(v string) {
	o.Label = &v
}

// GetAccountId returns the AccountId field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetAccountId() string {
	if o == nil || IsNil(o.AccountId) {
		var ret string
		return ret
	}
	return *o.AccountId
}

// GetAccountIdOk returns a tuple with the AccountId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetAccountIdOk() (*string, bool) {
	if o == nil || IsNil(o.AccountId) {
		return nil, false
	}
	return o.AccountId, true
}

// HasAccountId returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasAccountId() bool {
	if o != nil && !IsNil(o.AccountId) {
		return true
	}

	return false
}

// SetAccountId gets a reference to the given string and assigns it to the AccountId field.
func (o *FacebookAdsConnectorFormAllOf) SetAccountId(v string) {
	o.AccountId = &v
}

// GetCampaignId returns the CampaignId field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetCampaignId() string {
	if o == nil || IsNil(o.CampaignId) {
		var ret string
		return ret
	}
	return *o.CampaignId
}

// GetCampaignIdOk returns a tuple with the CampaignId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetCampaignIdOk() (*string, bool) {
	if o == nil || IsNil(o.CampaignId) {
		return nil, false
	}
	return o.CampaignId, true
}

// HasCampaignId returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasCampaignId() bool {
	if o != nil && !IsNil(o.CampaignId) {
		return true
	}

	return false
}

// SetCampaignId gets a reference to the given string and assigns it to the CampaignId field.
func (o *FacebookAdsConnectorFormAllOf) SetCampaignId(v string) {
	o.CampaignId = &v
}

// GetAdSetId returns the AdSetId field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetAdSetId() string {
	if o == nil || IsNil(o.AdSetId) {
		var ret string
		return ret
	}
	return *o.AdSetId
}

// GetAdSetIdOk returns a tuple with the AdSetId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetAdSetIdOk() (*string, bool) {
	if o == nil || IsNil(o.AdSetId) {
		return nil, false
	}
	return o.AdSetId, true
}

// HasAdSetId returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasAdSetId() bool {
	if o != nil && !IsNil(o.AdSetId) {
		return true
	}

	return false
}

// SetAdSetId gets a reference to the given string and assigns it to the AdSetId field.
func (o *FacebookAdsConnectorFormAllOf) SetAdSetId(v string) {
	o.AdSetId = &v
}

// GetDateRange returns the DateRange field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetDateRange() string {
	if o == nil || IsNil(o.DateRange) {
		var ret string
		return ret
	}
	return *o.DateRange
}

// GetDateRangeOk returns a tuple with the DateRange field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetDateRangeOk() (*string, bool) {
	if o == nil || IsNil(o.DateRange) {
		return nil, false
	}
	return o.DateRange, true
}

// HasDateRange returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasDateRange() bool {
	if o != nil && !IsNil(o.DateRange) {
		return true
	}

	return false
}

// SetDateRange gets a reference to the given string and assigns it to the DateRange field.
func (o *FacebookAdsConnectorFormAllOf) SetDateRange(v string) {
	o.DateRange = &v
}

// GetTimeBreakup returns the TimeBreakup field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetTimeBreakup() string {
	if o == nil || IsNil(o.TimeBreakup) {
		var ret string
		return ret
	}
	return *o.TimeBreakup
}

// GetTimeBreakupOk returns a tuple with the TimeBreakup field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetTimeBreakupOk() (*string, bool) {
	if o == nil || IsNil(o.TimeBreakup) {
		return nil, false
	}
	return o.TimeBreakup, true
}

// HasTimeBreakup returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasTimeBreakup() bool {
	if o != nil && !IsNil(o.TimeBreakup) {
		return true
	}

	return false
}

// SetTimeBreakup gets a reference to the given string and assigns it to the TimeBreakup field.
func (o *FacebookAdsConnectorFormAllOf) SetTimeBreakup(v string) {
	o.TimeBreakup = &v
}

// GetLevel returns the Level field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetLevel() string {
	if o == nil || IsNil(o.Level) {
		var ret string
		return ret
	}
	return *o.Level
}

// GetLevelOk returns a tuple with the Level field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetLevelOk() (*string, bool) {
	if o == nil || IsNil(o.Level) {
		return nil, false
	}
	return o.Level, true
}

// HasLevel returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasLevel() bool {
	if o != nil && !IsNil(o.Level) {
		return true
	}

	return false
}

// SetLevel gets a reference to the given string and assigns it to the Level field.
func (o *FacebookAdsConnectorFormAllOf) SetLevel(v string) {
	o.Level = &v
}

// GetMetric returns the Metric field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetMetric() []string {
	if o == nil || IsNil(o.Metric) {
		var ret []string
		return ret
	}
	return o.Metric
}

// GetMetricOk returns a tuple with the Metric field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetMetricOk() ([]string, bool) {
	if o == nil || IsNil(o.Metric) {
		return nil, false
	}
	return o.Metric, true
}

// HasMetric returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasMetric() bool {
	if o != nil && !IsNil(o.Metric) {
		return true
	}

	return false
}

// SetMetric gets a reference to the given []string and assigns it to the Metric field.
func (o *FacebookAdsConnectorFormAllOf) SetMetric(v []string) {
	o.Metric = v
}

// GetBreakdown returns the Breakdown field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetBreakdown() []string {
	if o == nil || IsNil(o.Breakdown) {
		var ret []string
		return ret
	}
	return o.Breakdown
}

// GetBreakdownOk returns a tuple with the Breakdown field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetBreakdownOk() ([]string, bool) {
	if o == nil || IsNil(o.Breakdown) {
		return nil, false
	}
	return o.Breakdown, true
}

// HasBreakdown returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasBreakdown() bool {
	if o != nil && !IsNil(o.Breakdown) {
		return true
	}

	return false
}

// SetBreakdown gets a reference to the given []string and assigns it to the Breakdown field.
func (o *FacebookAdsConnectorFormAllOf) SetBreakdown(v []string) {
	o.Breakdown = v
}

// GetDataLabel returns the DataLabel field value if set, zero value otherwise.
func (o *FacebookAdsConnectorFormAllOf) GetDataLabel() []string {
	if o == nil || IsNil(o.DataLabel) {
		var ret []string
		return ret
	}
	return o.DataLabel
}

// GetDataLabelOk returns a tuple with the DataLabel field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookAdsConnectorFormAllOf) GetDataLabelOk() ([]string, bool) {
	if o == nil || IsNil(o.DataLabel) {
		return nil, false
	}
	return o.DataLabel, true
}

// HasDataLabel returns a boolean if a field has been set.
func (o *FacebookAdsConnectorFormAllOf) HasDataLabel() bool {
	if o != nil && !IsNil(o.DataLabel) {
		return true
	}

	return false
}

// SetDataLabel gets a reference to the given []string and assigns it to the DataLabel field.
func (o *FacebookAdsConnectorFormAllOf) SetDataLabel(v []string) {
	o.DataLabel = v
}

func (o FacebookAdsConnectorFormAllOf) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o FacebookAdsConnectorFormAllOf) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.OAuthId) {
		toSerialize["oAuthId"] = o.OAuthId
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.AccountId) {
		toSerialize["accountId"] = o.AccountId
	}
	if !IsNil(o.CampaignId) {
		toSerialize["campaignId"] = o.CampaignId
	}
	if !IsNil(o.AdSetId) {
		toSerialize["adSetId"] = o.AdSetId
	}
	if !IsNil(o.DateRange) {
		toSerialize["dateRange"] = o.DateRange
	}
	if !IsNil(o.TimeBreakup) {
		toSerialize["timeBreakup"] = o.TimeBreakup
	}
	if !IsNil(o.Level) {
		toSerialize["level"] = o.Level
	}
	if !IsNil(o.Metric) {
		toSerialize["metric"] = o.Metric
	}
	if !IsNil(o.Breakdown) {
		toSerialize["breakdown"] = o.Breakdown
	}
	if !IsNil(o.DataLabel) {
		toSerialize["dataLabel"] = o.DataLabel
	}
	return toSerialize, nil
}

type NullableFacebookAdsConnectorFormAllOf struct {
	value *FacebookAdsConnectorFormAllOf
	isSet bool
}

func (v NullableFacebookAdsConnectorFormAllOf) Get() *FacebookAdsConnectorFormAllOf {
	return v.value
}

func (v *NullableFacebookAdsConnectorFormAllOf) Set(val *FacebookAdsConnectorFormAllOf) {
	v.value = val
	v.isSet = true
}

func (v NullableFacebookAdsConnectorFormAllOf) IsSet() bool {
	return v.isSet
}

func (v *NullableFacebookAdsConnectorFormAllOf) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFacebookAdsConnectorFormAllOf(val *FacebookAdsConnectorFormAllOf) *NullableFacebookAdsConnectorFormAllOf {
	return &NullableFacebookAdsConnectorFormAllOf{value: val, isSet: true}
}

func (v NullableFacebookAdsConnectorFormAllOf) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFacebookAdsConnectorFormAllOf) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
