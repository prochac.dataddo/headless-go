/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the FacebookStaticDtoAuthorizerAllOf type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &FacebookStaticDtoAuthorizerAllOf{}

// FacebookStaticDtoAuthorizerAllOf struct for FacebookStaticDtoAuthorizerAllOf
type FacebookStaticDtoAuthorizerAllOf struct {
	Label *string `json:"label,omitempty"`
}

// NewFacebookStaticDtoAuthorizerAllOf instantiates a new FacebookStaticDtoAuthorizerAllOf object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFacebookStaticDtoAuthorizerAllOf() *FacebookStaticDtoAuthorizerAllOf {
	this := FacebookStaticDtoAuthorizerAllOf{}
	return &this
}

// NewFacebookStaticDtoAuthorizerAllOfWithDefaults instantiates a new FacebookStaticDtoAuthorizerAllOf object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFacebookStaticDtoAuthorizerAllOfWithDefaults() *FacebookStaticDtoAuthorizerAllOf {
	this := FacebookStaticDtoAuthorizerAllOf{}
	return &this
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *FacebookStaticDtoAuthorizerAllOf) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FacebookStaticDtoAuthorizerAllOf) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *FacebookStaticDtoAuthorizerAllOf) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *FacebookStaticDtoAuthorizerAllOf) SetLabel(v string) {
	o.Label = &v
}

func (o FacebookStaticDtoAuthorizerAllOf) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o FacebookStaticDtoAuthorizerAllOf) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	return toSerialize, nil
}

type NullableFacebookStaticDtoAuthorizerAllOf struct {
	value *FacebookStaticDtoAuthorizerAllOf
	isSet bool
}

func (v NullableFacebookStaticDtoAuthorizerAllOf) Get() *FacebookStaticDtoAuthorizerAllOf {
	return v.value
}

func (v *NullableFacebookStaticDtoAuthorizerAllOf) Set(val *FacebookStaticDtoAuthorizerAllOf) {
	v.value = val
	v.isSet = true
}

func (v NullableFacebookStaticDtoAuthorizerAllOf) IsSet() bool {
	return v.isSet
}

func (v *NullableFacebookStaticDtoAuthorizerAllOf) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFacebookStaticDtoAuthorizerAllOf(val *FacebookStaticDtoAuthorizerAllOf) *NullableFacebookStaticDtoAuthorizerAllOf {
	return &NullableFacebookStaticDtoAuthorizerAllOf{value: val, isSet: true}
}

func (v NullableFacebookStaticDtoAuthorizerAllOf) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFacebookStaticDtoAuthorizerAllOf) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
