/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the FilterRuleRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &FilterRuleRequest{}

// FilterRuleRequest struct for FilterRuleRequest
type FilterRuleRequest struct {
	Column     *string `json:"column,omitempty"`
	Comparator *string `json:"comparator,omitempty"`
	Value      *string `json:"value,omitempty"`
}

// NewFilterRuleRequest instantiates a new FilterRuleRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFilterRuleRequest() *FilterRuleRequest {
	this := FilterRuleRequest{}
	return &this
}

// NewFilterRuleRequestWithDefaults instantiates a new FilterRuleRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFilterRuleRequestWithDefaults() *FilterRuleRequest {
	this := FilterRuleRequest{}
	return &this
}

// GetColumn returns the Column field value if set, zero value otherwise.
func (o *FilterRuleRequest) GetColumn() string {
	if o == nil || IsNil(o.Column) {
		var ret string
		return ret
	}
	return *o.Column
}

// GetColumnOk returns a tuple with the Column field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FilterRuleRequest) GetColumnOk() (*string, bool) {
	if o == nil || IsNil(o.Column) {
		return nil, false
	}
	return o.Column, true
}

// HasColumn returns a boolean if a field has been set.
func (o *FilterRuleRequest) HasColumn() bool {
	if o != nil && !IsNil(o.Column) {
		return true
	}

	return false
}

// SetColumn gets a reference to the given string and assigns it to the Column field.
func (o *FilterRuleRequest) SetColumn(v string) {
	o.Column = &v
}

// GetComparator returns the Comparator field value if set, zero value otherwise.
func (o *FilterRuleRequest) GetComparator() string {
	if o == nil || IsNil(o.Comparator) {
		var ret string
		return ret
	}
	return *o.Comparator
}

// GetComparatorOk returns a tuple with the Comparator field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FilterRuleRequest) GetComparatorOk() (*string, bool) {
	if o == nil || IsNil(o.Comparator) {
		return nil, false
	}
	return o.Comparator, true
}

// HasComparator returns a boolean if a field has been set.
func (o *FilterRuleRequest) HasComparator() bool {
	if o != nil && !IsNil(o.Comparator) {
		return true
	}

	return false
}

// SetComparator gets a reference to the given string and assigns it to the Comparator field.
func (o *FilterRuleRequest) SetComparator(v string) {
	o.Comparator = &v
}

// GetValue returns the Value field value if set, zero value otherwise.
func (o *FilterRuleRequest) GetValue() string {
	if o == nil || IsNil(o.Value) {
		var ret string
		return ret
	}
	return *o.Value
}

// GetValueOk returns a tuple with the Value field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FilterRuleRequest) GetValueOk() (*string, bool) {
	if o == nil || IsNil(o.Value) {
		return nil, false
	}
	return o.Value, true
}

// HasValue returns a boolean if a field has been set.
func (o *FilterRuleRequest) HasValue() bool {
	if o != nil && !IsNil(o.Value) {
		return true
	}

	return false
}

// SetValue gets a reference to the given string and assigns it to the Value field.
func (o *FilterRuleRequest) SetValue(v string) {
	o.Value = &v
}

func (o FilterRuleRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o FilterRuleRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Column) {
		toSerialize["column"] = o.Column
	}
	if !IsNil(o.Comparator) {
		toSerialize["comparator"] = o.Comparator
	}
	if !IsNil(o.Value) {
		toSerialize["value"] = o.Value
	}
	return toSerialize, nil
}

type NullableFilterRuleRequest struct {
	value *FilterRuleRequest
	isSet bool
}

func (v NullableFilterRuleRequest) Get() *FilterRuleRequest {
	return v.value
}

func (v *NullableFilterRuleRequest) Set(val *FilterRuleRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableFilterRuleRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableFilterRuleRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFilterRuleRequest(val *FilterRuleRequest) *NullableFilterRuleRequest {
	return &NullableFilterRuleRequest{value: val, isSet: true}
}

func (v NullableFilterRuleRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFilterRuleRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
