/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the FlowOperation type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &FlowOperation{}

// FlowOperation struct for FlowOperation
type FlowOperation struct {
	// The operation name (left_join, right_join, sort, ...)
	Name *string `json:"name,omitempty"`
	// Name of the output. By this name it may be used in following operation as a source.
	Output    *string                        `json:"output,omitempty"`
	Union     NullableFlowOperationUnion     `json:"union,omitempty"`
	LeftJoin  NullableFlowOperationLeftJoin  `json:"left_join,omitempty"`
	InnerJoin NullableFlowOperationInnerJoin `json:"inner_join,omitempty"`
	View      NullableFlowOperationView      `json:"view,omitempty"`
	Filter    NullableFlowOperationFilter    `json:"filter,omitempty"`
	Limit     NullableFlowOperationLimit     `json:"limit,omitempty"`
	Format    NullableFlowOperationFormat    `json:"format,omitempty"`
	Sort      NullableFlowOperationSort      `json:"sort,omitempty"`
}

// NewFlowOperation instantiates a new FlowOperation object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFlowOperation() *FlowOperation {
	this := FlowOperation{}
	return &this
}

// NewFlowOperationWithDefaults instantiates a new FlowOperation object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFlowOperationWithDefaults() *FlowOperation {
	this := FlowOperation{}
	return &this
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *FlowOperation) GetName() string {
	if o == nil || IsNil(o.Name) {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperation) GetNameOk() (*string, bool) {
	if o == nil || IsNil(o.Name) {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *FlowOperation) HasName() bool {
	if o != nil && !IsNil(o.Name) {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *FlowOperation) SetName(v string) {
	o.Name = &v
}

// GetOutput returns the Output field value if set, zero value otherwise.
func (o *FlowOperation) GetOutput() string {
	if o == nil || IsNil(o.Output) {
		var ret string
		return ret
	}
	return *o.Output
}

// GetOutputOk returns a tuple with the Output field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperation) GetOutputOk() (*string, bool) {
	if o == nil || IsNil(o.Output) {
		return nil, false
	}
	return o.Output, true
}

// HasOutput returns a boolean if a field has been set.
func (o *FlowOperation) HasOutput() bool {
	if o != nil && !IsNil(o.Output) {
		return true
	}

	return false
}

// SetOutput gets a reference to the given string and assigns it to the Output field.
func (o *FlowOperation) SetOutput(v string) {
	o.Output = &v
}

// GetUnion returns the Union field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FlowOperation) GetUnion() FlowOperationUnion {
	if o == nil || IsNil(o.Union.Get()) {
		var ret FlowOperationUnion
		return ret
	}
	return *o.Union.Get()
}

// GetUnionOk returns a tuple with the Union field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FlowOperation) GetUnionOk() (*FlowOperationUnion, bool) {
	if o == nil {
		return nil, false
	}
	return o.Union.Get(), o.Union.IsSet()
}

// HasUnion returns a boolean if a field has been set.
func (o *FlowOperation) HasUnion() bool {
	if o != nil && o.Union.IsSet() {
		return true
	}

	return false
}

// SetUnion gets a reference to the given NullableFlowOperationUnion and assigns it to the Union field.
func (o *FlowOperation) SetUnion(v FlowOperationUnion) {
	o.Union.Set(&v)
}

// SetUnionNil sets the value for Union to be an explicit nil
func (o *FlowOperation) SetUnionNil() {
	o.Union.Set(nil)
}

// UnsetUnion ensures that no value is present for Union, not even an explicit nil
func (o *FlowOperation) UnsetUnion() {
	o.Union.Unset()
}

// GetLeftJoin returns the LeftJoin field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FlowOperation) GetLeftJoin() FlowOperationLeftJoin {
	if o == nil || IsNil(o.LeftJoin.Get()) {
		var ret FlowOperationLeftJoin
		return ret
	}
	return *o.LeftJoin.Get()
}

// GetLeftJoinOk returns a tuple with the LeftJoin field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FlowOperation) GetLeftJoinOk() (*FlowOperationLeftJoin, bool) {
	if o == nil {
		return nil, false
	}
	return o.LeftJoin.Get(), o.LeftJoin.IsSet()
}

// HasLeftJoin returns a boolean if a field has been set.
func (o *FlowOperation) HasLeftJoin() bool {
	if o != nil && o.LeftJoin.IsSet() {
		return true
	}

	return false
}

// SetLeftJoin gets a reference to the given NullableFlowOperationLeftJoin and assigns it to the LeftJoin field.
func (o *FlowOperation) SetLeftJoin(v FlowOperationLeftJoin) {
	o.LeftJoin.Set(&v)
}

// SetLeftJoinNil sets the value for LeftJoin to be an explicit nil
func (o *FlowOperation) SetLeftJoinNil() {
	o.LeftJoin.Set(nil)
}

// UnsetLeftJoin ensures that no value is present for LeftJoin, not even an explicit nil
func (o *FlowOperation) UnsetLeftJoin() {
	o.LeftJoin.Unset()
}

// GetInnerJoin returns the InnerJoin field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FlowOperation) GetInnerJoin() FlowOperationInnerJoin {
	if o == nil || IsNil(o.InnerJoin.Get()) {
		var ret FlowOperationInnerJoin
		return ret
	}
	return *o.InnerJoin.Get()
}

// GetInnerJoinOk returns a tuple with the InnerJoin field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FlowOperation) GetInnerJoinOk() (*FlowOperationInnerJoin, bool) {
	if o == nil {
		return nil, false
	}
	return o.InnerJoin.Get(), o.InnerJoin.IsSet()
}

// HasInnerJoin returns a boolean if a field has been set.
func (o *FlowOperation) HasInnerJoin() bool {
	if o != nil && o.InnerJoin.IsSet() {
		return true
	}

	return false
}

// SetInnerJoin gets a reference to the given NullableFlowOperationInnerJoin and assigns it to the InnerJoin field.
func (o *FlowOperation) SetInnerJoin(v FlowOperationInnerJoin) {
	o.InnerJoin.Set(&v)
}

// SetInnerJoinNil sets the value for InnerJoin to be an explicit nil
func (o *FlowOperation) SetInnerJoinNil() {
	o.InnerJoin.Set(nil)
}

// UnsetInnerJoin ensures that no value is present for InnerJoin, not even an explicit nil
func (o *FlowOperation) UnsetInnerJoin() {
	o.InnerJoin.Unset()
}

// GetView returns the View field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FlowOperation) GetView() FlowOperationView {
	if o == nil || IsNil(o.View.Get()) {
		var ret FlowOperationView
		return ret
	}
	return *o.View.Get()
}

// GetViewOk returns a tuple with the View field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FlowOperation) GetViewOk() (*FlowOperationView, bool) {
	if o == nil {
		return nil, false
	}
	return o.View.Get(), o.View.IsSet()
}

// HasView returns a boolean if a field has been set.
func (o *FlowOperation) HasView() bool {
	if o != nil && o.View.IsSet() {
		return true
	}

	return false
}

// SetView gets a reference to the given NullableFlowOperationView and assigns it to the View field.
func (o *FlowOperation) SetView(v FlowOperationView) {
	o.View.Set(&v)
}

// SetViewNil sets the value for View to be an explicit nil
func (o *FlowOperation) SetViewNil() {
	o.View.Set(nil)
}

// UnsetView ensures that no value is present for View, not even an explicit nil
func (o *FlowOperation) UnsetView() {
	o.View.Unset()
}

// GetFilter returns the Filter field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FlowOperation) GetFilter() FlowOperationFilter {
	if o == nil || IsNil(o.Filter.Get()) {
		var ret FlowOperationFilter
		return ret
	}
	return *o.Filter.Get()
}

// GetFilterOk returns a tuple with the Filter field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FlowOperation) GetFilterOk() (*FlowOperationFilter, bool) {
	if o == nil {
		return nil, false
	}
	return o.Filter.Get(), o.Filter.IsSet()
}

// HasFilter returns a boolean if a field has been set.
func (o *FlowOperation) HasFilter() bool {
	if o != nil && o.Filter.IsSet() {
		return true
	}

	return false
}

// SetFilter gets a reference to the given NullableFlowOperationFilter and assigns it to the Filter field.
func (o *FlowOperation) SetFilter(v FlowOperationFilter) {
	o.Filter.Set(&v)
}

// SetFilterNil sets the value for Filter to be an explicit nil
func (o *FlowOperation) SetFilterNil() {
	o.Filter.Set(nil)
}

// UnsetFilter ensures that no value is present for Filter, not even an explicit nil
func (o *FlowOperation) UnsetFilter() {
	o.Filter.Unset()
}

// GetLimit returns the Limit field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FlowOperation) GetLimit() FlowOperationLimit {
	if o == nil || IsNil(o.Limit.Get()) {
		var ret FlowOperationLimit
		return ret
	}
	return *o.Limit.Get()
}

// GetLimitOk returns a tuple with the Limit field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FlowOperation) GetLimitOk() (*FlowOperationLimit, bool) {
	if o == nil {
		return nil, false
	}
	return o.Limit.Get(), o.Limit.IsSet()
}

// HasLimit returns a boolean if a field has been set.
func (o *FlowOperation) HasLimit() bool {
	if o != nil && o.Limit.IsSet() {
		return true
	}

	return false
}

// SetLimit gets a reference to the given NullableFlowOperationLimit and assigns it to the Limit field.
func (o *FlowOperation) SetLimit(v FlowOperationLimit) {
	o.Limit.Set(&v)
}

// SetLimitNil sets the value for Limit to be an explicit nil
func (o *FlowOperation) SetLimitNil() {
	o.Limit.Set(nil)
}

// UnsetLimit ensures that no value is present for Limit, not even an explicit nil
func (o *FlowOperation) UnsetLimit() {
	o.Limit.Unset()
}

// GetFormat returns the Format field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FlowOperation) GetFormat() FlowOperationFormat {
	if o == nil || IsNil(o.Format.Get()) {
		var ret FlowOperationFormat
		return ret
	}
	return *o.Format.Get()
}

// GetFormatOk returns a tuple with the Format field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FlowOperation) GetFormatOk() (*FlowOperationFormat, bool) {
	if o == nil {
		return nil, false
	}
	return o.Format.Get(), o.Format.IsSet()
}

// HasFormat returns a boolean if a field has been set.
func (o *FlowOperation) HasFormat() bool {
	if o != nil && o.Format.IsSet() {
		return true
	}

	return false
}

// SetFormat gets a reference to the given NullableFlowOperationFormat and assigns it to the Format field.
func (o *FlowOperation) SetFormat(v FlowOperationFormat) {
	o.Format.Set(&v)
}

// SetFormatNil sets the value for Format to be an explicit nil
func (o *FlowOperation) SetFormatNil() {
	o.Format.Set(nil)
}

// UnsetFormat ensures that no value is present for Format, not even an explicit nil
func (o *FlowOperation) UnsetFormat() {
	o.Format.Unset()
}

// GetSort returns the Sort field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FlowOperation) GetSort() FlowOperationSort {
	if o == nil || IsNil(o.Sort.Get()) {
		var ret FlowOperationSort
		return ret
	}
	return *o.Sort.Get()
}

// GetSortOk returns a tuple with the Sort field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FlowOperation) GetSortOk() (*FlowOperationSort, bool) {
	if o == nil {
		return nil, false
	}
	return o.Sort.Get(), o.Sort.IsSet()
}

// HasSort returns a boolean if a field has been set.
func (o *FlowOperation) HasSort() bool {
	if o != nil && o.Sort.IsSet() {
		return true
	}

	return false
}

// SetSort gets a reference to the given NullableFlowOperationSort and assigns it to the Sort field.
func (o *FlowOperation) SetSort(v FlowOperationSort) {
	o.Sort.Set(&v)
}

// SetSortNil sets the value for Sort to be an explicit nil
func (o *FlowOperation) SetSortNil() {
	o.Sort.Set(nil)
}

// UnsetSort ensures that no value is present for Sort, not even an explicit nil
func (o *FlowOperation) UnsetSort() {
	o.Sort.Unset()
}

func (o FlowOperation) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o FlowOperation) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Name) {
		toSerialize["name"] = o.Name
	}
	if !IsNil(o.Output) {
		toSerialize["output"] = o.Output
	}
	if o.Union.IsSet() {
		toSerialize["union"] = o.Union.Get()
	}
	if o.LeftJoin.IsSet() {
		toSerialize["left_join"] = o.LeftJoin.Get()
	}
	if o.InnerJoin.IsSet() {
		toSerialize["inner_join"] = o.InnerJoin.Get()
	}
	if o.View.IsSet() {
		toSerialize["view"] = o.View.Get()
	}
	if o.Filter.IsSet() {
		toSerialize["filter"] = o.Filter.Get()
	}
	if o.Limit.IsSet() {
		toSerialize["limit"] = o.Limit.Get()
	}
	if o.Format.IsSet() {
		toSerialize["format"] = o.Format.Get()
	}
	if o.Sort.IsSet() {
		toSerialize["sort"] = o.Sort.Get()
	}
	return toSerialize, nil
}

type NullableFlowOperation struct {
	value *FlowOperation
	isSet bool
}

func (v NullableFlowOperation) Get() *FlowOperation {
	return v.value
}

func (v *NullableFlowOperation) Set(val *FlowOperation) {
	v.value = val
	v.isSet = true
}

func (v NullableFlowOperation) IsSet() bool {
	return v.isSet
}

func (v *NullableFlowOperation) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFlowOperation(val *FlowOperation) *NullableFlowOperation {
	return &NullableFlowOperation{value: val, isSet: true}
}

func (v NullableFlowOperation) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFlowOperation) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
