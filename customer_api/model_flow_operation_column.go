/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the FlowOperationColumn type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &FlowOperationColumn{}

// FlowOperationColumn struct for FlowOperationColumn
type FlowOperationColumn struct {
	Source *string `json:"source,omitempty"`
	Column *string `json:"column,omitempty"`
	As     *string `json:"as,omitempty"`
}

// NewFlowOperationColumn instantiates a new FlowOperationColumn object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFlowOperationColumn() *FlowOperationColumn {
	this := FlowOperationColumn{}
	return &this
}

// NewFlowOperationColumnWithDefaults instantiates a new FlowOperationColumn object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFlowOperationColumnWithDefaults() *FlowOperationColumn {
	this := FlowOperationColumn{}
	return &this
}

// GetSource returns the Source field value if set, zero value otherwise.
func (o *FlowOperationColumn) GetSource() string {
	if o == nil || IsNil(o.Source) {
		var ret string
		return ret
	}
	return *o.Source
}

// GetSourceOk returns a tuple with the Source field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperationColumn) GetSourceOk() (*string, bool) {
	if o == nil || IsNil(o.Source) {
		return nil, false
	}
	return o.Source, true
}

// HasSource returns a boolean if a field has been set.
func (o *FlowOperationColumn) HasSource() bool {
	if o != nil && !IsNil(o.Source) {
		return true
	}

	return false
}

// SetSource gets a reference to the given string and assigns it to the Source field.
func (o *FlowOperationColumn) SetSource(v string) {
	o.Source = &v
}

// GetColumn returns the Column field value if set, zero value otherwise.
func (o *FlowOperationColumn) GetColumn() string {
	if o == nil || IsNil(o.Column) {
		var ret string
		return ret
	}
	return *o.Column
}

// GetColumnOk returns a tuple with the Column field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperationColumn) GetColumnOk() (*string, bool) {
	if o == nil || IsNil(o.Column) {
		return nil, false
	}
	return o.Column, true
}

// HasColumn returns a boolean if a field has been set.
func (o *FlowOperationColumn) HasColumn() bool {
	if o != nil && !IsNil(o.Column) {
		return true
	}

	return false
}

// SetColumn gets a reference to the given string and assigns it to the Column field.
func (o *FlowOperationColumn) SetColumn(v string) {
	o.Column = &v
}

// GetAs returns the As field value if set, zero value otherwise.
func (o *FlowOperationColumn) GetAs() string {
	if o == nil || IsNil(o.As) {
		var ret string
		return ret
	}
	return *o.As
}

// GetAsOk returns a tuple with the As field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperationColumn) GetAsOk() (*string, bool) {
	if o == nil || IsNil(o.As) {
		return nil, false
	}
	return o.As, true
}

// HasAs returns a boolean if a field has been set.
func (o *FlowOperationColumn) HasAs() bool {
	if o != nil && !IsNil(o.As) {
		return true
	}

	return false
}

// SetAs gets a reference to the given string and assigns it to the As field.
func (o *FlowOperationColumn) SetAs(v string) {
	o.As = &v
}

func (o FlowOperationColumn) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o FlowOperationColumn) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Source) {
		toSerialize["source"] = o.Source
	}
	if !IsNil(o.Column) {
		toSerialize["column"] = o.Column
	}
	if !IsNil(o.As) {
		toSerialize["as"] = o.As
	}
	return toSerialize, nil
}

type NullableFlowOperationColumn struct {
	value *FlowOperationColumn
	isSet bool
}

func (v NullableFlowOperationColumn) Get() *FlowOperationColumn {
	return v.value
}

func (v *NullableFlowOperationColumn) Set(val *FlowOperationColumn) {
	v.value = val
	v.isSet = true
}

func (v NullableFlowOperationColumn) IsSet() bool {
	return v.isSet
}

func (v *NullableFlowOperationColumn) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFlowOperationColumn(val *FlowOperationColumn) *NullableFlowOperationColumn {
	return &NullableFlowOperationColumn{value: val, isSet: true}
}

func (v NullableFlowOperationColumn) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFlowOperationColumn) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
