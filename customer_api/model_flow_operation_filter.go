/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"fmt"
)

// FlowOperationFilter - struct for FlowOperationFilter
type FlowOperationFilter struct {
	FlowOperationFilter *FlowOperationFilter
}

// FlowOperationFilterAsFlowOperationFilter is a convenience function that returns FlowOperationFilter wrapped in FlowOperationFilter
func FlowOperationFilterAsFlowOperationFilter(v *FlowOperationFilter) FlowOperationFilter {
	return FlowOperationFilter{
		FlowOperationFilter: v,
	}
}

// Unmarshal JSON data into one of the pointers in the struct
func (dst *FlowOperationFilter) UnmarshalJSON(data []byte) error {
	var err error
	// this object is nullable so check if the payload is null or empty string
	if string(data) == "" || string(data) == "{}" {
		return nil
	}

	match := 0
	// try to unmarshal data into FlowOperationFilter
	err = newStrictDecoder(data).Decode(&dst.FlowOperationFilter)
	if err == nil {
		jsonFlowOperationFilter, _ := json.Marshal(dst.FlowOperationFilter)
		if string(jsonFlowOperationFilter) == "{}" { // empty struct
			dst.FlowOperationFilter = nil
		} else {
			match++
		}
	} else {
		dst.FlowOperationFilter = nil
	}

	if match > 1 { // more than 1 match
		// reset to nil
		dst.FlowOperationFilter = nil

		return fmt.Errorf("data matches more than one schema in oneOf(FlowOperationFilter)")
	} else if match == 1 {
		return nil // exactly one match
	} else { // no match
		return fmt.Errorf("data failed to match schemas in oneOf(FlowOperationFilter)")
	}
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src FlowOperationFilter) MarshalJSON() ([]byte, error) {
	if src.FlowOperationFilter != nil {
		return json.Marshal(&src.FlowOperationFilter)
	}

	return nil, nil // no data in oneOf schemas
}

// Get the actual instance
func (obj *FlowOperationFilter) GetActualInstance() interface{} {
	if obj == nil {
		return nil
	}
	if obj.FlowOperationFilter != nil {
		return obj.FlowOperationFilter
	}

	// all schemas are nil
	return nil
}

type NullableFlowOperationFilter struct {
	value *FlowOperationFilter
	isSet bool
}

func (v NullableFlowOperationFilter) Get() *FlowOperationFilter {
	return v.value
}

func (v *NullableFlowOperationFilter) Set(val *FlowOperationFilter) {
	v.value = val
	v.isSet = true
}

func (v NullableFlowOperationFilter) IsSet() bool {
	return v.isSet
}

func (v *NullableFlowOperationFilter) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFlowOperationFilter(val *FlowOperationFilter) *NullableFlowOperationFilter {
	return &NullableFlowOperationFilter{value: val, isSet: true}
}

func (v NullableFlowOperationFilter) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFlowOperationFilter) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
