/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the FlowOperationJoin type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &FlowOperationJoin{}

// FlowOperationJoin struct for FlowOperationJoin
type FlowOperationJoin struct {
	// First Source ID
	Source *string `json:"source,omitempty"`
	// Second Source ID
	Target     *string                      `json:"target,omitempty"`
	Conditions []FlowOperationJoinCondition `json:"conditions,omitempty"`
	Columns    []FlowOperationColumn        `json:"columns,omitempty"`
}

// NewFlowOperationJoin instantiates a new FlowOperationJoin object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFlowOperationJoin() *FlowOperationJoin {
	this := FlowOperationJoin{}
	return &this
}

// NewFlowOperationJoinWithDefaults instantiates a new FlowOperationJoin object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFlowOperationJoinWithDefaults() *FlowOperationJoin {
	this := FlowOperationJoin{}
	return &this
}

// GetSource returns the Source field value if set, zero value otherwise.
func (o *FlowOperationJoin) GetSource() string {
	if o == nil || IsNil(o.Source) {
		var ret string
		return ret
	}
	return *o.Source
}

// GetSourceOk returns a tuple with the Source field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperationJoin) GetSourceOk() (*string, bool) {
	if o == nil || IsNil(o.Source) {
		return nil, false
	}
	return o.Source, true
}

// HasSource returns a boolean if a field has been set.
func (o *FlowOperationJoin) HasSource() bool {
	if o != nil && !IsNil(o.Source) {
		return true
	}

	return false
}

// SetSource gets a reference to the given string and assigns it to the Source field.
func (o *FlowOperationJoin) SetSource(v string) {
	o.Source = &v
}

// GetTarget returns the Target field value if set, zero value otherwise.
func (o *FlowOperationJoin) GetTarget() string {
	if o == nil || IsNil(o.Target) {
		var ret string
		return ret
	}
	return *o.Target
}

// GetTargetOk returns a tuple with the Target field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperationJoin) GetTargetOk() (*string, bool) {
	if o == nil || IsNil(o.Target) {
		return nil, false
	}
	return o.Target, true
}

// HasTarget returns a boolean if a field has been set.
func (o *FlowOperationJoin) HasTarget() bool {
	if o != nil && !IsNil(o.Target) {
		return true
	}

	return false
}

// SetTarget gets a reference to the given string and assigns it to the Target field.
func (o *FlowOperationJoin) SetTarget(v string) {
	o.Target = &v
}

// GetConditions returns the Conditions field value if set, zero value otherwise.
func (o *FlowOperationJoin) GetConditions() []FlowOperationJoinCondition {
	if o == nil || IsNil(o.Conditions) {
		var ret []FlowOperationJoinCondition
		return ret
	}
	return o.Conditions
}

// GetConditionsOk returns a tuple with the Conditions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperationJoin) GetConditionsOk() ([]FlowOperationJoinCondition, bool) {
	if o == nil || IsNil(o.Conditions) {
		return nil, false
	}
	return o.Conditions, true
}

// HasConditions returns a boolean if a field has been set.
func (o *FlowOperationJoin) HasConditions() bool {
	if o != nil && !IsNil(o.Conditions) {
		return true
	}

	return false
}

// SetConditions gets a reference to the given []FlowOperationJoinCondition and assigns it to the Conditions field.
func (o *FlowOperationJoin) SetConditions(v []FlowOperationJoinCondition) {
	o.Conditions = v
}

// GetColumns returns the Columns field value if set, zero value otherwise.
func (o *FlowOperationJoin) GetColumns() []FlowOperationColumn {
	if o == nil || IsNil(o.Columns) {
		var ret []FlowOperationColumn
		return ret
	}
	return o.Columns
}

// GetColumnsOk returns a tuple with the Columns field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FlowOperationJoin) GetColumnsOk() ([]FlowOperationColumn, bool) {
	if o == nil || IsNil(o.Columns) {
		return nil, false
	}
	return o.Columns, true
}

// HasColumns returns a boolean if a field has been set.
func (o *FlowOperationJoin) HasColumns() bool {
	if o != nil && !IsNil(o.Columns) {
		return true
	}

	return false
}

// SetColumns gets a reference to the given []FlowOperationColumn and assigns it to the Columns field.
func (o *FlowOperationJoin) SetColumns(v []FlowOperationColumn) {
	o.Columns = v
}

func (o FlowOperationJoin) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o FlowOperationJoin) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Source) {
		toSerialize["source"] = o.Source
	}
	if !IsNil(o.Target) {
		toSerialize["target"] = o.Target
	}
	if !IsNil(o.Conditions) {
		toSerialize["conditions"] = o.Conditions
	}
	if !IsNil(o.Columns) {
		toSerialize["columns"] = o.Columns
	}
	return toSerialize, nil
}

type NullableFlowOperationJoin struct {
	value *FlowOperationJoin
	isSet bool
}

func (v NullableFlowOperationJoin) Get() *FlowOperationJoin {
	return v.value
}

func (v *NullableFlowOperationJoin) Set(val *FlowOperationJoin) {
	v.value = val
	v.isSet = true
}

func (v NullableFlowOperationJoin) IsSet() bool {
	return v.isSet
}

func (v *NullableFlowOperationJoin) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFlowOperationJoin(val *FlowOperationJoin) *NullableFlowOperationJoin {
	return &NullableFlowOperationJoin{value: val, isSet: true}
}

func (v NullableFlowOperationJoin) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFlowOperationJoin) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
