/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"fmt"
)

// FlowOperationSort - struct for FlowOperationSort
type FlowOperationSort struct {
	FlowOperationSort *FlowOperationSort
}

// FlowOperationSortAsFlowOperationSort is a convenience function that returns FlowOperationSort wrapped in FlowOperationSort
func FlowOperationSortAsFlowOperationSort(v *FlowOperationSort) FlowOperationSort {
	return FlowOperationSort{
		FlowOperationSort: v,
	}
}

// Unmarshal JSON data into one of the pointers in the struct
func (dst *FlowOperationSort) UnmarshalJSON(data []byte) error {
	var err error
	// this object is nullable so check if the payload is null or empty string
	if string(data) == "" || string(data) == "{}" {
		return nil
	}

	match := 0
	// try to unmarshal data into FlowOperationSort
	err = newStrictDecoder(data).Decode(&dst.FlowOperationSort)
	if err == nil {
		jsonFlowOperationSort, _ := json.Marshal(dst.FlowOperationSort)
		if string(jsonFlowOperationSort) == "{}" { // empty struct
			dst.FlowOperationSort = nil
		} else {
			match++
		}
	} else {
		dst.FlowOperationSort = nil
	}

	if match > 1 { // more than 1 match
		// reset to nil
		dst.FlowOperationSort = nil

		return fmt.Errorf("data matches more than one schema in oneOf(FlowOperationSort)")
	} else if match == 1 {
		return nil // exactly one match
	} else { // no match
		return fmt.Errorf("data failed to match schemas in oneOf(FlowOperationSort)")
	}
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src FlowOperationSort) MarshalJSON() ([]byte, error) {
	if src.FlowOperationSort != nil {
		return json.Marshal(&src.FlowOperationSort)
	}

	return nil, nil // no data in oneOf schemas
}

// Get the actual instance
func (obj *FlowOperationSort) GetActualInstance() interface{} {
	if obj == nil {
		return nil
	}
	if obj.FlowOperationSort != nil {
		return obj.FlowOperationSort
	}

	// all schemas are nil
	return nil
}

type NullableFlowOperationSort struct {
	value *FlowOperationSort
	isSet bool
}

func (v NullableFlowOperationSort) Get() *FlowOperationSort {
	return v.value
}

func (v *NullableFlowOperationSort) Set(val *FlowOperationSort) {
	v.value = val
	v.isSet = true
}

func (v NullableFlowOperationSort) IsSet() bool {
	return v.isSet
}

func (v *NullableFlowOperationSort) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFlowOperationSort(val *FlowOperationSort) *NullableFlowOperationSort {
	return &NullableFlowOperationSort{value: val, isSet: true}
}

func (v NullableFlowOperationSort) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFlowOperationSort) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
