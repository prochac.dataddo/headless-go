/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"fmt"
)

// FlowOperationView - struct for FlowOperationView
type FlowOperationView struct {
	FlowOperationView *FlowOperationView
}

// FlowOperationViewAsFlowOperationView is a convenience function that returns FlowOperationView wrapped in FlowOperationView
func FlowOperationViewAsFlowOperationView(v *FlowOperationView) FlowOperationView {
	return FlowOperationView{
		FlowOperationView: v,
	}
}

// Unmarshal JSON data into one of the pointers in the struct
func (dst *FlowOperationView) UnmarshalJSON(data []byte) error {
	var err error
	// this object is nullable so check if the payload is null or empty string
	if string(data) == "" || string(data) == "{}" {
		return nil
	}

	match := 0
	// try to unmarshal data into FlowOperationView
	err = newStrictDecoder(data).Decode(&dst.FlowOperationView)
	if err == nil {
		jsonFlowOperationView, _ := json.Marshal(dst.FlowOperationView)
		if string(jsonFlowOperationView) == "{}" { // empty struct
			dst.FlowOperationView = nil
		} else {
			match++
		}
	} else {
		dst.FlowOperationView = nil
	}

	if match > 1 { // more than 1 match
		// reset to nil
		dst.FlowOperationView = nil

		return fmt.Errorf("data matches more than one schema in oneOf(FlowOperationView)")
	} else if match == 1 {
		return nil // exactly one match
	} else { // no match
		return fmt.Errorf("data failed to match schemas in oneOf(FlowOperationView)")
	}
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src FlowOperationView) MarshalJSON() ([]byte, error) {
	if src.FlowOperationView != nil {
		return json.Marshal(&src.FlowOperationView)
	}

	return nil, nil // no data in oneOf schemas
}

// Get the actual instance
func (obj *FlowOperationView) GetActualInstance() interface{} {
	if obj == nil {
		return nil
	}
	if obj.FlowOperationView != nil {
		return obj.FlowOperationView
	}

	// all schemas are nil
	return nil
}

type NullableFlowOperationView struct {
	value *FlowOperationView
	isSet bool
}

func (v NullableFlowOperationView) Get() *FlowOperationView {
	return v.value
}

func (v *NullableFlowOperationView) Set(val *FlowOperationView) {
	v.value = val
	v.isSet = true
}

func (v NullableFlowOperationView) IsSet() bool {
	return v.isSet
}

func (v *NullableFlowOperationView) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFlowOperationView(val *FlowOperationView) *NullableFlowOperationView {
	return &NullableFlowOperationView{value: val, isSet: true}
}

func (v NullableFlowOperationView) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFlowOperationView) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
