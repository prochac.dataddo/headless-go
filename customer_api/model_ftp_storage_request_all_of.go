/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the FtpStorageRequestAllOf type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &FtpStorageRequestAllOf{}

// FtpStorageRequestAllOf struct for FtpStorageRequestAllOf
type FtpStorageRequestAllOf struct {
	OAuthId *int32  `json:"o_auth_id,omitempty"`
	Path    *string `json:"path,omitempty"`
}

// NewFtpStorageRequestAllOf instantiates a new FtpStorageRequestAllOf object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFtpStorageRequestAllOf() *FtpStorageRequestAllOf {
	this := FtpStorageRequestAllOf{}
	return &this
}

// NewFtpStorageRequestAllOfWithDefaults instantiates a new FtpStorageRequestAllOf object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFtpStorageRequestAllOfWithDefaults() *FtpStorageRequestAllOf {
	this := FtpStorageRequestAllOf{}
	return &this
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise.
func (o *FtpStorageRequestAllOf) GetOAuthId() int32 {
	if o == nil || IsNil(o.OAuthId) {
		var ret int32
		return ret
	}
	return *o.OAuthId
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FtpStorageRequestAllOf) GetOAuthIdOk() (*int32, bool) {
	if o == nil || IsNil(o.OAuthId) {
		return nil, false
	}
	return o.OAuthId, true
}

// HasOAuthId returns a boolean if a field has been set.
func (o *FtpStorageRequestAllOf) HasOAuthId() bool {
	if o != nil && !IsNil(o.OAuthId) {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given int32 and assigns it to the OAuthId field.
func (o *FtpStorageRequestAllOf) SetOAuthId(v int32) {
	o.OAuthId = &v
}

// GetPath returns the Path field value if set, zero value otherwise.
func (o *FtpStorageRequestAllOf) GetPath() string {
	if o == nil || IsNil(o.Path) {
		var ret string
		return ret
	}
	return *o.Path
}

// GetPathOk returns a tuple with the Path field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FtpStorageRequestAllOf) GetPathOk() (*string, bool) {
	if o == nil || IsNil(o.Path) {
		return nil, false
	}
	return o.Path, true
}

// HasPath returns a boolean if a field has been set.
func (o *FtpStorageRequestAllOf) HasPath() bool {
	if o != nil && !IsNil(o.Path) {
		return true
	}

	return false
}

// SetPath gets a reference to the given string and assigns it to the Path field.
func (o *FtpStorageRequestAllOf) SetPath(v string) {
	o.Path = &v
}

func (o FtpStorageRequestAllOf) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o FtpStorageRequestAllOf) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.OAuthId) {
		toSerialize["o_auth_id"] = o.OAuthId
	}
	if !IsNil(o.Path) {
		toSerialize["path"] = o.Path
	}
	return toSerialize, nil
}

type NullableFtpStorageRequestAllOf struct {
	value *FtpStorageRequestAllOf
	isSet bool
}

func (v NullableFtpStorageRequestAllOf) Get() *FtpStorageRequestAllOf {
	return v.value
}

func (v *NullableFtpStorageRequestAllOf) Set(val *FtpStorageRequestAllOf) {
	v.value = val
	v.isSet = true
}

func (v NullableFtpStorageRequestAllOf) IsSet() bool {
	return v.isSet
}

func (v *NullableFtpStorageRequestAllOf) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFtpStorageRequestAllOf(val *FtpStorageRequestAllOf) *NullableFtpStorageRequestAllOf {
	return &NullableFtpStorageRequestAllOf{value: val, isSet: true}
}

func (v NullableFtpStorageRequestAllOf) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFtpStorageRequestAllOf) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
