/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the GoogleAdsStaticDtoAuthorizer type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GoogleAdsStaticDtoAuthorizer{}

// GoogleAdsStaticDtoAuthorizer struct for GoogleAdsStaticDtoAuthorizer
type GoogleAdsStaticDtoAuthorizer struct {
	StaticKey        *string `json:"staticKey,omitempty"`
	Label            *string `json:"label,omitempty"`
	ClientId         *string `json:"clientId,omitempty"`
	ClientSecret     *string `json:"clientSecret,omitempty"`
	RedirectUri      *string `json:"redirectUri,omitempty"`
	DeveloperToken   *string `json:"developerToken,omitempty"`
	ClientCustomerId *string `json:"clientCustomerId,omitempty"`
}

// NewGoogleAdsStaticDtoAuthorizer instantiates a new GoogleAdsStaticDtoAuthorizer object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGoogleAdsStaticDtoAuthorizer() *GoogleAdsStaticDtoAuthorizer {
	this := GoogleAdsStaticDtoAuthorizer{}
	return &this
}

// NewGoogleAdsStaticDtoAuthorizerWithDefaults instantiates a new GoogleAdsStaticDtoAuthorizer object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGoogleAdsStaticDtoAuthorizerWithDefaults() *GoogleAdsStaticDtoAuthorizer {
	this := GoogleAdsStaticDtoAuthorizer{}
	return &this
}

// GetStaticKey returns the StaticKey field value if set, zero value otherwise.
func (o *GoogleAdsStaticDtoAuthorizer) GetStaticKey() string {
	if o == nil || IsNil(o.StaticKey) {
		var ret string
		return ret
	}
	return *o.StaticKey
}

// GetStaticKeyOk returns a tuple with the StaticKey field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleAdsStaticDtoAuthorizer) GetStaticKeyOk() (*string, bool) {
	if o == nil || IsNil(o.StaticKey) {
		return nil, false
	}
	return o.StaticKey, true
}

// HasStaticKey returns a boolean if a field has been set.
func (o *GoogleAdsStaticDtoAuthorizer) HasStaticKey() bool {
	if o != nil && !IsNil(o.StaticKey) {
		return true
	}

	return false
}

// SetStaticKey gets a reference to the given string and assigns it to the StaticKey field.
func (o *GoogleAdsStaticDtoAuthorizer) SetStaticKey(v string) {
	o.StaticKey = &v
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *GoogleAdsStaticDtoAuthorizer) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleAdsStaticDtoAuthorizer) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *GoogleAdsStaticDtoAuthorizer) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *GoogleAdsStaticDtoAuthorizer) SetLabel(v string) {
	o.Label = &v
}

// GetClientId returns the ClientId field value if set, zero value otherwise.
func (o *GoogleAdsStaticDtoAuthorizer) GetClientId() string {
	if o == nil || IsNil(o.ClientId) {
		var ret string
		return ret
	}
	return *o.ClientId
}

// GetClientIdOk returns a tuple with the ClientId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleAdsStaticDtoAuthorizer) GetClientIdOk() (*string, bool) {
	if o == nil || IsNil(o.ClientId) {
		return nil, false
	}
	return o.ClientId, true
}

// HasClientId returns a boolean if a field has been set.
func (o *GoogleAdsStaticDtoAuthorizer) HasClientId() bool {
	if o != nil && !IsNil(o.ClientId) {
		return true
	}

	return false
}

// SetClientId gets a reference to the given string and assigns it to the ClientId field.
func (o *GoogleAdsStaticDtoAuthorizer) SetClientId(v string) {
	o.ClientId = &v
}

// GetClientSecret returns the ClientSecret field value if set, zero value otherwise.
func (o *GoogleAdsStaticDtoAuthorizer) GetClientSecret() string {
	if o == nil || IsNil(o.ClientSecret) {
		var ret string
		return ret
	}
	return *o.ClientSecret
}

// GetClientSecretOk returns a tuple with the ClientSecret field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleAdsStaticDtoAuthorizer) GetClientSecretOk() (*string, bool) {
	if o == nil || IsNil(o.ClientSecret) {
		return nil, false
	}
	return o.ClientSecret, true
}

// HasClientSecret returns a boolean if a field has been set.
func (o *GoogleAdsStaticDtoAuthorizer) HasClientSecret() bool {
	if o != nil && !IsNil(o.ClientSecret) {
		return true
	}

	return false
}

// SetClientSecret gets a reference to the given string and assigns it to the ClientSecret field.
func (o *GoogleAdsStaticDtoAuthorizer) SetClientSecret(v string) {
	o.ClientSecret = &v
}

// GetRedirectUri returns the RedirectUri field value if set, zero value otherwise.
func (o *GoogleAdsStaticDtoAuthorizer) GetRedirectUri() string {
	if o == nil || IsNil(o.RedirectUri) {
		var ret string
		return ret
	}
	return *o.RedirectUri
}

// GetRedirectUriOk returns a tuple with the RedirectUri field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleAdsStaticDtoAuthorizer) GetRedirectUriOk() (*string, bool) {
	if o == nil || IsNil(o.RedirectUri) {
		return nil, false
	}
	return o.RedirectUri, true
}

// HasRedirectUri returns a boolean if a field has been set.
func (o *GoogleAdsStaticDtoAuthorizer) HasRedirectUri() bool {
	if o != nil && !IsNil(o.RedirectUri) {
		return true
	}

	return false
}

// SetRedirectUri gets a reference to the given string and assigns it to the RedirectUri field.
func (o *GoogleAdsStaticDtoAuthorizer) SetRedirectUri(v string) {
	o.RedirectUri = &v
}

// GetDeveloperToken returns the DeveloperToken field value if set, zero value otherwise.
func (o *GoogleAdsStaticDtoAuthorizer) GetDeveloperToken() string {
	if o == nil || IsNil(o.DeveloperToken) {
		var ret string
		return ret
	}
	return *o.DeveloperToken
}

// GetDeveloperTokenOk returns a tuple with the DeveloperToken field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleAdsStaticDtoAuthorizer) GetDeveloperTokenOk() (*string, bool) {
	if o == nil || IsNil(o.DeveloperToken) {
		return nil, false
	}
	return o.DeveloperToken, true
}

// HasDeveloperToken returns a boolean if a field has been set.
func (o *GoogleAdsStaticDtoAuthorizer) HasDeveloperToken() bool {
	if o != nil && !IsNil(o.DeveloperToken) {
		return true
	}

	return false
}

// SetDeveloperToken gets a reference to the given string and assigns it to the DeveloperToken field.
func (o *GoogleAdsStaticDtoAuthorizer) SetDeveloperToken(v string) {
	o.DeveloperToken = &v
}

// GetClientCustomerId returns the ClientCustomerId field value if set, zero value otherwise.
func (o *GoogleAdsStaticDtoAuthorizer) GetClientCustomerId() string {
	if o == nil || IsNil(o.ClientCustomerId) {
		var ret string
		return ret
	}
	return *o.ClientCustomerId
}

// GetClientCustomerIdOk returns a tuple with the ClientCustomerId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleAdsStaticDtoAuthorizer) GetClientCustomerIdOk() (*string, bool) {
	if o == nil || IsNil(o.ClientCustomerId) {
		return nil, false
	}
	return o.ClientCustomerId, true
}

// HasClientCustomerId returns a boolean if a field has been set.
func (o *GoogleAdsStaticDtoAuthorizer) HasClientCustomerId() bool {
	if o != nil && !IsNil(o.ClientCustomerId) {
		return true
	}

	return false
}

// SetClientCustomerId gets a reference to the given string and assigns it to the ClientCustomerId field.
func (o *GoogleAdsStaticDtoAuthorizer) SetClientCustomerId(v string) {
	o.ClientCustomerId = &v
}

func (o GoogleAdsStaticDtoAuthorizer) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GoogleAdsStaticDtoAuthorizer) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.StaticKey) {
		toSerialize["staticKey"] = o.StaticKey
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.ClientId) {
		toSerialize["clientId"] = o.ClientId
	}
	if !IsNil(o.ClientSecret) {
		toSerialize["clientSecret"] = o.ClientSecret
	}
	if !IsNil(o.RedirectUri) {
		toSerialize["redirectUri"] = o.RedirectUri
	}
	if !IsNil(o.DeveloperToken) {
		toSerialize["developerToken"] = o.DeveloperToken
	}
	if !IsNil(o.ClientCustomerId) {
		toSerialize["clientCustomerId"] = o.ClientCustomerId
	}
	return toSerialize, nil
}

type NullableGoogleAdsStaticDtoAuthorizer struct {
	value *GoogleAdsStaticDtoAuthorizer
	isSet bool
}

func (v NullableGoogleAdsStaticDtoAuthorizer) Get() *GoogleAdsStaticDtoAuthorizer {
	return v.value
}

func (v *NullableGoogleAdsStaticDtoAuthorizer) Set(val *GoogleAdsStaticDtoAuthorizer) {
	v.value = val
	v.isSet = true
}

func (v NullableGoogleAdsStaticDtoAuthorizer) IsSet() bool {
	return v.isSet
}

func (v *NullableGoogleAdsStaticDtoAuthorizer) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGoogleAdsStaticDtoAuthorizer(val *GoogleAdsStaticDtoAuthorizer) *NullableGoogleAdsStaticDtoAuthorizer {
	return &NullableGoogleAdsStaticDtoAuthorizer{value: val, isSet: true}
}

func (v NullableGoogleAdsStaticDtoAuthorizer) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGoogleAdsStaticDtoAuthorizer) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
