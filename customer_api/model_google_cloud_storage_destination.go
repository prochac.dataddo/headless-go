/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"time"
)

// checks if the GoogleCloudStorageDestination type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GoogleCloudStorageDestination{}

// GoogleCloudStorageDestination struct for GoogleCloudStorageDestination
type GoogleCloudStorageDestination struct {
	Id           NullableString `json:"id,omitempty"`
	CustomerId   *string        `json:"customer_id,omitempty"`
	CreatedAt    NullableInt32  `json:"created_at,omitempty"`
	UpdatedAt    NullableInt32  `json:"updated_at,omitempty"`
	LastUse      NullableTime   `json:"lastUse,omitempty"`
	Label        *string        `json:"label,omitempty"`
	Description  *string        `json:"description,omitempty"`
	Status       *bool          `json:"status,omitempty"`
	StatusDetail *string        `json:"statusDetail,omitempty"`
	Type         *string        `json:"type,omitempty"`
	Path         NullableString `json:"path,omitempty"`
	OAuthId      *int32         `json:"o_auth_id,omitempty"`
	Bucket       *string        `json:"bucket,omitempty"`
}

// NewGoogleCloudStorageDestination instantiates a new GoogleCloudStorageDestination object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGoogleCloudStorageDestination() *GoogleCloudStorageDestination {
	this := GoogleCloudStorageDestination{}
	return &this
}

// NewGoogleCloudStorageDestinationWithDefaults instantiates a new GoogleCloudStorageDestination object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGoogleCloudStorageDestinationWithDefaults() *GoogleCloudStorageDestination {
	this := GoogleCloudStorageDestination{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleCloudStorageDestination) GetId() string {
	if o == nil || IsNil(o.Id.Get()) {
		var ret string
		return ret
	}
	return *o.Id.Get()
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleCloudStorageDestination) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Id.Get(), o.Id.IsSet()
}

// HasId returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasId() bool {
	if o != nil && o.Id.IsSet() {
		return true
	}

	return false
}

// SetId gets a reference to the given NullableString and assigns it to the Id field.
func (o *GoogleCloudStorageDestination) SetId(v string) {
	o.Id.Set(&v)
}

// SetIdNil sets the value for Id to be an explicit nil
func (o *GoogleCloudStorageDestination) SetIdNil() {
	o.Id.Set(nil)
}

// UnsetId ensures that no value is present for Id, not even an explicit nil
func (o *GoogleCloudStorageDestination) UnsetId() {
	o.Id.Unset()
}

// GetCustomerId returns the CustomerId field value if set, zero value otherwise.
func (o *GoogleCloudStorageDestination) GetCustomerId() string {
	if o == nil || IsNil(o.CustomerId) {
		var ret string
		return ret
	}
	return *o.CustomerId
}

// GetCustomerIdOk returns a tuple with the CustomerId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleCloudStorageDestination) GetCustomerIdOk() (*string, bool) {
	if o == nil || IsNil(o.CustomerId) {
		return nil, false
	}
	return o.CustomerId, true
}

// HasCustomerId returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasCustomerId() bool {
	if o != nil && !IsNil(o.CustomerId) {
		return true
	}

	return false
}

// SetCustomerId gets a reference to the given string and assigns it to the CustomerId field.
func (o *GoogleCloudStorageDestination) SetCustomerId(v string) {
	o.CustomerId = &v
}

// GetCreatedAt returns the CreatedAt field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleCloudStorageDestination) GetCreatedAt() int32 {
	if o == nil || IsNil(o.CreatedAt.Get()) {
		var ret int32
		return ret
	}
	return *o.CreatedAt.Get()
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleCloudStorageDestination) GetCreatedAtOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.CreatedAt.Get(), o.CreatedAt.IsSet()
}

// HasCreatedAt returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasCreatedAt() bool {
	if o != nil && o.CreatedAt.IsSet() {
		return true
	}

	return false
}

// SetCreatedAt gets a reference to the given NullableInt32 and assigns it to the CreatedAt field.
func (o *GoogleCloudStorageDestination) SetCreatedAt(v int32) {
	o.CreatedAt.Set(&v)
}

// SetCreatedAtNil sets the value for CreatedAt to be an explicit nil
func (o *GoogleCloudStorageDestination) SetCreatedAtNil() {
	o.CreatedAt.Set(nil)
}

// UnsetCreatedAt ensures that no value is present for CreatedAt, not even an explicit nil
func (o *GoogleCloudStorageDestination) UnsetCreatedAt() {
	o.CreatedAt.Unset()
}

// GetUpdatedAt returns the UpdatedAt field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleCloudStorageDestination) GetUpdatedAt() int32 {
	if o == nil || IsNil(o.UpdatedAt.Get()) {
		var ret int32
		return ret
	}
	return *o.UpdatedAt.Get()
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleCloudStorageDestination) GetUpdatedAtOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.UpdatedAt.Get(), o.UpdatedAt.IsSet()
}

// HasUpdatedAt returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasUpdatedAt() bool {
	if o != nil && o.UpdatedAt.IsSet() {
		return true
	}

	return false
}

// SetUpdatedAt gets a reference to the given NullableInt32 and assigns it to the UpdatedAt field.
func (o *GoogleCloudStorageDestination) SetUpdatedAt(v int32) {
	o.UpdatedAt.Set(&v)
}

// SetUpdatedAtNil sets the value for UpdatedAt to be an explicit nil
func (o *GoogleCloudStorageDestination) SetUpdatedAtNil() {
	o.UpdatedAt.Set(nil)
}

// UnsetUpdatedAt ensures that no value is present for UpdatedAt, not even an explicit nil
func (o *GoogleCloudStorageDestination) UnsetUpdatedAt() {
	o.UpdatedAt.Unset()
}

// GetLastUse returns the LastUse field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleCloudStorageDestination) GetLastUse() time.Time {
	if o == nil || IsNil(o.LastUse.Get()) {
		var ret time.Time
		return ret
	}
	return *o.LastUse.Get()
}

// GetLastUseOk returns a tuple with the LastUse field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleCloudStorageDestination) GetLastUseOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return o.LastUse.Get(), o.LastUse.IsSet()
}

// HasLastUse returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasLastUse() bool {
	if o != nil && o.LastUse.IsSet() {
		return true
	}

	return false
}

// SetLastUse gets a reference to the given NullableTime and assigns it to the LastUse field.
func (o *GoogleCloudStorageDestination) SetLastUse(v time.Time) {
	o.LastUse.Set(&v)
}

// SetLastUseNil sets the value for LastUse to be an explicit nil
func (o *GoogleCloudStorageDestination) SetLastUseNil() {
	o.LastUse.Set(nil)
}

// UnsetLastUse ensures that no value is present for LastUse, not even an explicit nil
func (o *GoogleCloudStorageDestination) UnsetLastUse() {
	o.LastUse.Unset()
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *GoogleCloudStorageDestination) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleCloudStorageDestination) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *GoogleCloudStorageDestination) SetLabel(v string) {
	o.Label = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *GoogleCloudStorageDestination) GetDescription() string {
	if o == nil || IsNil(o.Description) {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleCloudStorageDestination) GetDescriptionOk() (*string, bool) {
	if o == nil || IsNil(o.Description) {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasDescription() bool {
	if o != nil && !IsNil(o.Description) {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *GoogleCloudStorageDestination) SetDescription(v string) {
	o.Description = &v
}

// GetStatus returns the Status field value if set, zero value otherwise.
func (o *GoogleCloudStorageDestination) GetStatus() bool {
	if o == nil || IsNil(o.Status) {
		var ret bool
		return ret
	}
	return *o.Status
}

// GetStatusOk returns a tuple with the Status field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleCloudStorageDestination) GetStatusOk() (*bool, bool) {
	if o == nil || IsNil(o.Status) {
		return nil, false
	}
	return o.Status, true
}

// HasStatus returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasStatus() bool {
	if o != nil && !IsNil(o.Status) {
		return true
	}

	return false
}

// SetStatus gets a reference to the given bool and assigns it to the Status field.
func (o *GoogleCloudStorageDestination) SetStatus(v bool) {
	o.Status = &v
}

// GetStatusDetail returns the StatusDetail field value if set, zero value otherwise.
func (o *GoogleCloudStorageDestination) GetStatusDetail() string {
	if o == nil || IsNil(o.StatusDetail) {
		var ret string
		return ret
	}
	return *o.StatusDetail
}

// GetStatusDetailOk returns a tuple with the StatusDetail field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleCloudStorageDestination) GetStatusDetailOk() (*string, bool) {
	if o == nil || IsNil(o.StatusDetail) {
		return nil, false
	}
	return o.StatusDetail, true
}

// HasStatusDetail returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasStatusDetail() bool {
	if o != nil && !IsNil(o.StatusDetail) {
		return true
	}

	return false
}

// SetStatusDetail gets a reference to the given string and assigns it to the StatusDetail field.
func (o *GoogleCloudStorageDestination) SetStatusDetail(v string) {
	o.StatusDetail = &v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *GoogleCloudStorageDestination) GetType() string {
	if o == nil || IsNil(o.Type) {
		var ret string
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleCloudStorageDestination) GetTypeOk() (*string, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given string and assigns it to the Type field.
func (o *GoogleCloudStorageDestination) SetType(v string) {
	o.Type = &v
}

// GetPath returns the Path field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleCloudStorageDestination) GetPath() string {
	if o == nil || IsNil(o.Path.Get()) {
		var ret string
		return ret
	}
	return *o.Path.Get()
}

// GetPathOk returns a tuple with the Path field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleCloudStorageDestination) GetPathOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Path.Get(), o.Path.IsSet()
}

// HasPath returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasPath() bool {
	if o != nil && o.Path.IsSet() {
		return true
	}

	return false
}

// SetPath gets a reference to the given NullableString and assigns it to the Path field.
func (o *GoogleCloudStorageDestination) SetPath(v string) {
	o.Path.Set(&v)
}

// SetPathNil sets the value for Path to be an explicit nil
func (o *GoogleCloudStorageDestination) SetPathNil() {
	o.Path.Set(nil)
}

// UnsetPath ensures that no value is present for Path, not even an explicit nil
func (o *GoogleCloudStorageDestination) UnsetPath() {
	o.Path.Unset()
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise.
func (o *GoogleCloudStorageDestination) GetOAuthId() int32 {
	if o == nil || IsNil(o.OAuthId) {
		var ret int32
		return ret
	}
	return *o.OAuthId
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleCloudStorageDestination) GetOAuthIdOk() (*int32, bool) {
	if o == nil || IsNil(o.OAuthId) {
		return nil, false
	}
	return o.OAuthId, true
}

// HasOAuthId returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasOAuthId() bool {
	if o != nil && !IsNil(o.OAuthId) {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given int32 and assigns it to the OAuthId field.
func (o *GoogleCloudStorageDestination) SetOAuthId(v int32) {
	o.OAuthId = &v
}

// GetBucket returns the Bucket field value if set, zero value otherwise.
func (o *GoogleCloudStorageDestination) GetBucket() string {
	if o == nil || IsNil(o.Bucket) {
		var ret string
		return ret
	}
	return *o.Bucket
}

// GetBucketOk returns a tuple with the Bucket field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleCloudStorageDestination) GetBucketOk() (*string, bool) {
	if o == nil || IsNil(o.Bucket) {
		return nil, false
	}
	return o.Bucket, true
}

// HasBucket returns a boolean if a field has been set.
func (o *GoogleCloudStorageDestination) HasBucket() bool {
	if o != nil && !IsNil(o.Bucket) {
		return true
	}

	return false
}

// SetBucket gets a reference to the given string and assigns it to the Bucket field.
func (o *GoogleCloudStorageDestination) SetBucket(v string) {
	o.Bucket = &v
}

func (o GoogleCloudStorageDestination) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GoogleCloudStorageDestination) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.Id.IsSet() {
		toSerialize["id"] = o.Id.Get()
	}
	if !IsNil(o.CustomerId) {
		toSerialize["customer_id"] = o.CustomerId
	}
	if o.CreatedAt.IsSet() {
		toSerialize["created_at"] = o.CreatedAt.Get()
	}
	if o.UpdatedAt.IsSet() {
		toSerialize["updated_at"] = o.UpdatedAt.Get()
	}
	if o.LastUse.IsSet() {
		toSerialize["lastUse"] = o.LastUse.Get()
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.Description) {
		toSerialize["description"] = o.Description
	}
	if !IsNil(o.Status) {
		toSerialize["status"] = o.Status
	}
	if !IsNil(o.StatusDetail) {
		toSerialize["statusDetail"] = o.StatusDetail
	}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	if o.Path.IsSet() {
		toSerialize["path"] = o.Path.Get()
	}
	if !IsNil(o.OAuthId) {
		toSerialize["o_auth_id"] = o.OAuthId
	}
	if !IsNil(o.Bucket) {
		toSerialize["bucket"] = o.Bucket
	}
	return toSerialize, nil
}

type NullableGoogleCloudStorageDestination struct {
	value *GoogleCloudStorageDestination
	isSet bool
}

func (v NullableGoogleCloudStorageDestination) Get() *GoogleCloudStorageDestination {
	return v.value
}

func (v *NullableGoogleCloudStorageDestination) Set(val *GoogleCloudStorageDestination) {
	v.value = val
	v.isSet = true
}

func (v NullableGoogleCloudStorageDestination) IsSet() bool {
	return v.isSet
}

func (v *NullableGoogleCloudStorageDestination) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGoogleCloudStorageDestination(val *GoogleCloudStorageDestination) *NullableGoogleCloudStorageDestination {
	return &NullableGoogleCloudStorageDestination{value: val, isSet: true}
}

func (v NullableGoogleCloudStorageDestination) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGoogleCloudStorageDestination) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
