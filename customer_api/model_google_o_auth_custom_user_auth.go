/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"time"
)

// checks if the GoogleOAuthCustomUserAuth type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GoogleOAuthCustomUserAuth{}

// GoogleOAuthCustomUserAuth struct for GoogleOAuthCustomUserAuth
type GoogleOAuthCustomUserAuth struct {
	Id           NullableInt32 `json:"id,omitempty"`
	CustomerId   *string       `json:"customer_id,omitempty"`
	CreatedAt    NullableInt32 `json:"created_at,omitempty"`
	UpdatedAt    NullableInt32 `json:"updated_at,omitempty"`
	Label        *string       `json:"label,omitempty"`
	ObjectId     *string       `json:"objectId,omitempty"`
	Status       *bool         `json:"status,omitempty"`
	StatusDetail *string       `json:"status_detail,omitempty"`
	ServiceType  *string       `json:"service_type,omitempty"`
	LastUse      NullableTime  `json:"last_use,omitempty"`
	Identifier   *string       `json:"identifier,omitempty"`
	Hash         *string       `json:"hash,omitempty"`
	Type         *string       `json:"type,omitempty"`
	ClientId     *string       `json:"clientId,omitempty"`
	ClientSecret *string       `json:"clientSecret,omitempty"`
	RedirectUri  *string       `json:"redirectUri,omitempty"`
}

// NewGoogleOAuthCustomUserAuth instantiates a new GoogleOAuthCustomUserAuth object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGoogleOAuthCustomUserAuth() *GoogleOAuthCustomUserAuth {
	this := GoogleOAuthCustomUserAuth{}
	return &this
}

// NewGoogleOAuthCustomUserAuthWithDefaults instantiates a new GoogleOAuthCustomUserAuth object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGoogleOAuthCustomUserAuthWithDefaults() *GoogleOAuthCustomUserAuth {
	this := GoogleOAuthCustomUserAuth{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleOAuthCustomUserAuth) GetId() int32 {
	if o == nil || IsNil(o.Id.Get()) {
		var ret int32
		return ret
	}
	return *o.Id.Get()
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleOAuthCustomUserAuth) GetIdOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.Id.Get(), o.Id.IsSet()
}

// HasId returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasId() bool {
	if o != nil && o.Id.IsSet() {
		return true
	}

	return false
}

// SetId gets a reference to the given NullableInt32 and assigns it to the Id field.
func (o *GoogleOAuthCustomUserAuth) SetId(v int32) {
	o.Id.Set(&v)
}

// SetIdNil sets the value for Id to be an explicit nil
func (o *GoogleOAuthCustomUserAuth) SetIdNil() {
	o.Id.Set(nil)
}

// UnsetId ensures that no value is present for Id, not even an explicit nil
func (o *GoogleOAuthCustomUserAuth) UnsetId() {
	o.Id.Unset()
}

// GetCustomerId returns the CustomerId field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetCustomerId() string {
	if o == nil || IsNil(o.CustomerId) {
		var ret string
		return ret
	}
	return *o.CustomerId
}

// GetCustomerIdOk returns a tuple with the CustomerId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetCustomerIdOk() (*string, bool) {
	if o == nil || IsNil(o.CustomerId) {
		return nil, false
	}
	return o.CustomerId, true
}

// HasCustomerId returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasCustomerId() bool {
	if o != nil && !IsNil(o.CustomerId) {
		return true
	}

	return false
}

// SetCustomerId gets a reference to the given string and assigns it to the CustomerId field.
func (o *GoogleOAuthCustomUserAuth) SetCustomerId(v string) {
	o.CustomerId = &v
}

// GetCreatedAt returns the CreatedAt field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleOAuthCustomUserAuth) GetCreatedAt() int32 {
	if o == nil || IsNil(o.CreatedAt.Get()) {
		var ret int32
		return ret
	}
	return *o.CreatedAt.Get()
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleOAuthCustomUserAuth) GetCreatedAtOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.CreatedAt.Get(), o.CreatedAt.IsSet()
}

// HasCreatedAt returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasCreatedAt() bool {
	if o != nil && o.CreatedAt.IsSet() {
		return true
	}

	return false
}

// SetCreatedAt gets a reference to the given NullableInt32 and assigns it to the CreatedAt field.
func (o *GoogleOAuthCustomUserAuth) SetCreatedAt(v int32) {
	o.CreatedAt.Set(&v)
}

// SetCreatedAtNil sets the value for CreatedAt to be an explicit nil
func (o *GoogleOAuthCustomUserAuth) SetCreatedAtNil() {
	o.CreatedAt.Set(nil)
}

// UnsetCreatedAt ensures that no value is present for CreatedAt, not even an explicit nil
func (o *GoogleOAuthCustomUserAuth) UnsetCreatedAt() {
	o.CreatedAt.Unset()
}

// GetUpdatedAt returns the UpdatedAt field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleOAuthCustomUserAuth) GetUpdatedAt() int32 {
	if o == nil || IsNil(o.UpdatedAt.Get()) {
		var ret int32
		return ret
	}
	return *o.UpdatedAt.Get()
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleOAuthCustomUserAuth) GetUpdatedAtOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.UpdatedAt.Get(), o.UpdatedAt.IsSet()
}

// HasUpdatedAt returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasUpdatedAt() bool {
	if o != nil && o.UpdatedAt.IsSet() {
		return true
	}

	return false
}

// SetUpdatedAt gets a reference to the given NullableInt32 and assigns it to the UpdatedAt field.
func (o *GoogleOAuthCustomUserAuth) SetUpdatedAt(v int32) {
	o.UpdatedAt.Set(&v)
}

// SetUpdatedAtNil sets the value for UpdatedAt to be an explicit nil
func (o *GoogleOAuthCustomUserAuth) SetUpdatedAtNil() {
	o.UpdatedAt.Set(nil)
}

// UnsetUpdatedAt ensures that no value is present for UpdatedAt, not even an explicit nil
func (o *GoogleOAuthCustomUserAuth) UnsetUpdatedAt() {
	o.UpdatedAt.Unset()
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *GoogleOAuthCustomUserAuth) SetLabel(v string) {
	o.Label = &v
}

// GetObjectId returns the ObjectId field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetObjectId() string {
	if o == nil || IsNil(o.ObjectId) {
		var ret string
		return ret
	}
	return *o.ObjectId
}

// GetObjectIdOk returns a tuple with the ObjectId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetObjectIdOk() (*string, bool) {
	if o == nil || IsNil(o.ObjectId) {
		return nil, false
	}
	return o.ObjectId, true
}

// HasObjectId returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasObjectId() bool {
	if o != nil && !IsNil(o.ObjectId) {
		return true
	}

	return false
}

// SetObjectId gets a reference to the given string and assigns it to the ObjectId field.
func (o *GoogleOAuthCustomUserAuth) SetObjectId(v string) {
	o.ObjectId = &v
}

// GetStatus returns the Status field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetStatus() bool {
	if o == nil || IsNil(o.Status) {
		var ret bool
		return ret
	}
	return *o.Status
}

// GetStatusOk returns a tuple with the Status field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetStatusOk() (*bool, bool) {
	if o == nil || IsNil(o.Status) {
		return nil, false
	}
	return o.Status, true
}

// HasStatus returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasStatus() bool {
	if o != nil && !IsNil(o.Status) {
		return true
	}

	return false
}

// SetStatus gets a reference to the given bool and assigns it to the Status field.
func (o *GoogleOAuthCustomUserAuth) SetStatus(v bool) {
	o.Status = &v
}

// GetStatusDetail returns the StatusDetail field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetStatusDetail() string {
	if o == nil || IsNil(o.StatusDetail) {
		var ret string
		return ret
	}
	return *o.StatusDetail
}

// GetStatusDetailOk returns a tuple with the StatusDetail field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetStatusDetailOk() (*string, bool) {
	if o == nil || IsNil(o.StatusDetail) {
		return nil, false
	}
	return o.StatusDetail, true
}

// HasStatusDetail returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasStatusDetail() bool {
	if o != nil && !IsNil(o.StatusDetail) {
		return true
	}

	return false
}

// SetStatusDetail gets a reference to the given string and assigns it to the StatusDetail field.
func (o *GoogleOAuthCustomUserAuth) SetStatusDetail(v string) {
	o.StatusDetail = &v
}

// GetServiceType returns the ServiceType field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetServiceType() string {
	if o == nil || IsNil(o.ServiceType) {
		var ret string
		return ret
	}
	return *o.ServiceType
}

// GetServiceTypeOk returns a tuple with the ServiceType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetServiceTypeOk() (*string, bool) {
	if o == nil || IsNil(o.ServiceType) {
		return nil, false
	}
	return o.ServiceType, true
}

// HasServiceType returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasServiceType() bool {
	if o != nil && !IsNil(o.ServiceType) {
		return true
	}

	return false
}

// SetServiceType gets a reference to the given string and assigns it to the ServiceType field.
func (o *GoogleOAuthCustomUserAuth) SetServiceType(v string) {
	o.ServiceType = &v
}

// GetLastUse returns the LastUse field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *GoogleOAuthCustomUserAuth) GetLastUse() time.Time {
	if o == nil || IsNil(o.LastUse.Get()) {
		var ret time.Time
		return ret
	}
	return *o.LastUse.Get()
}

// GetLastUseOk returns a tuple with the LastUse field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *GoogleOAuthCustomUserAuth) GetLastUseOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return o.LastUse.Get(), o.LastUse.IsSet()
}

// HasLastUse returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasLastUse() bool {
	if o != nil && o.LastUse.IsSet() {
		return true
	}

	return false
}

// SetLastUse gets a reference to the given NullableTime and assigns it to the LastUse field.
func (o *GoogleOAuthCustomUserAuth) SetLastUse(v time.Time) {
	o.LastUse.Set(&v)
}

// SetLastUseNil sets the value for LastUse to be an explicit nil
func (o *GoogleOAuthCustomUserAuth) SetLastUseNil() {
	o.LastUse.Set(nil)
}

// UnsetLastUse ensures that no value is present for LastUse, not even an explicit nil
func (o *GoogleOAuthCustomUserAuth) UnsetLastUse() {
	o.LastUse.Unset()
}

// GetIdentifier returns the Identifier field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetIdentifier() string {
	if o == nil || IsNil(o.Identifier) {
		var ret string
		return ret
	}
	return *o.Identifier
}

// GetIdentifierOk returns a tuple with the Identifier field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetIdentifierOk() (*string, bool) {
	if o == nil || IsNil(o.Identifier) {
		return nil, false
	}
	return o.Identifier, true
}

// HasIdentifier returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasIdentifier() bool {
	if o != nil && !IsNil(o.Identifier) {
		return true
	}

	return false
}

// SetIdentifier gets a reference to the given string and assigns it to the Identifier field.
func (o *GoogleOAuthCustomUserAuth) SetIdentifier(v string) {
	o.Identifier = &v
}

// GetHash returns the Hash field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetHash() string {
	if o == nil || IsNil(o.Hash) {
		var ret string
		return ret
	}
	return *o.Hash
}

// GetHashOk returns a tuple with the Hash field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetHashOk() (*string, bool) {
	if o == nil || IsNil(o.Hash) {
		return nil, false
	}
	return o.Hash, true
}

// HasHash returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasHash() bool {
	if o != nil && !IsNil(o.Hash) {
		return true
	}

	return false
}

// SetHash gets a reference to the given string and assigns it to the Hash field.
func (o *GoogleOAuthCustomUserAuth) SetHash(v string) {
	o.Hash = &v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetType() string {
	if o == nil || IsNil(o.Type) {
		var ret string
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetTypeOk() (*string, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given string and assigns it to the Type field.
func (o *GoogleOAuthCustomUserAuth) SetType(v string) {
	o.Type = &v
}

// GetClientId returns the ClientId field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetClientId() string {
	if o == nil || IsNil(o.ClientId) {
		var ret string
		return ret
	}
	return *o.ClientId
}

// GetClientIdOk returns a tuple with the ClientId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetClientIdOk() (*string, bool) {
	if o == nil || IsNil(o.ClientId) {
		return nil, false
	}
	return o.ClientId, true
}

// HasClientId returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasClientId() bool {
	if o != nil && !IsNil(o.ClientId) {
		return true
	}

	return false
}

// SetClientId gets a reference to the given string and assigns it to the ClientId field.
func (o *GoogleOAuthCustomUserAuth) SetClientId(v string) {
	o.ClientId = &v
}

// GetClientSecret returns the ClientSecret field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetClientSecret() string {
	if o == nil || IsNil(o.ClientSecret) {
		var ret string
		return ret
	}
	return *o.ClientSecret
}

// GetClientSecretOk returns a tuple with the ClientSecret field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetClientSecretOk() (*string, bool) {
	if o == nil || IsNil(o.ClientSecret) {
		return nil, false
	}
	return o.ClientSecret, true
}

// HasClientSecret returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasClientSecret() bool {
	if o != nil && !IsNil(o.ClientSecret) {
		return true
	}

	return false
}

// SetClientSecret gets a reference to the given string and assigns it to the ClientSecret field.
func (o *GoogleOAuthCustomUserAuth) SetClientSecret(v string) {
	o.ClientSecret = &v
}

// GetRedirectUri returns the RedirectUri field value if set, zero value otherwise.
func (o *GoogleOAuthCustomUserAuth) GetRedirectUri() string {
	if o == nil || IsNil(o.RedirectUri) {
		var ret string
		return ret
	}
	return *o.RedirectUri
}

// GetRedirectUriOk returns a tuple with the RedirectUri field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleOAuthCustomUserAuth) GetRedirectUriOk() (*string, bool) {
	if o == nil || IsNil(o.RedirectUri) {
		return nil, false
	}
	return o.RedirectUri, true
}

// HasRedirectUri returns a boolean if a field has been set.
func (o *GoogleOAuthCustomUserAuth) HasRedirectUri() bool {
	if o != nil && !IsNil(o.RedirectUri) {
		return true
	}

	return false
}

// SetRedirectUri gets a reference to the given string and assigns it to the RedirectUri field.
func (o *GoogleOAuthCustomUserAuth) SetRedirectUri(v string) {
	o.RedirectUri = &v
}

func (o GoogleOAuthCustomUserAuth) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GoogleOAuthCustomUserAuth) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.Id.IsSet() {
		toSerialize["id"] = o.Id.Get()
	}
	if !IsNil(o.CustomerId) {
		toSerialize["customer_id"] = o.CustomerId
	}
	if o.CreatedAt.IsSet() {
		toSerialize["created_at"] = o.CreatedAt.Get()
	}
	if o.UpdatedAt.IsSet() {
		toSerialize["updated_at"] = o.UpdatedAt.Get()
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.ObjectId) {
		toSerialize["objectId"] = o.ObjectId
	}
	if !IsNil(o.Status) {
		toSerialize["status"] = o.Status
	}
	if !IsNil(o.StatusDetail) {
		toSerialize["status_detail"] = o.StatusDetail
	}
	if !IsNil(o.ServiceType) {
		toSerialize["service_type"] = o.ServiceType
	}
	if o.LastUse.IsSet() {
		toSerialize["last_use"] = o.LastUse.Get()
	}
	if !IsNil(o.Identifier) {
		toSerialize["identifier"] = o.Identifier
	}
	if !IsNil(o.Hash) {
		toSerialize["hash"] = o.Hash
	}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	if !IsNil(o.ClientId) {
		toSerialize["clientId"] = o.ClientId
	}
	if !IsNil(o.ClientSecret) {
		toSerialize["clientSecret"] = o.ClientSecret
	}
	if !IsNil(o.RedirectUri) {
		toSerialize["redirectUri"] = o.RedirectUri
	}
	return toSerialize, nil
}

type NullableGoogleOAuthCustomUserAuth struct {
	value *GoogleOAuthCustomUserAuth
	isSet bool
}

func (v NullableGoogleOAuthCustomUserAuth) Get() *GoogleOAuthCustomUserAuth {
	return v.value
}

func (v *NullableGoogleOAuthCustomUserAuth) Set(val *GoogleOAuthCustomUserAuth) {
	v.value = val
	v.isSet = true
}

func (v NullableGoogleOAuthCustomUserAuth) IsSet() bool {
	return v.isSet
}

func (v *NullableGoogleOAuthCustomUserAuth) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGoogleOAuthCustomUserAuth(val *GoogleOAuthCustomUserAuth) *NullableGoogleOAuthCustomUserAuth {
	return &NullableGoogleOAuthCustomUserAuth{value: val, isSet: true}
}

func (v NullableGoogleOAuthCustomUserAuth) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGoogleOAuthCustomUserAuth) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
