/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the GoogleSheetsCustomDtoAuthorizer type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GoogleSheetsCustomDtoAuthorizer{}

// GoogleSheetsCustomDtoAuthorizer struct for GoogleSheetsCustomDtoAuthorizer
type GoogleSheetsCustomDtoAuthorizer struct {
	Label        *string `json:"label,omitempty"`
	ClientId     *string `json:"clientId,omitempty"`
	ClientSecret *string `json:"clientSecret,omitempty"`
	RedirectUri  *string `json:"redirectUri,omitempty"`
}

// NewGoogleSheetsCustomDtoAuthorizer instantiates a new GoogleSheetsCustomDtoAuthorizer object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGoogleSheetsCustomDtoAuthorizer() *GoogleSheetsCustomDtoAuthorizer {
	this := GoogleSheetsCustomDtoAuthorizer{}
	return &this
}

// NewGoogleSheetsCustomDtoAuthorizerWithDefaults instantiates a new GoogleSheetsCustomDtoAuthorizer object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGoogleSheetsCustomDtoAuthorizerWithDefaults() *GoogleSheetsCustomDtoAuthorizer {
	this := GoogleSheetsCustomDtoAuthorizer{}
	return &this
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *GoogleSheetsCustomDtoAuthorizer) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleSheetsCustomDtoAuthorizer) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *GoogleSheetsCustomDtoAuthorizer) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *GoogleSheetsCustomDtoAuthorizer) SetLabel(v string) {
	o.Label = &v
}

// GetClientId returns the ClientId field value if set, zero value otherwise.
func (o *GoogleSheetsCustomDtoAuthorizer) GetClientId() string {
	if o == nil || IsNil(o.ClientId) {
		var ret string
		return ret
	}
	return *o.ClientId
}

// GetClientIdOk returns a tuple with the ClientId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleSheetsCustomDtoAuthorizer) GetClientIdOk() (*string, bool) {
	if o == nil || IsNil(o.ClientId) {
		return nil, false
	}
	return o.ClientId, true
}

// HasClientId returns a boolean if a field has been set.
func (o *GoogleSheetsCustomDtoAuthorizer) HasClientId() bool {
	if o != nil && !IsNil(o.ClientId) {
		return true
	}

	return false
}

// SetClientId gets a reference to the given string and assigns it to the ClientId field.
func (o *GoogleSheetsCustomDtoAuthorizer) SetClientId(v string) {
	o.ClientId = &v
}

// GetClientSecret returns the ClientSecret field value if set, zero value otherwise.
func (o *GoogleSheetsCustomDtoAuthorizer) GetClientSecret() string {
	if o == nil || IsNil(o.ClientSecret) {
		var ret string
		return ret
	}
	return *o.ClientSecret
}

// GetClientSecretOk returns a tuple with the ClientSecret field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleSheetsCustomDtoAuthorizer) GetClientSecretOk() (*string, bool) {
	if o == nil || IsNil(o.ClientSecret) {
		return nil, false
	}
	return o.ClientSecret, true
}

// HasClientSecret returns a boolean if a field has been set.
func (o *GoogleSheetsCustomDtoAuthorizer) HasClientSecret() bool {
	if o != nil && !IsNil(o.ClientSecret) {
		return true
	}

	return false
}

// SetClientSecret gets a reference to the given string and assigns it to the ClientSecret field.
func (o *GoogleSheetsCustomDtoAuthorizer) SetClientSecret(v string) {
	o.ClientSecret = &v
}

// GetRedirectUri returns the RedirectUri field value if set, zero value otherwise.
func (o *GoogleSheetsCustomDtoAuthorizer) GetRedirectUri() string {
	if o == nil || IsNil(o.RedirectUri) {
		var ret string
		return ret
	}
	return *o.RedirectUri
}

// GetRedirectUriOk returns a tuple with the RedirectUri field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GoogleSheetsCustomDtoAuthorizer) GetRedirectUriOk() (*string, bool) {
	if o == nil || IsNil(o.RedirectUri) {
		return nil, false
	}
	return o.RedirectUri, true
}

// HasRedirectUri returns a boolean if a field has been set.
func (o *GoogleSheetsCustomDtoAuthorizer) HasRedirectUri() bool {
	if o != nil && !IsNil(o.RedirectUri) {
		return true
	}

	return false
}

// SetRedirectUri gets a reference to the given string and assigns it to the RedirectUri field.
func (o *GoogleSheetsCustomDtoAuthorizer) SetRedirectUri(v string) {
	o.RedirectUri = &v
}

func (o GoogleSheetsCustomDtoAuthorizer) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GoogleSheetsCustomDtoAuthorizer) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.ClientId) {
		toSerialize["clientId"] = o.ClientId
	}
	if !IsNil(o.ClientSecret) {
		toSerialize["clientSecret"] = o.ClientSecret
	}
	if !IsNil(o.RedirectUri) {
		toSerialize["redirectUri"] = o.RedirectUri
	}
	return toSerialize, nil
}

type NullableGoogleSheetsCustomDtoAuthorizer struct {
	value *GoogleSheetsCustomDtoAuthorizer
	isSet bool
}

func (v NullableGoogleSheetsCustomDtoAuthorizer) Get() *GoogleSheetsCustomDtoAuthorizer {
	return v.value
}

func (v *NullableGoogleSheetsCustomDtoAuthorizer) Set(val *GoogleSheetsCustomDtoAuthorizer) {
	v.value = val
	v.isSet = true
}

func (v NullableGoogleSheetsCustomDtoAuthorizer) IsSet() bool {
	return v.isSet
}

func (v *NullableGoogleSheetsCustomDtoAuthorizer) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGoogleSheetsCustomDtoAuthorizer(val *GoogleSheetsCustomDtoAuthorizer) *NullableGoogleSheetsCustomDtoAuthorizer {
	return &NullableGoogleSheetsCustomDtoAuthorizer{value: val, isSet: true}
}

func (v NullableGoogleSheetsCustomDtoAuthorizer) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGoogleSheetsCustomDtoAuthorizer) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
