/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the HttpConnectorDto type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &HttpConnectorDto{}

// HttpConnectorDto struct for HttpConnectorDto
type HttpConnectorDto struct {
	ConnectorId *string                         `json:"connectorId,omitempty"`
	TemplateId  *string                         `json:"templateId,omitempty"`
	Label       *string                         `json:"label,omitempty"`
	Strategy    *string                         `json:"strategy,omitempty"`
	Request     NullableHttpConnectorDtoRequest `json:"request,omitempty"`
}

// NewHttpConnectorDto instantiates a new HttpConnectorDto object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewHttpConnectorDto() *HttpConnectorDto {
	this := HttpConnectorDto{}
	return &this
}

// NewHttpConnectorDtoWithDefaults instantiates a new HttpConnectorDto object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewHttpConnectorDtoWithDefaults() *HttpConnectorDto {
	this := HttpConnectorDto{}
	return &this
}

// GetConnectorId returns the ConnectorId field value if set, zero value otherwise.
func (o *HttpConnectorDto) GetConnectorId() string {
	if o == nil || IsNil(o.ConnectorId) {
		var ret string
		return ret
	}
	return *o.ConnectorId
}

// GetConnectorIdOk returns a tuple with the ConnectorId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *HttpConnectorDto) GetConnectorIdOk() (*string, bool) {
	if o == nil || IsNil(o.ConnectorId) {
		return nil, false
	}
	return o.ConnectorId, true
}

// HasConnectorId returns a boolean if a field has been set.
func (o *HttpConnectorDto) HasConnectorId() bool {
	if o != nil && !IsNil(o.ConnectorId) {
		return true
	}

	return false
}

// SetConnectorId gets a reference to the given string and assigns it to the ConnectorId field.
func (o *HttpConnectorDto) SetConnectorId(v string) {
	o.ConnectorId = &v
}

// GetTemplateId returns the TemplateId field value if set, zero value otherwise.
func (o *HttpConnectorDto) GetTemplateId() string {
	if o == nil || IsNil(o.TemplateId) {
		var ret string
		return ret
	}
	return *o.TemplateId
}

// GetTemplateIdOk returns a tuple with the TemplateId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *HttpConnectorDto) GetTemplateIdOk() (*string, bool) {
	if o == nil || IsNil(o.TemplateId) {
		return nil, false
	}
	return o.TemplateId, true
}

// HasTemplateId returns a boolean if a field has been set.
func (o *HttpConnectorDto) HasTemplateId() bool {
	if o != nil && !IsNil(o.TemplateId) {
		return true
	}

	return false
}

// SetTemplateId gets a reference to the given string and assigns it to the TemplateId field.
func (o *HttpConnectorDto) SetTemplateId(v string) {
	o.TemplateId = &v
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *HttpConnectorDto) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *HttpConnectorDto) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *HttpConnectorDto) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *HttpConnectorDto) SetLabel(v string) {
	o.Label = &v
}

// GetStrategy returns the Strategy field value if set, zero value otherwise.
func (o *HttpConnectorDto) GetStrategy() string {
	if o == nil || IsNil(o.Strategy) {
		var ret string
		return ret
	}
	return *o.Strategy
}

// GetStrategyOk returns a tuple with the Strategy field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *HttpConnectorDto) GetStrategyOk() (*string, bool) {
	if o == nil || IsNil(o.Strategy) {
		return nil, false
	}
	return o.Strategy, true
}

// HasStrategy returns a boolean if a field has been set.
func (o *HttpConnectorDto) HasStrategy() bool {
	if o != nil && !IsNil(o.Strategy) {
		return true
	}

	return false
}

// SetStrategy gets a reference to the given string and assigns it to the Strategy field.
func (o *HttpConnectorDto) SetStrategy(v string) {
	o.Strategy = &v
}

// GetRequest returns the Request field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *HttpConnectorDto) GetRequest() HttpConnectorDtoRequest {
	if o == nil || IsNil(o.Request.Get()) {
		var ret HttpConnectorDtoRequest
		return ret
	}
	return *o.Request.Get()
}

// GetRequestOk returns a tuple with the Request field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *HttpConnectorDto) GetRequestOk() (*HttpConnectorDtoRequest, bool) {
	if o == nil {
		return nil, false
	}
	return o.Request.Get(), o.Request.IsSet()
}

// HasRequest returns a boolean if a field has been set.
func (o *HttpConnectorDto) HasRequest() bool {
	if o != nil && o.Request.IsSet() {
		return true
	}

	return false
}

// SetRequest gets a reference to the given NullableHttpConnectorDtoRequest and assigns it to the Request field.
func (o *HttpConnectorDto) SetRequest(v HttpConnectorDtoRequest) {
	o.Request.Set(&v)
}

// SetRequestNil sets the value for Request to be an explicit nil
func (o *HttpConnectorDto) SetRequestNil() {
	o.Request.Set(nil)
}

// UnsetRequest ensures that no value is present for Request, not even an explicit nil
func (o *HttpConnectorDto) UnsetRequest() {
	o.Request.Unset()
}

func (o HttpConnectorDto) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o HttpConnectorDto) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.ConnectorId) {
		toSerialize["connectorId"] = o.ConnectorId
	}
	if !IsNil(o.TemplateId) {
		toSerialize["templateId"] = o.TemplateId
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.Strategy) {
		toSerialize["strategy"] = o.Strategy
	}
	if o.Request.IsSet() {
		toSerialize["request"] = o.Request.Get()
	}
	return toSerialize, nil
}

type NullableHttpConnectorDto struct {
	value *HttpConnectorDto
	isSet bool
}

func (v NullableHttpConnectorDto) Get() *HttpConnectorDto {
	return v.value
}

func (v *NullableHttpConnectorDto) Set(val *HttpConnectorDto) {
	v.value = val
	v.isSet = true
}

func (v NullableHttpConnectorDto) IsSet() bool {
	return v.isSet
}

func (v *NullableHttpConnectorDto) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableHttpConnectorDto(val *HttpConnectorDto) *NullableHttpConnectorDto {
	return &NullableHttpConnectorDto{value: val, isSet: true}
}

func (v NullableHttpConnectorDto) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableHttpConnectorDto) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
