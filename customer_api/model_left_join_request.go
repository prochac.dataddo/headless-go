/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the LeftJoinRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &LeftJoinRequest{}

// LeftJoinRequest struct for LeftJoinRequest
type LeftJoinRequest struct {
	Source *string `json:"source,omitempty"`
	Target *string `json:"target,omitempty"`
	//
	Conditions []JoinConditionRequest `json:"conditions,omitempty"`
	//
	Columns []JoinColumnRequest `json:"columns,omitempty"`
}

// NewLeftJoinRequest instantiates a new LeftJoinRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewLeftJoinRequest() *LeftJoinRequest {
	this := LeftJoinRequest{}
	return &this
}

// NewLeftJoinRequestWithDefaults instantiates a new LeftJoinRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewLeftJoinRequestWithDefaults() *LeftJoinRequest {
	this := LeftJoinRequest{}
	return &this
}

// GetSource returns the Source field value if set, zero value otherwise.
func (o *LeftJoinRequest) GetSource() string {
	if o == nil || IsNil(o.Source) {
		var ret string
		return ret
	}
	return *o.Source
}

// GetSourceOk returns a tuple with the Source field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LeftJoinRequest) GetSourceOk() (*string, bool) {
	if o == nil || IsNil(o.Source) {
		return nil, false
	}
	return o.Source, true
}

// HasSource returns a boolean if a field has been set.
func (o *LeftJoinRequest) HasSource() bool {
	if o != nil && !IsNil(o.Source) {
		return true
	}

	return false
}

// SetSource gets a reference to the given string and assigns it to the Source field.
func (o *LeftJoinRequest) SetSource(v string) {
	o.Source = &v
}

// GetTarget returns the Target field value if set, zero value otherwise.
func (o *LeftJoinRequest) GetTarget() string {
	if o == nil || IsNil(o.Target) {
		var ret string
		return ret
	}
	return *o.Target
}

// GetTargetOk returns a tuple with the Target field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LeftJoinRequest) GetTargetOk() (*string, bool) {
	if o == nil || IsNil(o.Target) {
		return nil, false
	}
	return o.Target, true
}

// HasTarget returns a boolean if a field has been set.
func (o *LeftJoinRequest) HasTarget() bool {
	if o != nil && !IsNil(o.Target) {
		return true
	}

	return false
}

// SetTarget gets a reference to the given string and assigns it to the Target field.
func (o *LeftJoinRequest) SetTarget(v string) {
	o.Target = &v
}

// GetConditions returns the Conditions field value if set, zero value otherwise.
func (o *LeftJoinRequest) GetConditions() []JoinConditionRequest {
	if o == nil || IsNil(o.Conditions) {
		var ret []JoinConditionRequest
		return ret
	}
	return o.Conditions
}

// GetConditionsOk returns a tuple with the Conditions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LeftJoinRequest) GetConditionsOk() ([]JoinConditionRequest, bool) {
	if o == nil || IsNil(o.Conditions) {
		return nil, false
	}
	return o.Conditions, true
}

// HasConditions returns a boolean if a field has been set.
func (o *LeftJoinRequest) HasConditions() bool {
	if o != nil && !IsNil(o.Conditions) {
		return true
	}

	return false
}

// SetConditions gets a reference to the given []JoinConditionRequest and assigns it to the Conditions field.
func (o *LeftJoinRequest) SetConditions(v []JoinConditionRequest) {
	o.Conditions = v
}

// GetColumns returns the Columns field value if set, zero value otherwise.
func (o *LeftJoinRequest) GetColumns() []JoinColumnRequest {
	if o == nil || IsNil(o.Columns) {
		var ret []JoinColumnRequest
		return ret
	}
	return o.Columns
}

// GetColumnsOk returns a tuple with the Columns field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LeftJoinRequest) GetColumnsOk() ([]JoinColumnRequest, bool) {
	if o == nil || IsNil(o.Columns) {
		return nil, false
	}
	return o.Columns, true
}

// HasColumns returns a boolean if a field has been set.
func (o *LeftJoinRequest) HasColumns() bool {
	if o != nil && !IsNil(o.Columns) {
		return true
	}

	return false
}

// SetColumns gets a reference to the given []JoinColumnRequest and assigns it to the Columns field.
func (o *LeftJoinRequest) SetColumns(v []JoinColumnRequest) {
	o.Columns = v
}

func (o LeftJoinRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o LeftJoinRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Source) {
		toSerialize["source"] = o.Source
	}
	if !IsNil(o.Target) {
		toSerialize["target"] = o.Target
	}
	if !IsNil(o.Conditions) {
		toSerialize["conditions"] = o.Conditions
	}
	if !IsNil(o.Columns) {
		toSerialize["columns"] = o.Columns
	}
	return toSerialize, nil
}

type NullableLeftJoinRequest struct {
	value *LeftJoinRequest
	isSet bool
}

func (v NullableLeftJoinRequest) Get() *LeftJoinRequest {
	return v.value
}

func (v *NullableLeftJoinRequest) Set(val *LeftJoinRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableLeftJoinRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableLeftJoinRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableLeftJoinRequest(val *LeftJoinRequest) *NullableLeftJoinRequest {
	return &NullableLeftJoinRequest{value: val, isSet: true}
}

func (v NullableLeftJoinRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableLeftJoinRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
