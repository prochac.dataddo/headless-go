/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the LimitRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &LimitRequest{}

// LimitRequest struct for LimitRequest
type LimitRequest struct {
	Source *string `json:"source,omitempty"`
	Index  *int32  `json:"index,omitempty"`
	Length *int32  `json:"length,omitempty"`
}

// NewLimitRequest instantiates a new LimitRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewLimitRequest() *LimitRequest {
	this := LimitRequest{}
	return &this
}

// NewLimitRequestWithDefaults instantiates a new LimitRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewLimitRequestWithDefaults() *LimitRequest {
	this := LimitRequest{}
	return &this
}

// GetSource returns the Source field value if set, zero value otherwise.
func (o *LimitRequest) GetSource() string {
	if o == nil || IsNil(o.Source) {
		var ret string
		return ret
	}
	return *o.Source
}

// GetSourceOk returns a tuple with the Source field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LimitRequest) GetSourceOk() (*string, bool) {
	if o == nil || IsNil(o.Source) {
		return nil, false
	}
	return o.Source, true
}

// HasSource returns a boolean if a field has been set.
func (o *LimitRequest) HasSource() bool {
	if o != nil && !IsNil(o.Source) {
		return true
	}

	return false
}

// SetSource gets a reference to the given string and assigns it to the Source field.
func (o *LimitRequest) SetSource(v string) {
	o.Source = &v
}

// GetIndex returns the Index field value if set, zero value otherwise.
func (o *LimitRequest) GetIndex() int32 {
	if o == nil || IsNil(o.Index) {
		var ret int32
		return ret
	}
	return *o.Index
}

// GetIndexOk returns a tuple with the Index field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LimitRequest) GetIndexOk() (*int32, bool) {
	if o == nil || IsNil(o.Index) {
		return nil, false
	}
	return o.Index, true
}

// HasIndex returns a boolean if a field has been set.
func (o *LimitRequest) HasIndex() bool {
	if o != nil && !IsNil(o.Index) {
		return true
	}

	return false
}

// SetIndex gets a reference to the given int32 and assigns it to the Index field.
func (o *LimitRequest) SetIndex(v int32) {
	o.Index = &v
}

// GetLength returns the Length field value if set, zero value otherwise.
func (o *LimitRequest) GetLength() int32 {
	if o == nil || IsNil(o.Length) {
		var ret int32
		return ret
	}
	return *o.Length
}

// GetLengthOk returns a tuple with the Length field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LimitRequest) GetLengthOk() (*int32, bool) {
	if o == nil || IsNil(o.Length) {
		return nil, false
	}
	return o.Length, true
}

// HasLength returns a boolean if a field has been set.
func (o *LimitRequest) HasLength() bool {
	if o != nil && !IsNil(o.Length) {
		return true
	}

	return false
}

// SetLength gets a reference to the given int32 and assigns it to the Length field.
func (o *LimitRequest) SetLength(v int32) {
	o.Length = &v
}

func (o LimitRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o LimitRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Source) {
		toSerialize["source"] = o.Source
	}
	if !IsNil(o.Index) {
		toSerialize["index"] = o.Index
	}
	if !IsNil(o.Length) {
		toSerialize["length"] = o.Length
	}
	return toSerialize, nil
}

type NullableLimitRequest struct {
	value *LimitRequest
	isSet bool
}

func (v NullableLimitRequest) Get() *LimitRequest {
	return v.value
}

func (v *NullableLimitRequest) Set(val *LimitRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableLimitRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableLimitRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableLimitRequest(val *LimitRequest) *NullableLimitRequest {
	return &NullableLimitRequest{value: val, isSet: true}
}

func (v NullableLimitRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableLimitRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
