/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"time"
)

// checks if the MemberAllOf type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MemberAllOf{}

// MemberAllOf struct for MemberAllOf
type MemberAllOf struct {
	Email             *string        `json:"email,omitempty"`
	FirstName         NullableString `json:"firstName,omitempty"`
	LastName          NullableString `json:"lastName,omitempty"`
	HubspotContactId  NullableString `json:"hubspotContactId,omitempty"`
	JobTitle          NullableString `json:"jobTitle,omitempty"`
	Department        NullableString `json:"department,omitempty"`
	IsMfa             *bool          `json:"isMfa,omitempty"`
	LastLogin         NullableTime   `json:"lastLogin,omitempty"`
	InvalidLoginCount *int32         `json:"invalidLoginCount,omitempty"`
}

// NewMemberAllOf instantiates a new MemberAllOf object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMemberAllOf() *MemberAllOf {
	this := MemberAllOf{}
	return &this
}

// NewMemberAllOfWithDefaults instantiates a new MemberAllOf object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMemberAllOfWithDefaults() *MemberAllOf {
	this := MemberAllOf{}
	return &this
}

// GetEmail returns the Email field value if set, zero value otherwise.
func (o *MemberAllOf) GetEmail() string {
	if o == nil || IsNil(o.Email) {
		var ret string
		return ret
	}
	return *o.Email
}

// GetEmailOk returns a tuple with the Email field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MemberAllOf) GetEmailOk() (*string, bool) {
	if o == nil || IsNil(o.Email) {
		return nil, false
	}
	return o.Email, true
}

// HasEmail returns a boolean if a field has been set.
func (o *MemberAllOf) HasEmail() bool {
	if o != nil && !IsNil(o.Email) {
		return true
	}

	return false
}

// SetEmail gets a reference to the given string and assigns it to the Email field.
func (o *MemberAllOf) SetEmail(v string) {
	o.Email = &v
}

// GetFirstName returns the FirstName field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MemberAllOf) GetFirstName() string {
	if o == nil || IsNil(o.FirstName.Get()) {
		var ret string
		return ret
	}
	return *o.FirstName.Get()
}

// GetFirstNameOk returns a tuple with the FirstName field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MemberAllOf) GetFirstNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.FirstName.Get(), o.FirstName.IsSet()
}

// HasFirstName returns a boolean if a field has been set.
func (o *MemberAllOf) HasFirstName() bool {
	if o != nil && o.FirstName.IsSet() {
		return true
	}

	return false
}

// SetFirstName gets a reference to the given NullableString and assigns it to the FirstName field.
func (o *MemberAllOf) SetFirstName(v string) {
	o.FirstName.Set(&v)
}

// SetFirstNameNil sets the value for FirstName to be an explicit nil
func (o *MemberAllOf) SetFirstNameNil() {
	o.FirstName.Set(nil)
}

// UnsetFirstName ensures that no value is present for FirstName, not even an explicit nil
func (o *MemberAllOf) UnsetFirstName() {
	o.FirstName.Unset()
}

// GetLastName returns the LastName field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MemberAllOf) GetLastName() string {
	if o == nil || IsNil(o.LastName.Get()) {
		var ret string
		return ret
	}
	return *o.LastName.Get()
}

// GetLastNameOk returns a tuple with the LastName field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MemberAllOf) GetLastNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.LastName.Get(), o.LastName.IsSet()
}

// HasLastName returns a boolean if a field has been set.
func (o *MemberAllOf) HasLastName() bool {
	if o != nil && o.LastName.IsSet() {
		return true
	}

	return false
}

// SetLastName gets a reference to the given NullableString and assigns it to the LastName field.
func (o *MemberAllOf) SetLastName(v string) {
	o.LastName.Set(&v)
}

// SetLastNameNil sets the value for LastName to be an explicit nil
func (o *MemberAllOf) SetLastNameNil() {
	o.LastName.Set(nil)
}

// UnsetLastName ensures that no value is present for LastName, not even an explicit nil
func (o *MemberAllOf) UnsetLastName() {
	o.LastName.Unset()
}

// GetHubspotContactId returns the HubspotContactId field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MemberAllOf) GetHubspotContactId() string {
	if o == nil || IsNil(o.HubspotContactId.Get()) {
		var ret string
		return ret
	}
	return *o.HubspotContactId.Get()
}

// GetHubspotContactIdOk returns a tuple with the HubspotContactId field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MemberAllOf) GetHubspotContactIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.HubspotContactId.Get(), o.HubspotContactId.IsSet()
}

// HasHubspotContactId returns a boolean if a field has been set.
func (o *MemberAllOf) HasHubspotContactId() bool {
	if o != nil && o.HubspotContactId.IsSet() {
		return true
	}

	return false
}

// SetHubspotContactId gets a reference to the given NullableString and assigns it to the HubspotContactId field.
func (o *MemberAllOf) SetHubspotContactId(v string) {
	o.HubspotContactId.Set(&v)
}

// SetHubspotContactIdNil sets the value for HubspotContactId to be an explicit nil
func (o *MemberAllOf) SetHubspotContactIdNil() {
	o.HubspotContactId.Set(nil)
}

// UnsetHubspotContactId ensures that no value is present for HubspotContactId, not even an explicit nil
func (o *MemberAllOf) UnsetHubspotContactId() {
	o.HubspotContactId.Unset()
}

// GetJobTitle returns the JobTitle field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MemberAllOf) GetJobTitle() string {
	if o == nil || IsNil(o.JobTitle.Get()) {
		var ret string
		return ret
	}
	return *o.JobTitle.Get()
}

// GetJobTitleOk returns a tuple with the JobTitle field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MemberAllOf) GetJobTitleOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.JobTitle.Get(), o.JobTitle.IsSet()
}

// HasJobTitle returns a boolean if a field has been set.
func (o *MemberAllOf) HasJobTitle() bool {
	if o != nil && o.JobTitle.IsSet() {
		return true
	}

	return false
}

// SetJobTitle gets a reference to the given NullableString and assigns it to the JobTitle field.
func (o *MemberAllOf) SetJobTitle(v string) {
	o.JobTitle.Set(&v)
}

// SetJobTitleNil sets the value for JobTitle to be an explicit nil
func (o *MemberAllOf) SetJobTitleNil() {
	o.JobTitle.Set(nil)
}

// UnsetJobTitle ensures that no value is present for JobTitle, not even an explicit nil
func (o *MemberAllOf) UnsetJobTitle() {
	o.JobTitle.Unset()
}

// GetDepartment returns the Department field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MemberAllOf) GetDepartment() string {
	if o == nil || IsNil(o.Department.Get()) {
		var ret string
		return ret
	}
	return *o.Department.Get()
}

// GetDepartmentOk returns a tuple with the Department field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MemberAllOf) GetDepartmentOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Department.Get(), o.Department.IsSet()
}

// HasDepartment returns a boolean if a field has been set.
func (o *MemberAllOf) HasDepartment() bool {
	if o != nil && o.Department.IsSet() {
		return true
	}

	return false
}

// SetDepartment gets a reference to the given NullableString and assigns it to the Department field.
func (o *MemberAllOf) SetDepartment(v string) {
	o.Department.Set(&v)
}

// SetDepartmentNil sets the value for Department to be an explicit nil
func (o *MemberAllOf) SetDepartmentNil() {
	o.Department.Set(nil)
}

// UnsetDepartment ensures that no value is present for Department, not even an explicit nil
func (o *MemberAllOf) UnsetDepartment() {
	o.Department.Unset()
}

// GetIsMfa returns the IsMfa field value if set, zero value otherwise.
func (o *MemberAllOf) GetIsMfa() bool {
	if o == nil || IsNil(o.IsMfa) {
		var ret bool
		return ret
	}
	return *o.IsMfa
}

// GetIsMfaOk returns a tuple with the IsMfa field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MemberAllOf) GetIsMfaOk() (*bool, bool) {
	if o == nil || IsNil(o.IsMfa) {
		return nil, false
	}
	return o.IsMfa, true
}

// HasIsMfa returns a boolean if a field has been set.
func (o *MemberAllOf) HasIsMfa() bool {
	if o != nil && !IsNil(o.IsMfa) {
		return true
	}

	return false
}

// SetIsMfa gets a reference to the given bool and assigns it to the IsMfa field.
func (o *MemberAllOf) SetIsMfa(v bool) {
	o.IsMfa = &v
}

// GetLastLogin returns the LastLogin field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MemberAllOf) GetLastLogin() time.Time {
	if o == nil || IsNil(o.LastLogin.Get()) {
		var ret time.Time
		return ret
	}
	return *o.LastLogin.Get()
}

// GetLastLoginOk returns a tuple with the LastLogin field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MemberAllOf) GetLastLoginOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return o.LastLogin.Get(), o.LastLogin.IsSet()
}

// HasLastLogin returns a boolean if a field has been set.
func (o *MemberAllOf) HasLastLogin() bool {
	if o != nil && o.LastLogin.IsSet() {
		return true
	}

	return false
}

// SetLastLogin gets a reference to the given NullableTime and assigns it to the LastLogin field.
func (o *MemberAllOf) SetLastLogin(v time.Time) {
	o.LastLogin.Set(&v)
}

// SetLastLoginNil sets the value for LastLogin to be an explicit nil
func (o *MemberAllOf) SetLastLoginNil() {
	o.LastLogin.Set(nil)
}

// UnsetLastLogin ensures that no value is present for LastLogin, not even an explicit nil
func (o *MemberAllOf) UnsetLastLogin() {
	o.LastLogin.Unset()
}

// GetInvalidLoginCount returns the InvalidLoginCount field value if set, zero value otherwise.
func (o *MemberAllOf) GetInvalidLoginCount() int32 {
	if o == nil || IsNil(o.InvalidLoginCount) {
		var ret int32
		return ret
	}
	return *o.InvalidLoginCount
}

// GetInvalidLoginCountOk returns a tuple with the InvalidLoginCount field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MemberAllOf) GetInvalidLoginCountOk() (*int32, bool) {
	if o == nil || IsNil(o.InvalidLoginCount) {
		return nil, false
	}
	return o.InvalidLoginCount, true
}

// HasInvalidLoginCount returns a boolean if a field has been set.
func (o *MemberAllOf) HasInvalidLoginCount() bool {
	if o != nil && !IsNil(o.InvalidLoginCount) {
		return true
	}

	return false
}

// SetInvalidLoginCount gets a reference to the given int32 and assigns it to the InvalidLoginCount field.
func (o *MemberAllOf) SetInvalidLoginCount(v int32) {
	o.InvalidLoginCount = &v
}

func (o MemberAllOf) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MemberAllOf) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Email) {
		toSerialize["email"] = o.Email
	}
	if o.FirstName.IsSet() {
		toSerialize["firstName"] = o.FirstName.Get()
	}
	if o.LastName.IsSet() {
		toSerialize["lastName"] = o.LastName.Get()
	}
	if o.HubspotContactId.IsSet() {
		toSerialize["hubspotContactId"] = o.HubspotContactId.Get()
	}
	if o.JobTitle.IsSet() {
		toSerialize["jobTitle"] = o.JobTitle.Get()
	}
	if o.Department.IsSet() {
		toSerialize["department"] = o.Department.Get()
	}
	if !IsNil(o.IsMfa) {
		toSerialize["isMfa"] = o.IsMfa
	}
	if o.LastLogin.IsSet() {
		toSerialize["lastLogin"] = o.LastLogin.Get()
	}
	if !IsNil(o.InvalidLoginCount) {
		toSerialize["invalidLoginCount"] = o.InvalidLoginCount
	}
	return toSerialize, nil
}

type NullableMemberAllOf struct {
	value *MemberAllOf
	isSet bool
}

func (v NullableMemberAllOf) Get() *MemberAllOf {
	return v.value
}

func (v *NullableMemberAllOf) Set(val *MemberAllOf) {
	v.value = val
	v.isSet = true
}

func (v NullableMemberAllOf) IsSet() bool {
	return v.isSet
}

func (v *NullableMemberAllOf) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMemberAllOf(val *MemberAllOf) *NullableMemberAllOf {
	return &NullableMemberAllOf{value: val, isSet: true}
}

func (v NullableMemberAllOf) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMemberAllOf) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
