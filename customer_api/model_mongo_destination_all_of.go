/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the MongoDestinationAllOf type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MongoDestinationAllOf{}

// MongoDestinationAllOf struct for MongoDestinationAllOf
type MongoDestinationAllOf struct {
	OAuthId *int32 `json:"o_auth_id,omitempty"`
}

// NewMongoDestinationAllOf instantiates a new MongoDestinationAllOf object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMongoDestinationAllOf() *MongoDestinationAllOf {
	this := MongoDestinationAllOf{}
	return &this
}

// NewMongoDestinationAllOfWithDefaults instantiates a new MongoDestinationAllOf object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMongoDestinationAllOfWithDefaults() *MongoDestinationAllOf {
	this := MongoDestinationAllOf{}
	return &this
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise.
func (o *MongoDestinationAllOf) GetOAuthId() int32 {
	if o == nil || IsNil(o.OAuthId) {
		var ret int32
		return ret
	}
	return *o.OAuthId
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MongoDestinationAllOf) GetOAuthIdOk() (*int32, bool) {
	if o == nil || IsNil(o.OAuthId) {
		return nil, false
	}
	return o.OAuthId, true
}

// HasOAuthId returns a boolean if a field has been set.
func (o *MongoDestinationAllOf) HasOAuthId() bool {
	if o != nil && !IsNil(o.OAuthId) {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given int32 and assigns it to the OAuthId field.
func (o *MongoDestinationAllOf) SetOAuthId(v int32) {
	o.OAuthId = &v
}

func (o MongoDestinationAllOf) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MongoDestinationAllOf) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.OAuthId) {
		toSerialize["o_auth_id"] = o.OAuthId
	}
	return toSerialize, nil
}

type NullableMongoDestinationAllOf struct {
	value *MongoDestinationAllOf
	isSet bool
}

func (v NullableMongoDestinationAllOf) Get() *MongoDestinationAllOf {
	return v.value
}

func (v *NullableMongoDestinationAllOf) Set(val *MongoDestinationAllOf) {
	v.value = val
	v.isSet = true
}

func (v NullableMongoDestinationAllOf) IsSet() bool {
	return v.isSet
}

func (v *NullableMongoDestinationAllOf) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMongoDestinationAllOf(val *MongoDestinationAllOf) *NullableMongoDestinationAllOf {
	return &NullableMongoDestinationAllOf{value: val, isSet: true}
}

func (v NullableMongoDestinationAllOf) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMongoDestinationAllOf) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
