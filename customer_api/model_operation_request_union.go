/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"fmt"
)

// OperationRequestUnion - struct for OperationRequestUnion
type OperationRequestUnion struct {
	UnionRequest *UnionRequest
}

// UnionRequestAsOperationRequestUnion is a convenience function that returns UnionRequest wrapped in OperationRequestUnion
func UnionRequestAsOperationRequestUnion(v *UnionRequest) OperationRequestUnion {
	return OperationRequestUnion{
		UnionRequest: v,
	}
}

// Unmarshal JSON data into one of the pointers in the struct
func (dst *OperationRequestUnion) UnmarshalJSON(data []byte) error {
	var err error
	// this object is nullable so check if the payload is null or empty string
	if string(data) == "" || string(data) == "{}" {
		return nil
	}

	match := 0
	// try to unmarshal data into UnionRequest
	err = newStrictDecoder(data).Decode(&dst.UnionRequest)
	if err == nil {
		jsonUnionRequest, _ := json.Marshal(dst.UnionRequest)
		if string(jsonUnionRequest) == "{}" { // empty struct
			dst.UnionRequest = nil
		} else {
			match++
		}
	} else {
		dst.UnionRequest = nil
	}

	if match > 1 { // more than 1 match
		// reset to nil
		dst.UnionRequest = nil

		return fmt.Errorf("data matches more than one schema in oneOf(OperationRequestUnion)")
	} else if match == 1 {
		return nil // exactly one match
	} else { // no match
		return fmt.Errorf("data failed to match schemas in oneOf(OperationRequestUnion)")
	}
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src OperationRequestUnion) MarshalJSON() ([]byte, error) {
	if src.UnionRequest != nil {
		return json.Marshal(&src.UnionRequest)
	}

	return nil, nil // no data in oneOf schemas
}

// Get the actual instance
func (obj *OperationRequestUnion) GetActualInstance() interface{} {
	if obj == nil {
		return nil
	}
	if obj.UnionRequest != nil {
		return obj.UnionRequest
	}

	// all schemas are nil
	return nil
}

type NullableOperationRequestUnion struct {
	value *OperationRequestUnion
	isSet bool
}

func (v NullableOperationRequestUnion) Get() *OperationRequestUnion {
	return v.value
}

func (v *NullableOperationRequestUnion) Set(val *OperationRequestUnion) {
	v.value = val
	v.isSet = true
}

func (v NullableOperationRequestUnion) IsSet() bool {
	return v.isSet
}

func (v *NullableOperationRequestUnion) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableOperationRequestUnion(val *OperationRequestUnion) *NullableOperationRequestUnion {
	return &NullableOperationRequestUnion{value: val, isSet: true}
}

func (v NullableOperationRequestUnion) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableOperationRequestUnion) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
