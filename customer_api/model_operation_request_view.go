/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"fmt"
)

// OperationRequestView - struct for OperationRequestView
type OperationRequestView struct {
	ViewRequest *ViewRequest
}

// ViewRequestAsOperationRequestView is a convenience function that returns ViewRequest wrapped in OperationRequestView
func ViewRequestAsOperationRequestView(v *ViewRequest) OperationRequestView {
	return OperationRequestView{
		ViewRequest: v,
	}
}

// Unmarshal JSON data into one of the pointers in the struct
func (dst *OperationRequestView) UnmarshalJSON(data []byte) error {
	var err error
	// this object is nullable so check if the payload is null or empty string
	if string(data) == "" || string(data) == "{}" {
		return nil
	}

	match := 0
	// try to unmarshal data into ViewRequest
	err = newStrictDecoder(data).Decode(&dst.ViewRequest)
	if err == nil {
		jsonViewRequest, _ := json.Marshal(dst.ViewRequest)
		if string(jsonViewRequest) == "{}" { // empty struct
			dst.ViewRequest = nil
		} else {
			match++
		}
	} else {
		dst.ViewRequest = nil
	}

	if match > 1 { // more than 1 match
		// reset to nil
		dst.ViewRequest = nil

		return fmt.Errorf("data matches more than one schema in oneOf(OperationRequestView)")
	} else if match == 1 {
		return nil // exactly one match
	} else { // no match
		return fmt.Errorf("data failed to match schemas in oneOf(OperationRequestView)")
	}
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src OperationRequestView) MarshalJSON() ([]byte, error) {
	if src.ViewRequest != nil {
		return json.Marshal(&src.ViewRequest)
	}

	return nil, nil // no data in oneOf schemas
}

// Get the actual instance
func (obj *OperationRequestView) GetActualInstance() interface{} {
	if obj == nil {
		return nil
	}
	if obj.ViewRequest != nil {
		return obj.ViewRequest
	}

	// all schemas are nil
	return nil
}

type NullableOperationRequestView struct {
	value *OperationRequestView
	isSet bool
}

func (v NullableOperationRequestView) Get() *OperationRequestView {
	return v.value
}

func (v *NullableOperationRequestView) Set(val *OperationRequestView) {
	v.value = val
	v.isSet = true
}

func (v NullableOperationRequestView) IsSet() bool {
	return v.isSet
}

func (v *NullableOperationRequestView) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableOperationRequestView(val *OperationRequestView) *NullableOperationRequestView {
	return &NullableOperationRequestView{value: val, isSet: true}
}

func (v NullableOperationRequestView) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableOperationRequestView) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
