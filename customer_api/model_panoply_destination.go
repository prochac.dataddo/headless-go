/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
	"time"
)

// checks if the PanoplyDestination type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &PanoplyDestination{}

// PanoplyDestination struct for PanoplyDestination
type PanoplyDestination struct {
	Id           NullableString `json:"id,omitempty"`
	CustomerId   *string        `json:"customer_id,omitempty"`
	CreatedAt    NullableInt32  `json:"created_at,omitempty"`
	UpdatedAt    NullableInt32  `json:"updated_at,omitempty"`
	LastUse      NullableTime   `json:"lastUse,omitempty"`
	Label        *string        `json:"label,omitempty"`
	Description  *string        `json:"description,omitempty"`
	Status       *bool          `json:"status,omitempty"`
	StatusDetail *string        `json:"statusDetail,omitempty"`
	Type         *string        `json:"type,omitempty"`
	OAuthId      *int32         `json:"o_auth_id,omitempty"`
	ProjectId    *string        `json:"project_id,omitempty"`
	DatasetId    *string        `json:"dataset_id,omitempty"`
}

// NewPanoplyDestination instantiates a new PanoplyDestination object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPanoplyDestination() *PanoplyDestination {
	this := PanoplyDestination{}
	return &this
}

// NewPanoplyDestinationWithDefaults instantiates a new PanoplyDestination object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPanoplyDestinationWithDefaults() *PanoplyDestination {
	this := PanoplyDestination{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PanoplyDestination) GetId() string {
	if o == nil || IsNil(o.Id.Get()) {
		var ret string
		return ret
	}
	return *o.Id.Get()
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PanoplyDestination) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Id.Get(), o.Id.IsSet()
}

// HasId returns a boolean if a field has been set.
func (o *PanoplyDestination) HasId() bool {
	if o != nil && o.Id.IsSet() {
		return true
	}

	return false
}

// SetId gets a reference to the given NullableString and assigns it to the Id field.
func (o *PanoplyDestination) SetId(v string) {
	o.Id.Set(&v)
}

// SetIdNil sets the value for Id to be an explicit nil
func (o *PanoplyDestination) SetIdNil() {
	o.Id.Set(nil)
}

// UnsetId ensures that no value is present for Id, not even an explicit nil
func (o *PanoplyDestination) UnsetId() {
	o.Id.Unset()
}

// GetCustomerId returns the CustomerId field value if set, zero value otherwise.
func (o *PanoplyDestination) GetCustomerId() string {
	if o == nil || IsNil(o.CustomerId) {
		var ret string
		return ret
	}
	return *o.CustomerId
}

// GetCustomerIdOk returns a tuple with the CustomerId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetCustomerIdOk() (*string, bool) {
	if o == nil || IsNil(o.CustomerId) {
		return nil, false
	}
	return o.CustomerId, true
}

// HasCustomerId returns a boolean if a field has been set.
func (o *PanoplyDestination) HasCustomerId() bool {
	if o != nil && !IsNil(o.CustomerId) {
		return true
	}

	return false
}

// SetCustomerId gets a reference to the given string and assigns it to the CustomerId field.
func (o *PanoplyDestination) SetCustomerId(v string) {
	o.CustomerId = &v
}

// GetCreatedAt returns the CreatedAt field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PanoplyDestination) GetCreatedAt() int32 {
	if o == nil || IsNil(o.CreatedAt.Get()) {
		var ret int32
		return ret
	}
	return *o.CreatedAt.Get()
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PanoplyDestination) GetCreatedAtOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.CreatedAt.Get(), o.CreatedAt.IsSet()
}

// HasCreatedAt returns a boolean if a field has been set.
func (o *PanoplyDestination) HasCreatedAt() bool {
	if o != nil && o.CreatedAt.IsSet() {
		return true
	}

	return false
}

// SetCreatedAt gets a reference to the given NullableInt32 and assigns it to the CreatedAt field.
func (o *PanoplyDestination) SetCreatedAt(v int32) {
	o.CreatedAt.Set(&v)
}

// SetCreatedAtNil sets the value for CreatedAt to be an explicit nil
func (o *PanoplyDestination) SetCreatedAtNil() {
	o.CreatedAt.Set(nil)
}

// UnsetCreatedAt ensures that no value is present for CreatedAt, not even an explicit nil
func (o *PanoplyDestination) UnsetCreatedAt() {
	o.CreatedAt.Unset()
}

// GetUpdatedAt returns the UpdatedAt field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PanoplyDestination) GetUpdatedAt() int32 {
	if o == nil || IsNil(o.UpdatedAt.Get()) {
		var ret int32
		return ret
	}
	return *o.UpdatedAt.Get()
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PanoplyDestination) GetUpdatedAtOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.UpdatedAt.Get(), o.UpdatedAt.IsSet()
}

// HasUpdatedAt returns a boolean if a field has been set.
func (o *PanoplyDestination) HasUpdatedAt() bool {
	if o != nil && o.UpdatedAt.IsSet() {
		return true
	}

	return false
}

// SetUpdatedAt gets a reference to the given NullableInt32 and assigns it to the UpdatedAt field.
func (o *PanoplyDestination) SetUpdatedAt(v int32) {
	o.UpdatedAt.Set(&v)
}

// SetUpdatedAtNil sets the value for UpdatedAt to be an explicit nil
func (o *PanoplyDestination) SetUpdatedAtNil() {
	o.UpdatedAt.Set(nil)
}

// UnsetUpdatedAt ensures that no value is present for UpdatedAt, not even an explicit nil
func (o *PanoplyDestination) UnsetUpdatedAt() {
	o.UpdatedAt.Unset()
}

// GetLastUse returns the LastUse field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PanoplyDestination) GetLastUse() time.Time {
	if o == nil || IsNil(o.LastUse.Get()) {
		var ret time.Time
		return ret
	}
	return *o.LastUse.Get()
}

// GetLastUseOk returns a tuple with the LastUse field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PanoplyDestination) GetLastUseOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return o.LastUse.Get(), o.LastUse.IsSet()
}

// HasLastUse returns a boolean if a field has been set.
func (o *PanoplyDestination) HasLastUse() bool {
	if o != nil && o.LastUse.IsSet() {
		return true
	}

	return false
}

// SetLastUse gets a reference to the given NullableTime and assigns it to the LastUse field.
func (o *PanoplyDestination) SetLastUse(v time.Time) {
	o.LastUse.Set(&v)
}

// SetLastUseNil sets the value for LastUse to be an explicit nil
func (o *PanoplyDestination) SetLastUseNil() {
	o.LastUse.Set(nil)
}

// UnsetLastUse ensures that no value is present for LastUse, not even an explicit nil
func (o *PanoplyDestination) UnsetLastUse() {
	o.LastUse.Unset()
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *PanoplyDestination) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *PanoplyDestination) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *PanoplyDestination) SetLabel(v string) {
	o.Label = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *PanoplyDestination) GetDescription() string {
	if o == nil || IsNil(o.Description) {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetDescriptionOk() (*string, bool) {
	if o == nil || IsNil(o.Description) {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *PanoplyDestination) HasDescription() bool {
	if o != nil && !IsNil(o.Description) {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *PanoplyDestination) SetDescription(v string) {
	o.Description = &v
}

// GetStatus returns the Status field value if set, zero value otherwise.
func (o *PanoplyDestination) GetStatus() bool {
	if o == nil || IsNil(o.Status) {
		var ret bool
		return ret
	}
	return *o.Status
}

// GetStatusOk returns a tuple with the Status field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetStatusOk() (*bool, bool) {
	if o == nil || IsNil(o.Status) {
		return nil, false
	}
	return o.Status, true
}

// HasStatus returns a boolean if a field has been set.
func (o *PanoplyDestination) HasStatus() bool {
	if o != nil && !IsNil(o.Status) {
		return true
	}

	return false
}

// SetStatus gets a reference to the given bool and assigns it to the Status field.
func (o *PanoplyDestination) SetStatus(v bool) {
	o.Status = &v
}

// GetStatusDetail returns the StatusDetail field value if set, zero value otherwise.
func (o *PanoplyDestination) GetStatusDetail() string {
	if o == nil || IsNil(o.StatusDetail) {
		var ret string
		return ret
	}
	return *o.StatusDetail
}

// GetStatusDetailOk returns a tuple with the StatusDetail field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetStatusDetailOk() (*string, bool) {
	if o == nil || IsNil(o.StatusDetail) {
		return nil, false
	}
	return o.StatusDetail, true
}

// HasStatusDetail returns a boolean if a field has been set.
func (o *PanoplyDestination) HasStatusDetail() bool {
	if o != nil && !IsNil(o.StatusDetail) {
		return true
	}

	return false
}

// SetStatusDetail gets a reference to the given string and assigns it to the StatusDetail field.
func (o *PanoplyDestination) SetStatusDetail(v string) {
	o.StatusDetail = &v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *PanoplyDestination) GetType() string {
	if o == nil || IsNil(o.Type) {
		var ret string
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetTypeOk() (*string, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *PanoplyDestination) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given string and assigns it to the Type field.
func (o *PanoplyDestination) SetType(v string) {
	o.Type = &v
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise.
func (o *PanoplyDestination) GetOAuthId() int32 {
	if o == nil || IsNil(o.OAuthId) {
		var ret int32
		return ret
	}
	return *o.OAuthId
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetOAuthIdOk() (*int32, bool) {
	if o == nil || IsNil(o.OAuthId) {
		return nil, false
	}
	return o.OAuthId, true
}

// HasOAuthId returns a boolean if a field has been set.
func (o *PanoplyDestination) HasOAuthId() bool {
	if o != nil && !IsNil(o.OAuthId) {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given int32 and assigns it to the OAuthId field.
func (o *PanoplyDestination) SetOAuthId(v int32) {
	o.OAuthId = &v
}

// GetProjectId returns the ProjectId field value if set, zero value otherwise.
func (o *PanoplyDestination) GetProjectId() string {
	if o == nil || IsNil(o.ProjectId) {
		var ret string
		return ret
	}
	return *o.ProjectId
}

// GetProjectIdOk returns a tuple with the ProjectId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetProjectIdOk() (*string, bool) {
	if o == nil || IsNil(o.ProjectId) {
		return nil, false
	}
	return o.ProjectId, true
}

// HasProjectId returns a boolean if a field has been set.
func (o *PanoplyDestination) HasProjectId() bool {
	if o != nil && !IsNil(o.ProjectId) {
		return true
	}

	return false
}

// SetProjectId gets a reference to the given string and assigns it to the ProjectId field.
func (o *PanoplyDestination) SetProjectId(v string) {
	o.ProjectId = &v
}

// GetDatasetId returns the DatasetId field value if set, zero value otherwise.
func (o *PanoplyDestination) GetDatasetId() string {
	if o == nil || IsNil(o.DatasetId) {
		var ret string
		return ret
	}
	return *o.DatasetId
}

// GetDatasetIdOk returns a tuple with the DatasetId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PanoplyDestination) GetDatasetIdOk() (*string, bool) {
	if o == nil || IsNil(o.DatasetId) {
		return nil, false
	}
	return o.DatasetId, true
}

// HasDatasetId returns a boolean if a field has been set.
func (o *PanoplyDestination) HasDatasetId() bool {
	if o != nil && !IsNil(o.DatasetId) {
		return true
	}

	return false
}

// SetDatasetId gets a reference to the given string and assigns it to the DatasetId field.
func (o *PanoplyDestination) SetDatasetId(v string) {
	o.DatasetId = &v
}

func (o PanoplyDestination) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o PanoplyDestination) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.Id.IsSet() {
		toSerialize["id"] = o.Id.Get()
	}
	if !IsNil(o.CustomerId) {
		toSerialize["customer_id"] = o.CustomerId
	}
	if o.CreatedAt.IsSet() {
		toSerialize["created_at"] = o.CreatedAt.Get()
	}
	if o.UpdatedAt.IsSet() {
		toSerialize["updated_at"] = o.UpdatedAt.Get()
	}
	if o.LastUse.IsSet() {
		toSerialize["lastUse"] = o.LastUse.Get()
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.Description) {
		toSerialize["description"] = o.Description
	}
	if !IsNil(o.Status) {
		toSerialize["status"] = o.Status
	}
	if !IsNil(o.StatusDetail) {
		toSerialize["statusDetail"] = o.StatusDetail
	}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	if !IsNil(o.OAuthId) {
		toSerialize["o_auth_id"] = o.OAuthId
	}
	if !IsNil(o.ProjectId) {
		toSerialize["project_id"] = o.ProjectId
	}
	if !IsNil(o.DatasetId) {
		toSerialize["dataset_id"] = o.DatasetId
	}
	return toSerialize, nil
}

type NullablePanoplyDestination struct {
	value *PanoplyDestination
	isSet bool
}

func (v NullablePanoplyDestination) Get() *PanoplyDestination {
	return v.value
}

func (v *NullablePanoplyDestination) Set(val *PanoplyDestination) {
	v.value = val
	v.isSet = true
}

func (v NullablePanoplyDestination) IsSet() bool {
	return v.isSet
}

func (v *NullablePanoplyDestination) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePanoplyDestination(val *PanoplyDestination) *NullablePanoplyDestination {
	return &NullablePanoplyDestination{value: val, isSet: true}
}

func (v NullablePanoplyDestination) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePanoplyDestination) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
