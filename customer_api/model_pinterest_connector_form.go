/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the PinterestConnectorForm type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &PinterestConnectorForm{}

// PinterestConnectorForm struct for PinterestConnectorForm
type PinterestConnectorForm struct {
	ConnectorId string `json:"connectorId"`
	TemplateId  string `json:"templateId"`
	Strategy    string `json:"strategy"`
	// Enforcing data types
	EnsureDataTypes map[string]interface{} `json:"ensureDataTypes,omitempty"`
	// Fixed data types Associative array
	ReportLevel *string `json:"reportLevel,omitempty"`
	// OAuthId
	OAuthId *string `json:"oAuthId,omitempty"`
	// Label
	Label *string `json:"label,omitempty"`
	// TikTok advertiser ID
	AdAccountId *string `json:"adAccountId,omitempty"`
	// Data range expression. Required for automation
	DateRange *string `json:"dateRange,omitempty"`
	// Metrics to be included in the result
	Column []string `json:"column,omitempty"`
	// Metrics to be included in the result
	DataLabel []string `json:"dataLabel,omitempty"`
	// ID dimension to be included in the result
	EngagementWindowDays *string `json:"engagementWindowDays,omitempty"`
	// Time dimension to be included in the result
	ViewWindowDays *string `json:"viewWindowDays,omitempty"`
	// Audience dimension to be included in the result
	ConversionReportTime *string `json:"conversionReportTime,omitempty"`
	// The report type
	Granularity *string `json:"granularity,omitempty"`
	// The data level
	ClickWindowDays *string `json:"clickWindowDays,omitempty"`
}

// NewPinterestConnectorForm instantiates a new PinterestConnectorForm object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPinterestConnectorForm(connectorId string, templateId string, strategy string) *PinterestConnectorForm {
	this := PinterestConnectorForm{}
	this.ConnectorId = connectorId
	this.TemplateId = templateId
	this.Strategy = strategy
	return &this
}

// NewPinterestConnectorFormWithDefaults instantiates a new PinterestConnectorForm object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPinterestConnectorFormWithDefaults() *PinterestConnectorForm {
	this := PinterestConnectorForm{}
	return &this
}

// GetConnectorId returns the ConnectorId field value
func (o *PinterestConnectorForm) GetConnectorId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ConnectorId
}

// GetConnectorIdOk returns a tuple with the ConnectorId field value
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetConnectorIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ConnectorId, true
}

// SetConnectorId sets field value
func (o *PinterestConnectorForm) SetConnectorId(v string) {
	o.ConnectorId = v
}

// GetTemplateId returns the TemplateId field value
func (o *PinterestConnectorForm) GetTemplateId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.TemplateId
}

// GetTemplateIdOk returns a tuple with the TemplateId field value
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetTemplateIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.TemplateId, true
}

// SetTemplateId sets field value
func (o *PinterestConnectorForm) SetTemplateId(v string) {
	o.TemplateId = v
}

// GetStrategy returns the Strategy field value
func (o *PinterestConnectorForm) GetStrategy() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Strategy
}

// GetStrategyOk returns a tuple with the Strategy field value
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetStrategyOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Strategy, true
}

// SetStrategy sets field value
func (o *PinterestConnectorForm) SetStrategy(v string) {
	o.Strategy = v
}

// GetEnsureDataTypes returns the EnsureDataTypes field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetEnsureDataTypes() map[string]interface{} {
	if o == nil || IsNil(o.EnsureDataTypes) {
		var ret map[string]interface{}
		return ret
	}
	return o.EnsureDataTypes
}

// GetEnsureDataTypesOk returns a tuple with the EnsureDataTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetEnsureDataTypesOk() (map[string]interface{}, bool) {
	if o == nil || IsNil(o.EnsureDataTypes) {
		return map[string]interface{}{}, false
	}
	return o.EnsureDataTypes, true
}

// HasEnsureDataTypes returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasEnsureDataTypes() bool {
	if o != nil && !IsNil(o.EnsureDataTypes) {
		return true
	}

	return false
}

// SetEnsureDataTypes gets a reference to the given map[string]interface{} and assigns it to the EnsureDataTypes field.
func (o *PinterestConnectorForm) SetEnsureDataTypes(v map[string]interface{}) {
	o.EnsureDataTypes = v
}

// GetReportLevel returns the ReportLevel field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetReportLevel() string {
	if o == nil || IsNil(o.ReportLevel) {
		var ret string
		return ret
	}
	return *o.ReportLevel
}

// GetReportLevelOk returns a tuple with the ReportLevel field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetReportLevelOk() (*string, bool) {
	if o == nil || IsNil(o.ReportLevel) {
		return nil, false
	}
	return o.ReportLevel, true
}

// HasReportLevel returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasReportLevel() bool {
	if o != nil && !IsNil(o.ReportLevel) {
		return true
	}

	return false
}

// SetReportLevel gets a reference to the given string and assigns it to the ReportLevel field.
func (o *PinterestConnectorForm) SetReportLevel(v string) {
	o.ReportLevel = &v
}

// GetOAuthId returns the OAuthId field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetOAuthId() string {
	if o == nil || IsNil(o.OAuthId) {
		var ret string
		return ret
	}
	return *o.OAuthId
}

// GetOAuthIdOk returns a tuple with the OAuthId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetOAuthIdOk() (*string, bool) {
	if o == nil || IsNil(o.OAuthId) {
		return nil, false
	}
	return o.OAuthId, true
}

// HasOAuthId returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasOAuthId() bool {
	if o != nil && !IsNil(o.OAuthId) {
		return true
	}

	return false
}

// SetOAuthId gets a reference to the given string and assigns it to the OAuthId field.
func (o *PinterestConnectorForm) SetOAuthId(v string) {
	o.OAuthId = &v
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *PinterestConnectorForm) SetLabel(v string) {
	o.Label = &v
}

// GetAdAccountId returns the AdAccountId field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetAdAccountId() string {
	if o == nil || IsNil(o.AdAccountId) {
		var ret string
		return ret
	}
	return *o.AdAccountId
}

// GetAdAccountIdOk returns a tuple with the AdAccountId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetAdAccountIdOk() (*string, bool) {
	if o == nil || IsNil(o.AdAccountId) {
		return nil, false
	}
	return o.AdAccountId, true
}

// HasAdAccountId returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasAdAccountId() bool {
	if o != nil && !IsNil(o.AdAccountId) {
		return true
	}

	return false
}

// SetAdAccountId gets a reference to the given string and assigns it to the AdAccountId field.
func (o *PinterestConnectorForm) SetAdAccountId(v string) {
	o.AdAccountId = &v
}

// GetDateRange returns the DateRange field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetDateRange() string {
	if o == nil || IsNil(o.DateRange) {
		var ret string
		return ret
	}
	return *o.DateRange
}

// GetDateRangeOk returns a tuple with the DateRange field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetDateRangeOk() (*string, bool) {
	if o == nil || IsNil(o.DateRange) {
		return nil, false
	}
	return o.DateRange, true
}

// HasDateRange returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasDateRange() bool {
	if o != nil && !IsNil(o.DateRange) {
		return true
	}

	return false
}

// SetDateRange gets a reference to the given string and assigns it to the DateRange field.
func (o *PinterestConnectorForm) SetDateRange(v string) {
	o.DateRange = &v
}

// GetColumn returns the Column field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetColumn() []string {
	if o == nil || IsNil(o.Column) {
		var ret []string
		return ret
	}
	return o.Column
}

// GetColumnOk returns a tuple with the Column field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetColumnOk() ([]string, bool) {
	if o == nil || IsNil(o.Column) {
		return nil, false
	}
	return o.Column, true
}

// HasColumn returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasColumn() bool {
	if o != nil && !IsNil(o.Column) {
		return true
	}

	return false
}

// SetColumn gets a reference to the given []string and assigns it to the Column field.
func (o *PinterestConnectorForm) SetColumn(v []string) {
	o.Column = v
}

// GetDataLabel returns the DataLabel field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetDataLabel() []string {
	if o == nil || IsNil(o.DataLabel) {
		var ret []string
		return ret
	}
	return o.DataLabel
}

// GetDataLabelOk returns a tuple with the DataLabel field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetDataLabelOk() ([]string, bool) {
	if o == nil || IsNil(o.DataLabel) {
		return nil, false
	}
	return o.DataLabel, true
}

// HasDataLabel returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasDataLabel() bool {
	if o != nil && !IsNil(o.DataLabel) {
		return true
	}

	return false
}

// SetDataLabel gets a reference to the given []string and assigns it to the DataLabel field.
func (o *PinterestConnectorForm) SetDataLabel(v []string) {
	o.DataLabel = v
}

// GetEngagementWindowDays returns the EngagementWindowDays field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetEngagementWindowDays() string {
	if o == nil || IsNil(o.EngagementWindowDays) {
		var ret string
		return ret
	}
	return *o.EngagementWindowDays
}

// GetEngagementWindowDaysOk returns a tuple with the EngagementWindowDays field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetEngagementWindowDaysOk() (*string, bool) {
	if o == nil || IsNil(o.EngagementWindowDays) {
		return nil, false
	}
	return o.EngagementWindowDays, true
}

// HasEngagementWindowDays returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasEngagementWindowDays() bool {
	if o != nil && !IsNil(o.EngagementWindowDays) {
		return true
	}

	return false
}

// SetEngagementWindowDays gets a reference to the given string and assigns it to the EngagementWindowDays field.
func (o *PinterestConnectorForm) SetEngagementWindowDays(v string) {
	o.EngagementWindowDays = &v
}

// GetViewWindowDays returns the ViewWindowDays field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetViewWindowDays() string {
	if o == nil || IsNil(o.ViewWindowDays) {
		var ret string
		return ret
	}
	return *o.ViewWindowDays
}

// GetViewWindowDaysOk returns a tuple with the ViewWindowDays field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetViewWindowDaysOk() (*string, bool) {
	if o == nil || IsNil(o.ViewWindowDays) {
		return nil, false
	}
	return o.ViewWindowDays, true
}

// HasViewWindowDays returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasViewWindowDays() bool {
	if o != nil && !IsNil(o.ViewWindowDays) {
		return true
	}

	return false
}

// SetViewWindowDays gets a reference to the given string and assigns it to the ViewWindowDays field.
func (o *PinterestConnectorForm) SetViewWindowDays(v string) {
	o.ViewWindowDays = &v
}

// GetConversionReportTime returns the ConversionReportTime field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetConversionReportTime() string {
	if o == nil || IsNil(o.ConversionReportTime) {
		var ret string
		return ret
	}
	return *o.ConversionReportTime
}

// GetConversionReportTimeOk returns a tuple with the ConversionReportTime field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetConversionReportTimeOk() (*string, bool) {
	if o == nil || IsNil(o.ConversionReportTime) {
		return nil, false
	}
	return o.ConversionReportTime, true
}

// HasConversionReportTime returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasConversionReportTime() bool {
	if o != nil && !IsNil(o.ConversionReportTime) {
		return true
	}

	return false
}

// SetConversionReportTime gets a reference to the given string and assigns it to the ConversionReportTime field.
func (o *PinterestConnectorForm) SetConversionReportTime(v string) {
	o.ConversionReportTime = &v
}

// GetGranularity returns the Granularity field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetGranularity() string {
	if o == nil || IsNil(o.Granularity) {
		var ret string
		return ret
	}
	return *o.Granularity
}

// GetGranularityOk returns a tuple with the Granularity field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetGranularityOk() (*string, bool) {
	if o == nil || IsNil(o.Granularity) {
		return nil, false
	}
	return o.Granularity, true
}

// HasGranularity returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasGranularity() bool {
	if o != nil && !IsNil(o.Granularity) {
		return true
	}

	return false
}

// SetGranularity gets a reference to the given string and assigns it to the Granularity field.
func (o *PinterestConnectorForm) SetGranularity(v string) {
	o.Granularity = &v
}

// GetClickWindowDays returns the ClickWindowDays field value if set, zero value otherwise.
func (o *PinterestConnectorForm) GetClickWindowDays() string {
	if o == nil || IsNil(o.ClickWindowDays) {
		var ret string
		return ret
	}
	return *o.ClickWindowDays
}

// GetClickWindowDaysOk returns a tuple with the ClickWindowDays field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestConnectorForm) GetClickWindowDaysOk() (*string, bool) {
	if o == nil || IsNil(o.ClickWindowDays) {
		return nil, false
	}
	return o.ClickWindowDays, true
}

// HasClickWindowDays returns a boolean if a field has been set.
func (o *PinterestConnectorForm) HasClickWindowDays() bool {
	if o != nil && !IsNil(o.ClickWindowDays) {
		return true
	}

	return false
}

// SetClickWindowDays gets a reference to the given string and assigns it to the ClickWindowDays field.
func (o *PinterestConnectorForm) SetClickWindowDays(v string) {
	o.ClickWindowDays = &v
}

func (o PinterestConnectorForm) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o PinterestConnectorForm) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["connectorId"] = o.ConnectorId
	toSerialize["templateId"] = o.TemplateId
	toSerialize["strategy"] = o.Strategy
	if !IsNil(o.EnsureDataTypes) {
		toSerialize["ensureDataTypes"] = o.EnsureDataTypes
	}
	if !IsNil(o.ReportLevel) {
		toSerialize["reportLevel"] = o.ReportLevel
	}
	if !IsNil(o.OAuthId) {
		toSerialize["oAuthId"] = o.OAuthId
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.AdAccountId) {
		toSerialize["adAccountId"] = o.AdAccountId
	}
	if !IsNil(o.DateRange) {
		toSerialize["dateRange"] = o.DateRange
	}
	if !IsNil(o.Column) {
		toSerialize["column"] = o.Column
	}
	if !IsNil(o.DataLabel) {
		toSerialize["dataLabel"] = o.DataLabel
	}
	if !IsNil(o.EngagementWindowDays) {
		toSerialize["engagementWindowDays"] = o.EngagementWindowDays
	}
	if !IsNil(o.ViewWindowDays) {
		toSerialize["viewWindowDays"] = o.ViewWindowDays
	}
	if !IsNil(o.ConversionReportTime) {
		toSerialize["conversionReportTime"] = o.ConversionReportTime
	}
	if !IsNil(o.Granularity) {
		toSerialize["granularity"] = o.Granularity
	}
	if !IsNil(o.ClickWindowDays) {
		toSerialize["clickWindowDays"] = o.ClickWindowDays
	}
	return toSerialize, nil
}

type NullablePinterestConnectorForm struct {
	value *PinterestConnectorForm
	isSet bool
}

func (v NullablePinterestConnectorForm) Get() *PinterestConnectorForm {
	return v.value
}

func (v *NullablePinterestConnectorForm) Set(val *PinterestConnectorForm) {
	v.value = val
	v.isSet = true
}

func (v NullablePinterestConnectorForm) IsSet() bool {
	return v.isSet
}

func (v *NullablePinterestConnectorForm) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePinterestConnectorForm(val *PinterestConnectorForm) *NullablePinterestConnectorForm {
	return &NullablePinterestConnectorForm{value: val, isSet: true}
}

func (v NullablePinterestConnectorForm) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePinterestConnectorForm) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
