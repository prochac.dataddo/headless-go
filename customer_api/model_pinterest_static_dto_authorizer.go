/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the PinterestStaticDtoAuthorizer type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &PinterestStaticDtoAuthorizer{}

// PinterestStaticDtoAuthorizer struct for PinterestStaticDtoAuthorizer
type PinterestStaticDtoAuthorizer struct {
	StaticKey    *string `json:"staticKey,omitempty"`
	Label        *string `json:"label,omitempty"`
	ClientId     *string `json:"clientId,omitempty"`
	ClientSecret *string `json:"clientSecret,omitempty"`
	RedirectUri  *string `json:"redirectUri,omitempty"`
}

// NewPinterestStaticDtoAuthorizer instantiates a new PinterestStaticDtoAuthorizer object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPinterestStaticDtoAuthorizer() *PinterestStaticDtoAuthorizer {
	this := PinterestStaticDtoAuthorizer{}
	return &this
}

// NewPinterestStaticDtoAuthorizerWithDefaults instantiates a new PinterestStaticDtoAuthorizer object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPinterestStaticDtoAuthorizerWithDefaults() *PinterestStaticDtoAuthorizer {
	this := PinterestStaticDtoAuthorizer{}
	return &this
}

// GetStaticKey returns the StaticKey field value if set, zero value otherwise.
func (o *PinterestStaticDtoAuthorizer) GetStaticKey() string {
	if o == nil || IsNil(o.StaticKey) {
		var ret string
		return ret
	}
	return *o.StaticKey
}

// GetStaticKeyOk returns a tuple with the StaticKey field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestStaticDtoAuthorizer) GetStaticKeyOk() (*string, bool) {
	if o == nil || IsNil(o.StaticKey) {
		return nil, false
	}
	return o.StaticKey, true
}

// HasStaticKey returns a boolean if a field has been set.
func (o *PinterestStaticDtoAuthorizer) HasStaticKey() bool {
	if o != nil && !IsNil(o.StaticKey) {
		return true
	}

	return false
}

// SetStaticKey gets a reference to the given string and assigns it to the StaticKey field.
func (o *PinterestStaticDtoAuthorizer) SetStaticKey(v string) {
	o.StaticKey = &v
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *PinterestStaticDtoAuthorizer) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestStaticDtoAuthorizer) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *PinterestStaticDtoAuthorizer) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *PinterestStaticDtoAuthorizer) SetLabel(v string) {
	o.Label = &v
}

// GetClientId returns the ClientId field value if set, zero value otherwise.
func (o *PinterestStaticDtoAuthorizer) GetClientId() string {
	if o == nil || IsNil(o.ClientId) {
		var ret string
		return ret
	}
	return *o.ClientId
}

// GetClientIdOk returns a tuple with the ClientId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestStaticDtoAuthorizer) GetClientIdOk() (*string, bool) {
	if o == nil || IsNil(o.ClientId) {
		return nil, false
	}
	return o.ClientId, true
}

// HasClientId returns a boolean if a field has been set.
func (o *PinterestStaticDtoAuthorizer) HasClientId() bool {
	if o != nil && !IsNil(o.ClientId) {
		return true
	}

	return false
}

// SetClientId gets a reference to the given string and assigns it to the ClientId field.
func (o *PinterestStaticDtoAuthorizer) SetClientId(v string) {
	o.ClientId = &v
}

// GetClientSecret returns the ClientSecret field value if set, zero value otherwise.
func (o *PinterestStaticDtoAuthorizer) GetClientSecret() string {
	if o == nil || IsNil(o.ClientSecret) {
		var ret string
		return ret
	}
	return *o.ClientSecret
}

// GetClientSecretOk returns a tuple with the ClientSecret field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestStaticDtoAuthorizer) GetClientSecretOk() (*string, bool) {
	if o == nil || IsNil(o.ClientSecret) {
		return nil, false
	}
	return o.ClientSecret, true
}

// HasClientSecret returns a boolean if a field has been set.
func (o *PinterestStaticDtoAuthorizer) HasClientSecret() bool {
	if o != nil && !IsNil(o.ClientSecret) {
		return true
	}

	return false
}

// SetClientSecret gets a reference to the given string and assigns it to the ClientSecret field.
func (o *PinterestStaticDtoAuthorizer) SetClientSecret(v string) {
	o.ClientSecret = &v
}

// GetRedirectUri returns the RedirectUri field value if set, zero value otherwise.
func (o *PinterestStaticDtoAuthorizer) GetRedirectUri() string {
	if o == nil || IsNil(o.RedirectUri) {
		var ret string
		return ret
	}
	return *o.RedirectUri
}

// GetRedirectUriOk returns a tuple with the RedirectUri field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PinterestStaticDtoAuthorizer) GetRedirectUriOk() (*string, bool) {
	if o == nil || IsNil(o.RedirectUri) {
		return nil, false
	}
	return o.RedirectUri, true
}

// HasRedirectUri returns a boolean if a field has been set.
func (o *PinterestStaticDtoAuthorizer) HasRedirectUri() bool {
	if o != nil && !IsNil(o.RedirectUri) {
		return true
	}

	return false
}

// SetRedirectUri gets a reference to the given string and assigns it to the RedirectUri field.
func (o *PinterestStaticDtoAuthorizer) SetRedirectUri(v string) {
	o.RedirectUri = &v
}

func (o PinterestStaticDtoAuthorizer) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o PinterestStaticDtoAuthorizer) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.StaticKey) {
		toSerialize["staticKey"] = o.StaticKey
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.ClientId) {
		toSerialize["clientId"] = o.ClientId
	}
	if !IsNil(o.ClientSecret) {
		toSerialize["clientSecret"] = o.ClientSecret
	}
	if !IsNil(o.RedirectUri) {
		toSerialize["redirectUri"] = o.RedirectUri
	}
	return toSerialize, nil
}

type NullablePinterestStaticDtoAuthorizer struct {
	value *PinterestStaticDtoAuthorizer
	isSet bool
}

func (v NullablePinterestStaticDtoAuthorizer) Get() *PinterestStaticDtoAuthorizer {
	return v.value
}

func (v *NullablePinterestStaticDtoAuthorizer) Set(val *PinterestStaticDtoAuthorizer) {
	v.value = val
	v.isSet = true
}

func (v NullablePinterestStaticDtoAuthorizer) IsSet() bool {
	return v.isSet
}

func (v *NullablePinterestStaticDtoAuthorizer) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePinterestStaticDtoAuthorizer(val *PinterestStaticDtoAuthorizer) *NullablePinterestStaticDtoAuthorizer {
	return &NullablePinterestStaticDtoAuthorizer{value: val, isSet: true}
}

func (v NullablePinterestStaticDtoAuthorizer) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePinterestStaticDtoAuthorizer) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
