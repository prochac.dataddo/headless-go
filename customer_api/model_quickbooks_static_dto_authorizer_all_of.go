/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the QuickbooksStaticDtoAuthorizerAllOf type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &QuickbooksStaticDtoAuthorizerAllOf{}

// QuickbooksStaticDtoAuthorizerAllOf struct for QuickbooksStaticDtoAuthorizerAllOf
type QuickbooksStaticDtoAuthorizerAllOf struct {
	Label        *string `json:"label,omitempty"`
	Tenant       *string `json:"tenant,omitempty"`
	ClientId     *string `json:"clientId,omitempty"`
	ClientSecret *string `json:"clientSecret,omitempty"`
	RedirectUri  *string `json:"redirectUri,omitempty"`
}

// NewQuickbooksStaticDtoAuthorizerAllOf instantiates a new QuickbooksStaticDtoAuthorizerAllOf object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewQuickbooksStaticDtoAuthorizerAllOf() *QuickbooksStaticDtoAuthorizerAllOf {
	this := QuickbooksStaticDtoAuthorizerAllOf{}
	return &this
}

// NewQuickbooksStaticDtoAuthorizerAllOfWithDefaults instantiates a new QuickbooksStaticDtoAuthorizerAllOf object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewQuickbooksStaticDtoAuthorizerAllOfWithDefaults() *QuickbooksStaticDtoAuthorizerAllOf {
	this := QuickbooksStaticDtoAuthorizerAllOf{}
	return &this
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *QuickbooksStaticDtoAuthorizerAllOf) SetLabel(v string) {
	o.Label = &v
}

// GetTenant returns the Tenant field value if set, zero value otherwise.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetTenant() string {
	if o == nil || IsNil(o.Tenant) {
		var ret string
		return ret
	}
	return *o.Tenant
}

// GetTenantOk returns a tuple with the Tenant field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetTenantOk() (*string, bool) {
	if o == nil || IsNil(o.Tenant) {
		return nil, false
	}
	return o.Tenant, true
}

// HasTenant returns a boolean if a field has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) HasTenant() bool {
	if o != nil && !IsNil(o.Tenant) {
		return true
	}

	return false
}

// SetTenant gets a reference to the given string and assigns it to the Tenant field.
func (o *QuickbooksStaticDtoAuthorizerAllOf) SetTenant(v string) {
	o.Tenant = &v
}

// GetClientId returns the ClientId field value if set, zero value otherwise.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetClientId() string {
	if o == nil || IsNil(o.ClientId) {
		var ret string
		return ret
	}
	return *o.ClientId
}

// GetClientIdOk returns a tuple with the ClientId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetClientIdOk() (*string, bool) {
	if o == nil || IsNil(o.ClientId) {
		return nil, false
	}
	return o.ClientId, true
}

// HasClientId returns a boolean if a field has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) HasClientId() bool {
	if o != nil && !IsNil(o.ClientId) {
		return true
	}

	return false
}

// SetClientId gets a reference to the given string and assigns it to the ClientId field.
func (o *QuickbooksStaticDtoAuthorizerAllOf) SetClientId(v string) {
	o.ClientId = &v
}

// GetClientSecret returns the ClientSecret field value if set, zero value otherwise.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetClientSecret() string {
	if o == nil || IsNil(o.ClientSecret) {
		var ret string
		return ret
	}
	return *o.ClientSecret
}

// GetClientSecretOk returns a tuple with the ClientSecret field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetClientSecretOk() (*string, bool) {
	if o == nil || IsNil(o.ClientSecret) {
		return nil, false
	}
	return o.ClientSecret, true
}

// HasClientSecret returns a boolean if a field has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) HasClientSecret() bool {
	if o != nil && !IsNil(o.ClientSecret) {
		return true
	}

	return false
}

// SetClientSecret gets a reference to the given string and assigns it to the ClientSecret field.
func (o *QuickbooksStaticDtoAuthorizerAllOf) SetClientSecret(v string) {
	o.ClientSecret = &v
}

// GetRedirectUri returns the RedirectUri field value if set, zero value otherwise.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetRedirectUri() string {
	if o == nil || IsNil(o.RedirectUri) {
		var ret string
		return ret
	}
	return *o.RedirectUri
}

// GetRedirectUriOk returns a tuple with the RedirectUri field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) GetRedirectUriOk() (*string, bool) {
	if o == nil || IsNil(o.RedirectUri) {
		return nil, false
	}
	return o.RedirectUri, true
}

// HasRedirectUri returns a boolean if a field has been set.
func (o *QuickbooksStaticDtoAuthorizerAllOf) HasRedirectUri() bool {
	if o != nil && !IsNil(o.RedirectUri) {
		return true
	}

	return false
}

// SetRedirectUri gets a reference to the given string and assigns it to the RedirectUri field.
func (o *QuickbooksStaticDtoAuthorizerAllOf) SetRedirectUri(v string) {
	o.RedirectUri = &v
}

func (o QuickbooksStaticDtoAuthorizerAllOf) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o QuickbooksStaticDtoAuthorizerAllOf) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	if !IsNil(o.Tenant) {
		toSerialize["tenant"] = o.Tenant
	}
	if !IsNil(o.ClientId) {
		toSerialize["clientId"] = o.ClientId
	}
	if !IsNil(o.ClientSecret) {
		toSerialize["clientSecret"] = o.ClientSecret
	}
	if !IsNil(o.RedirectUri) {
		toSerialize["redirectUri"] = o.RedirectUri
	}
	return toSerialize, nil
}

type NullableQuickbooksStaticDtoAuthorizerAllOf struct {
	value *QuickbooksStaticDtoAuthorizerAllOf
	isSet bool
}

func (v NullableQuickbooksStaticDtoAuthorizerAllOf) Get() *QuickbooksStaticDtoAuthorizerAllOf {
	return v.value
}

func (v *NullableQuickbooksStaticDtoAuthorizerAllOf) Set(val *QuickbooksStaticDtoAuthorizerAllOf) {
	v.value = val
	v.isSet = true
}

func (v NullableQuickbooksStaticDtoAuthorizerAllOf) IsSet() bool {
	return v.isSet
}

func (v *NullableQuickbooksStaticDtoAuthorizerAllOf) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableQuickbooksStaticDtoAuthorizerAllOf(val *QuickbooksStaticDtoAuthorizerAllOf) *NullableQuickbooksStaticDtoAuthorizerAllOf {
	return &NullableQuickbooksStaticDtoAuthorizerAllOf{value: val, isSet: true}
}

func (v NullableQuickbooksStaticDtoAuthorizerAllOf) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableQuickbooksStaticDtoAuthorizerAllOf) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
