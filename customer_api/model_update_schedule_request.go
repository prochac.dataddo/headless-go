/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the UpdateScheduleRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &UpdateScheduleRequest{}

// UpdateScheduleRequest struct for UpdateScheduleRequest
type UpdateScheduleRequest struct {
	SyncFrequency *string `json:"sync_frequency,omitempty"`
	Timezone      *string `json:"timezone,omitempty"`
	// Hour schedule definition
	Minute NullableString `json:"minute,omitempty"`
	// Hour schedule definition
	Hour NullableString `json:"hour,omitempty"`
	// Day of month schedule definition
	DayOfMonth NullableString `json:"dayOfMonth,omitempty"`
	// Month schedule definition
	Month NullableString `json:"month,omitempty"`
	// Day of week schedule definition
	DayOfWeek NullableString `json:"dayOfWeek,omitempty"`
}

// NewUpdateScheduleRequest instantiates a new UpdateScheduleRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUpdateScheduleRequest() *UpdateScheduleRequest {
	this := UpdateScheduleRequest{}
	return &this
}

// NewUpdateScheduleRequestWithDefaults instantiates a new UpdateScheduleRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUpdateScheduleRequestWithDefaults() *UpdateScheduleRequest {
	this := UpdateScheduleRequest{}
	return &this
}

// GetSyncFrequency returns the SyncFrequency field value if set, zero value otherwise.
func (o *UpdateScheduleRequest) GetSyncFrequency() string {
	if o == nil || IsNil(o.SyncFrequency) {
		var ret string
		return ret
	}
	return *o.SyncFrequency
}

// GetSyncFrequencyOk returns a tuple with the SyncFrequency field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UpdateScheduleRequest) GetSyncFrequencyOk() (*string, bool) {
	if o == nil || IsNil(o.SyncFrequency) {
		return nil, false
	}
	return o.SyncFrequency, true
}

// HasSyncFrequency returns a boolean if a field has been set.
func (o *UpdateScheduleRequest) HasSyncFrequency() bool {
	if o != nil && !IsNil(o.SyncFrequency) {
		return true
	}

	return false
}

// SetSyncFrequency gets a reference to the given string and assigns it to the SyncFrequency field.
func (o *UpdateScheduleRequest) SetSyncFrequency(v string) {
	o.SyncFrequency = &v
}

// GetTimezone returns the Timezone field value if set, zero value otherwise.
func (o *UpdateScheduleRequest) GetTimezone() string {
	if o == nil || IsNil(o.Timezone) {
		var ret string
		return ret
	}
	return *o.Timezone
}

// GetTimezoneOk returns a tuple with the Timezone field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UpdateScheduleRequest) GetTimezoneOk() (*string, bool) {
	if o == nil || IsNil(o.Timezone) {
		return nil, false
	}
	return o.Timezone, true
}

// HasTimezone returns a boolean if a field has been set.
func (o *UpdateScheduleRequest) HasTimezone() bool {
	if o != nil && !IsNil(o.Timezone) {
		return true
	}

	return false
}

// SetTimezone gets a reference to the given string and assigns it to the Timezone field.
func (o *UpdateScheduleRequest) SetTimezone(v string) {
	o.Timezone = &v
}

// GetMinute returns the Minute field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *UpdateScheduleRequest) GetMinute() string {
	if o == nil || IsNil(o.Minute.Get()) {
		var ret string
		return ret
	}
	return *o.Minute.Get()
}

// GetMinuteOk returns a tuple with the Minute field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *UpdateScheduleRequest) GetMinuteOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Minute.Get(), o.Minute.IsSet()
}

// HasMinute returns a boolean if a field has been set.
func (o *UpdateScheduleRequest) HasMinute() bool {
	if o != nil && o.Minute.IsSet() {
		return true
	}

	return false
}

// SetMinute gets a reference to the given NullableString and assigns it to the Minute field.
func (o *UpdateScheduleRequest) SetMinute(v string) {
	o.Minute.Set(&v)
}

// SetMinuteNil sets the value for Minute to be an explicit nil
func (o *UpdateScheduleRequest) SetMinuteNil() {
	o.Minute.Set(nil)
}

// UnsetMinute ensures that no value is present for Minute, not even an explicit nil
func (o *UpdateScheduleRequest) UnsetMinute() {
	o.Minute.Unset()
}

// GetHour returns the Hour field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *UpdateScheduleRequest) GetHour() string {
	if o == nil || IsNil(o.Hour.Get()) {
		var ret string
		return ret
	}
	return *o.Hour.Get()
}

// GetHourOk returns a tuple with the Hour field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *UpdateScheduleRequest) GetHourOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Hour.Get(), o.Hour.IsSet()
}

// HasHour returns a boolean if a field has been set.
func (o *UpdateScheduleRequest) HasHour() bool {
	if o != nil && o.Hour.IsSet() {
		return true
	}

	return false
}

// SetHour gets a reference to the given NullableString and assigns it to the Hour field.
func (o *UpdateScheduleRequest) SetHour(v string) {
	o.Hour.Set(&v)
}

// SetHourNil sets the value for Hour to be an explicit nil
func (o *UpdateScheduleRequest) SetHourNil() {
	o.Hour.Set(nil)
}

// UnsetHour ensures that no value is present for Hour, not even an explicit nil
func (o *UpdateScheduleRequest) UnsetHour() {
	o.Hour.Unset()
}

// GetDayOfMonth returns the DayOfMonth field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *UpdateScheduleRequest) GetDayOfMonth() string {
	if o == nil || IsNil(o.DayOfMonth.Get()) {
		var ret string
		return ret
	}
	return *o.DayOfMonth.Get()
}

// GetDayOfMonthOk returns a tuple with the DayOfMonth field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *UpdateScheduleRequest) GetDayOfMonthOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.DayOfMonth.Get(), o.DayOfMonth.IsSet()
}

// HasDayOfMonth returns a boolean if a field has been set.
func (o *UpdateScheduleRequest) HasDayOfMonth() bool {
	if o != nil && o.DayOfMonth.IsSet() {
		return true
	}

	return false
}

// SetDayOfMonth gets a reference to the given NullableString and assigns it to the DayOfMonth field.
func (o *UpdateScheduleRequest) SetDayOfMonth(v string) {
	o.DayOfMonth.Set(&v)
}

// SetDayOfMonthNil sets the value for DayOfMonth to be an explicit nil
func (o *UpdateScheduleRequest) SetDayOfMonthNil() {
	o.DayOfMonth.Set(nil)
}

// UnsetDayOfMonth ensures that no value is present for DayOfMonth, not even an explicit nil
func (o *UpdateScheduleRequest) UnsetDayOfMonth() {
	o.DayOfMonth.Unset()
}

// GetMonth returns the Month field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *UpdateScheduleRequest) GetMonth() string {
	if o == nil || IsNil(o.Month.Get()) {
		var ret string
		return ret
	}
	return *o.Month.Get()
}

// GetMonthOk returns a tuple with the Month field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *UpdateScheduleRequest) GetMonthOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Month.Get(), o.Month.IsSet()
}

// HasMonth returns a boolean if a field has been set.
func (o *UpdateScheduleRequest) HasMonth() bool {
	if o != nil && o.Month.IsSet() {
		return true
	}

	return false
}

// SetMonth gets a reference to the given NullableString and assigns it to the Month field.
func (o *UpdateScheduleRequest) SetMonth(v string) {
	o.Month.Set(&v)
}

// SetMonthNil sets the value for Month to be an explicit nil
func (o *UpdateScheduleRequest) SetMonthNil() {
	o.Month.Set(nil)
}

// UnsetMonth ensures that no value is present for Month, not even an explicit nil
func (o *UpdateScheduleRequest) UnsetMonth() {
	o.Month.Unset()
}

// GetDayOfWeek returns the DayOfWeek field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *UpdateScheduleRequest) GetDayOfWeek() string {
	if o == nil || IsNil(o.DayOfWeek.Get()) {
		var ret string
		return ret
	}
	return *o.DayOfWeek.Get()
}

// GetDayOfWeekOk returns a tuple with the DayOfWeek field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *UpdateScheduleRequest) GetDayOfWeekOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.DayOfWeek.Get(), o.DayOfWeek.IsSet()
}

// HasDayOfWeek returns a boolean if a field has been set.
func (o *UpdateScheduleRequest) HasDayOfWeek() bool {
	if o != nil && o.DayOfWeek.IsSet() {
		return true
	}

	return false
}

// SetDayOfWeek gets a reference to the given NullableString and assigns it to the DayOfWeek field.
func (o *UpdateScheduleRequest) SetDayOfWeek(v string) {
	o.DayOfWeek.Set(&v)
}

// SetDayOfWeekNil sets the value for DayOfWeek to be an explicit nil
func (o *UpdateScheduleRequest) SetDayOfWeekNil() {
	o.DayOfWeek.Set(nil)
}

// UnsetDayOfWeek ensures that no value is present for DayOfWeek, not even an explicit nil
func (o *UpdateScheduleRequest) UnsetDayOfWeek() {
	o.DayOfWeek.Unset()
}

func (o UpdateScheduleRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o UpdateScheduleRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.SyncFrequency) {
		toSerialize["sync_frequency"] = o.SyncFrequency
	}
	if !IsNil(o.Timezone) {
		toSerialize["timezone"] = o.Timezone
	}
	if o.Minute.IsSet() {
		toSerialize["minute"] = o.Minute.Get()
	}
	if o.Hour.IsSet() {
		toSerialize["hour"] = o.Hour.Get()
	}
	if o.DayOfMonth.IsSet() {
		toSerialize["dayOfMonth"] = o.DayOfMonth.Get()
	}
	if o.Month.IsSet() {
		toSerialize["month"] = o.Month.Get()
	}
	if o.DayOfWeek.IsSet() {
		toSerialize["dayOfWeek"] = o.DayOfWeek.Get()
	}
	return toSerialize, nil
}

type NullableUpdateScheduleRequest struct {
	value *UpdateScheduleRequest
	isSet bool
}

func (v NullableUpdateScheduleRequest) Get() *UpdateScheduleRequest {
	return v.value
}

func (v *NullableUpdateScheduleRequest) Set(val *UpdateScheduleRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableUpdateScheduleRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableUpdateScheduleRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUpdateScheduleRequest(val *UpdateScheduleRequest) *NullableUpdateScheduleRequest {
	return &NullableUpdateScheduleRequest{value: val, isSet: true}
}

func (v NullableUpdateScheduleRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUpdateScheduleRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
