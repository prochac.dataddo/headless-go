/*
Dataddo Headless BETA API

Dataddo Headless BETA API

API version: 1.0.0-beta.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package customer_api

import (
	"encoding/json"
)

// checks if the WriteActionSettingsRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &WriteActionSettingsRequest{}

// WriteActionSettingsRequest struct for WriteActionSettingsRequest
type WriteActionSettingsRequest struct {
	WriteMode *string `json:"write_mode,omitempty"`
	//
	UniqueColumns []string `json:"unique_columns,omitempty"`
}

// NewWriteActionSettingsRequest instantiates a new WriteActionSettingsRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWriteActionSettingsRequest() *WriteActionSettingsRequest {
	this := WriteActionSettingsRequest{}
	return &this
}

// NewWriteActionSettingsRequestWithDefaults instantiates a new WriteActionSettingsRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWriteActionSettingsRequestWithDefaults() *WriteActionSettingsRequest {
	this := WriteActionSettingsRequest{}
	return &this
}

// GetWriteMode returns the WriteMode field value if set, zero value otherwise.
func (o *WriteActionSettingsRequest) GetWriteMode() string {
	if o == nil || IsNil(o.WriteMode) {
		var ret string
		return ret
	}
	return *o.WriteMode
}

// GetWriteModeOk returns a tuple with the WriteMode field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WriteActionSettingsRequest) GetWriteModeOk() (*string, bool) {
	if o == nil || IsNil(o.WriteMode) {
		return nil, false
	}
	return o.WriteMode, true
}

// HasWriteMode returns a boolean if a field has been set.
func (o *WriteActionSettingsRequest) HasWriteMode() bool {
	if o != nil && !IsNil(o.WriteMode) {
		return true
	}

	return false
}

// SetWriteMode gets a reference to the given string and assigns it to the WriteMode field.
func (o *WriteActionSettingsRequest) SetWriteMode(v string) {
	o.WriteMode = &v
}

// GetUniqueColumns returns the UniqueColumns field value if set, zero value otherwise.
func (o *WriteActionSettingsRequest) GetUniqueColumns() []string {
	if o == nil || IsNil(o.UniqueColumns) {
		var ret []string
		return ret
	}
	return o.UniqueColumns
}

// GetUniqueColumnsOk returns a tuple with the UniqueColumns field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WriteActionSettingsRequest) GetUniqueColumnsOk() ([]string, bool) {
	if o == nil || IsNil(o.UniqueColumns) {
		return nil, false
	}
	return o.UniqueColumns, true
}

// HasUniqueColumns returns a boolean if a field has been set.
func (o *WriteActionSettingsRequest) HasUniqueColumns() bool {
	if o != nil && !IsNil(o.UniqueColumns) {
		return true
	}

	return false
}

// SetUniqueColumns gets a reference to the given []string and assigns it to the UniqueColumns field.
func (o *WriteActionSettingsRequest) SetUniqueColumns(v []string) {
	o.UniqueColumns = v
}

func (o WriteActionSettingsRequest) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o WriteActionSettingsRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.WriteMode) {
		toSerialize["write_mode"] = o.WriteMode
	}
	if !IsNil(o.UniqueColumns) {
		toSerialize["unique_columns"] = o.UniqueColumns
	}
	return toSerialize, nil
}

type NullableWriteActionSettingsRequest struct {
	value *WriteActionSettingsRequest
	isSet bool
}

func (v NullableWriteActionSettingsRequest) Get() *WriteActionSettingsRequest {
	return v.value
}

func (v *NullableWriteActionSettingsRequest) Set(val *WriteActionSettingsRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableWriteActionSettingsRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableWriteActionSettingsRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWriteActionSettingsRequest(val *WriteActionSettingsRequest) *NullableWriteActionSettingsRequest {
	return &NullableWriteActionSettingsRequest{value: val, isSet: true}
}

func (v NullableWriteActionSettingsRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWriteActionSettingsRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
