package customer_api

type NullableOneOfConnectionResult struct {
	val *OneOfConnectionResult
}

func (r NullableOneOfConnectionResult) Get() *OneOfConnectionResult {
	return r.val
}

func (r NullableOneOfConnectionResult) IsSet() bool {
	return r.val != nil
}

func (r NullableOneOfConnectionResult) Set(o *OneOfConnectionResult) {
	r.val = o
}

func (r NullableOneOfConnectionResult) Unset() {
	r.val = nil
}

type OneOfConnectionResult struct{}

type Array []any
